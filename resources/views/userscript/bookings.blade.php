<style>
    .scans .mid { color: yellow; }
    .scans .new { color: green; }
    .scans .old { color: red; }
</style>

<div class="container">
    <div class="header">Attack Bookings</div>
    <div class="maintext">
        @if( $bookings->count() )
            <table style='width: 100%' id="bookings">
                <tr style="display: table-row !important">
                    <td style='text-align: left'><b>Target</b></td>
                    <td><b>LT</b></td>
                    <td><b>Eta</b></td>
                    <td><b>Race</b></td>
                    <td><b>Size</b></td>
                    <td><b>Value</b></td>
                    <td><b>Score</b></td>
                    <td><b>Amps</b></td>
                    <td><b>Scans</b></td>
                    <td><b>Intel</b></td>
                    <td><b>Fleets</b></td>
                    <td><b>Notes</b></td>
                    <td><b>Calcs</b></td>
                    <td><b>Teamup</b></td>
                    <td><b>Launch</b></td>
                    <td><b>Status</b></td>
                    <td></td>
                </tr>
                @foreach($bookings as $booking)
                    <tr class="booking" style="display: table-row !important" data-lt="{{ $booking->land_tick }}" data-x="{{ $booking->target->planet->x }}" data-y="{{ $booking->target->planet->y }}" data-z="{{ $booking->target->planet->z }}" id="booking{{ $loop->iteration }}">
                        <td style='text-align: left'>
                            {{ $booking->target->planet->x }}:{{ $booking->target->planet->y }}:{{ $booking->target->planet->z }}
                        </td>
                        <td>{{ $booking->land_tick }}</td>
                        <td>{{ $booking->land_tick-$tick }}</td>
                        <td>
                            <span class="{{ strtolower($booking->target->planet->race) }}">
                                {{ $booking->target->planet->race }}
                            </span>
                        </td>
                        <td>
                            {{ $booking->target->planet->size }}
                        </td>
                        <td>{{ number_shorten($booking->target->planet->value, 1) }}</td>
                        <td>{{ number_shorten($booking->target->planet->score, 1) }}</td>
                        <td>{{ $booking->target->planet->amps }}{{ $booking->target->planet->waves }}</td>
                        <td class="scans">
                            @if($booking->target->planet->latestP && $booking->target->planet->latestP->scan->tick >= $tick-24)
                                <a href="https://game.planetarion.com/showscan.pl?scan_id={{ $booking->target->planet->latestP->scan->pa_id }}" target="_blank"
                                @class([
                                    'new' => $booking->target->planet->latestP->scan->tick >= $tick-1,
                                    'mid' => $booking->target->planet->latestP->scan->tick >= $tick-12 && $booking->target->planet->latestP->scan->tick < $tick-1,
                                    'old' => $booking->target->planet->latestP->scan->tick >= $tick-24 && $booking->target->planet->latestP->scan->tick < $tick-12
                                ])>P</a>
                            @endif
                            @if($booking->target->planet->latestD && $booking->target->planet->latestD->scan->tick >= $tick-24)
                                <a href="https://game.planetarion.com/showscan.pl?scan_id={{ $booking->target->planet->latestD->scan->pa_id }}" target="_blank"
                                @class([
                                    'new' => $booking->target->planet->latestD->scan->tick >= $tick-1,
                                    'mid' => $booking->target->planet->latestD->scan->tick >= $tick-12 && $booking->target->planet->latestD->scan->tick < $tick-1,
                                    'old' => $booking->target->planet->latestD->scan->tick >= $tick-24 && $booking->target->planet->latestD->scan->tick < $tick-12
                                ])>D</a>
                            @endif
                            @if($booking->target->planet->latestA && $booking->target->planet->latestA->tick >= $tick-24)
                                <a href="https://game.planetarion.com/showscan.pl?scan_id={{ $booking->target->planet->latestA->pa_id }}" target="_blank"
                                @class([
                                    'new' => $booking->target->planet->latestA->tick >= $tick-1,
                                    'mid' => $booking->target->planet->latestA->tick >= $tick-12 && $booking->target->planet->latestA->tick < $tick-1,
                                    'old' => $booking->target->planet->latestA->tick >= $tick-24 && $booking->target->planet->latestA->tick < $tick-12
                                ])>A</a>
                            @endif
                            @if($booking->target->planet->latestJ && $booking->target->planet->latestJ->tick >= $tick-24)
                                <a href="https://game.planetarion.com/showscan.pl?scan_id={{ $booking->target->planet->latestJ->pa_id }}" target="_blank"
                                @class([
                                    'new' => $booking->target->planet->latestJ->tick >= $tick-1,
                                    'mid' => $booking->target->planet->latestJ->tick >= $tick-12 && $booking->target->planet->latestJ->tick < $tick-1,
                                    'old' => $booking->target->planet->latestJ->tick >= $tick-24 && $booking->target->planet->latestJ->tick < $tick-12
                                ])>A</a>
                            @endif
                        </td>
                        <td>
                            @if($booking->target->planet->alliance)
                                {{ $booking->target->planet->alliance->name }}
                            @endif
                        </td>
                        <td>
                            <span style='color: red'>
                                {{ $booking->attack_count }}({{ $booking->attack_fleets }})
                            </span><br/>
                            <span style='color: green'>
                                {{ $booking->defence_count }}({{ $booking->defence_fleets }})
                            </span>
                        </td>
                        <td>
                            {{ $booking->notes }}
                        </td>
                        <td>
                            @if($booking->battle_calc)
                                <a target='_BLANK' href='{{ $booking->battle_calc }}' style='margin: 0; background: #FB9678; color: #fff; padding: 5px; border-radius: 5px; display: inline-block; text-decoration: none; font-weight: 400; margin-right: 2px;'>
                                    manual
                                </a>
                            @endif
                            <a target='_BLANK' href='{{ $booking->auto_calc }}' style='margin: 0; background: #47C5B5; color: #fff; padding: 5px; border-radius: 5px; display: inline-block; text-decoration: none; font-weight: 400;'>
                                auto
                            </a>
                        </td>
                        <td>
                            {{ $booking->user->name }} ({{ $booking->user->planet->x }}:{{ $booking->user->planet->y }}:{{ $booking->user->planet->z }})<br/>
                            @foreach($booking->users as $user) 
                                <span>{{ $user->name }} ({{ $user->planet->x }}:{{ $user->planet->y }}:{{ $user->planet->z }})</span><br/>
                            @endforeach
                        </td>
                        <td>
                            @if($fleets[1] || $fleets[2] || $fleets[3])
                                <select class="launcher">
                                    <option value="" selected>Fleet</option>
                                    @if($fleets[1])
                                        <option value="1">1</option>
                                    @endif
                                    @if($fleets[2])
                                        <option value="2">2</option>
                                    @endif
                                    @if($fleets[3])
                                        <option value="3">3</option>
                                    @endif
                                </select>
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if($booking->status == 'land')
                                <span style="color: green">
                            @elseif($booking->status == 'recall')
                                <span style="color: red">
                            @else
                                <span>
                            @endif
                            {{ strtoupper($booking->status) }}</span>
                        </td>
                        <td>
                            <a target='_BLANK' href='{{ url("/") }}/#/attacks/{{ $booking->target->attack->attack_id }}/booking/{{ $booking->id }}' style='background: #FB9678; padding: 5px; border-radius: 5px; display: inline-block; text-decoration: none; font-weight: 400;'>
                                view
                            </a>
                        </td>
                    </tr>
                @endforeach
            </table>
        @else
            <div style="text-align: left;">No Bookings</div>
        @endif  
    </div>    
</div>