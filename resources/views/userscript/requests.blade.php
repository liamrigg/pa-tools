<div class="container">
    <div class="header">Scan Requests</div>
    <div class="maintext">
        @if( $requests->count() )
            <table style='width: 100%' id="bookings">
                <tr style="display: table-row !important">
                    <th class="align-bottom" rowspan="2">#</th>
                    <th>Co-ords</th>
                    <th>Type</th>
                    <th>Request by</th>
                    <th>Link</th>
                    <th>Time</th>
                </tr>
                @foreach($requests as $key => $request)
                    <tr style="display: table-row !important">
                        <td>{{ $key+1 }}</td>
                        <td>{{ $request->planet->x }}:{{ $request->planet->y }}:{{ $request->planet->z }}</td>
                        <td>{{ $request->scan_type }}</td>
                        <td>{{ $request->user->name }}</td>
                        <td>
                            <a target="_BLANK" v-bind:href="'https://game.planetarion.com/waves.pl?id='+$request->scan_type_id+'&x='+$request->planet->x+'&y='+$request->planet->y+'&z='$request->planet->z">attempt scan</a>
                        </td>
                        <td>{{ $request->created_at }}</td>             
                    </tr>
                @endforeach
            </table>
        @else
            <div style="text-align: left;">No Scan Requests</div>
        @endif  
    </div>    
</div>