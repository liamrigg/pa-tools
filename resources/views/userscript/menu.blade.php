<li id="menu_vengeance" class="menu_grp">
    <span class="textlabel">{{ $name }}</span>
    <ul>
        <li id="menu_att">
            <a target="_blank" href="{{ url('/') }}/#/attacks">
                <span class="textlabel">Attacks</span>
            </a>
        </li>
        <li id="menu_def">
            <a target="_blank" href="{{ url('/') }}/#/defence">
                <span class="textlabel">Defence</span>
            </a>
        </li>
        <li id="menu_bgs">
            <a target="_blank" href="https://game.planetarion.com/alliance_fleets.pl?view=launch">
                <span class="textlabel">Alliance Fleets</span>
            </a>
        </li>
    </ul>
</li>
<li id="menu_tools" class="menu_grp">
    <span class="textlabel">Tools</span>
    <ul>
        <li id="menu_tools">
            <a target="_blank" href="{{ url('/') }}/#/">
                <span class="textlabel">{{ $name }}</span>
            </a>
        </li>
        <li id="menu_bream">
            <a target="_blank" href="http://breampatools.ddns.net/PA/index.php">
                <span class="textlabel">Bream</span>
            </a>
        </li>
        <li id="menu_kia">
            <a target="_blank" href="https://kia.cthq.net/">
                <span class="textlabel">CT KIA</span>
            </a>
        </li>
    </ul>
</li>