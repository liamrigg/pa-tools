@extends('layouts.blank')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Install userscript</div>

                <div class="card-body" style="font-size: 12px;">
					<p>
						The user script on this page is unique to your user and should not be shared with others. Once 
						installed correctly you should see a pink banner stating "Userscript Loaded" underneath the menu 
						when logged in to Planetarion.
					</p>
					<h6>Desktop & Android (Firefox)</h6>
					<ol>
						<li>download <a target="_BLANK" href="https://www.tampermonkey.net/">tampermonkey</a> for your browser</li>
						<li>copy the code below</li>
						<li>create a new user script in tampermonkey</li>
						<li>enable the new user script</li>
					</ol>
					<h6>iOS (Safari)</h6>
					<ol>
						<li><a href="/js/pa_userscript.js" download>download the userscript</a></li>
						<li>move the downloaded file to a new folder on your device</li>
						<li>download the <a target="_BLANK" href="https://apps.apple.com/us/app/userscripts/id1463298887">Userscripts app</a></li>
						<li>open the Userscripts app and set your user script directory to your newly created folder from step 2</li>
						<li>open Settings, find Safari & click Extensions</li>
						<li>enable Userscripts extension & set permissions to all websites</li>
					</ol>
					<p>
					</p>
					<p>
						<button class="btn btn-sm btn-link" onclick="copy()">Copy to clipboard</button> <span style="display: none" id="copied">copied!</span>
					</p>
					<textarea id="userscript" style="width: 100%" rows=20>
// ==UserScript==
// @name          VenoX PA Script
// @namespace     na
// @version       0.1
// @description   Loads VenoX PA Script into PA
// @run-at        document-start
// @exclude       https://*/login*
// @exclude       https://*/signup*
// @exclude       https://*/register*
// @exclude       https://*/support*
// @exclude       https://*/manual*
// @exclude       https://*/chat*
// @exclude       https://*/forum*
// @exclude       https://*/botfiles*
// @exclude       https://pirate.planetarion.com/*
// @exclude       https://ninja.planetarion.com/*
// @exclude       https://forum.planetarion.com/*
// @exclude       https://www.planetarion.com/*
// {{ '@'.'include' }}        https://*.planetarion.com/*
// {{ '@'.'include' }}        http://*.planetarion.com/*
// {{ '@'.'include' }}        https://*.planetarion.com/*.p*
// {{ '@'.'include' }}        http://*.planetarion.com/*.p*
// {{ '@'.'include' }}        https://pa.ranultech.co.uk/*.p*
// @match       https://*.planetarion.com/*.p*
// @match       http://*.planetarion.com/*.p*
// @match       https://*.planetarion.com/*
// @match       http://*.planetarion.com/*
// @match       https://pa.ranultech.co.uk/*.p*
// @copyright   Original Copyright: 2008-2013, William Elwood (Will, will@howlingrain.co.uk). Adapted for CT by Rene Lodder (remyafk@gmail.com) 2011-2013. Adapted for p3nguins by munkee 2017. Stolen by moldypenguins 2018. Adapted by VenoX 2019.
// @license     GNU General Public License version 3; https://www.gnu.org/licenses/gpl.html
// ==/UserScript==

(function(w) {
    var d = w.document, l = w.location;
    if(!d || !l) return;
    var h;
    var s = d.createElement('script');
    s.async = true;
    s.setAttribute('type', 'text/javascript');
    s.setAttribute('src', '{{ url('/') }}/pascript?api_token={{ $apiToken }}');
    inject();
    function inject() {
        if(h = (d.head || d.getElementsByTagName('head')[0] || d.body || d.documentElement)) {
            h.insertBefore(s, h.firstChild);
        } else {
            setTimeout(inject, 10);
        }
    }
})(window);</textarea>
              	</div>
            </div>
		</div>
	</div>
</div>
@endsection

<script>
	function copy() {
		let textarea = document.getElementById("userscript");
		let copied = document.getElementById("copied");
		textarea.select();
		document.execCommand("copy");
		copied.style.display = "inline-block";
	}
</script>