/*
// @name pa.js
// @description VenoX script for Planetarion
// @copyright VenoX (c) 2019
// @version 129
//
*/
var toolsUrl = '{{ secure_url('/') }}';
var apiToken = '{{ $apiToken }}';
var ships = {!! $ships !!};
var scans = {!! $scans !!};

{!! file_get_contents('js/pa.js', true) !!}
