<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('fleet_movements', function (Blueprint $table) {
            $table->enum('source', [
                'incoming',
                'launch',
                'jgp',
                'notification',
                'parser',
                'userscript_alliance_defence',
                'userscript_galaxy_status',
                'userscript_alliance_fleets',
            ])->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('fleet_movements', function (Blueprint $table) {
            $table->enum('source', ['incoming', 'launch', 'jgp', 'notification', 'parser', 'userscript_alliance_defence', 'userscript_galaxy_status'])->change();
        });
    }
};
