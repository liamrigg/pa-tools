<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('galaxies', function (Blueprint $table) {
            $table->bigInteger('tick_roids')->default(0);
            $table->bigInteger('round_roids')->default(0);
            $table->bigInteger('lost_roids')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('galaxies', function (Blueprint $table) {
            $table->dropColumn('round_roids');
            $table->dropColumn('lost_roids');
            $table->dropColumn('tick_roids');
        });
    }
};
