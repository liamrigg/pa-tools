<?php

use App\Role;
use App\User;
use Illuminate\Database\Migrations\Migration;

class AddRolesToExistingUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $adminRole = Role::where('name', 'Admin')->first();
        $memberRole = Role::where('name', 'Member')->first();

        $users = User::all();

        foreach ($users as $user) {
            if ($user->is_admin) {
                $user->role_id = $adminRole->id;
            } else {
                $user->role_id = $memberRole->id;
            }
            $user->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
