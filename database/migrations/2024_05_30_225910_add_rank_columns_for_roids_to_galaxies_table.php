<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('galaxies', function (Blueprint $table) {
            $table->bigInteger('rank_round_roids')->default(0);
            $table->bigInteger('rank_lost_roids')->default(0);
            $table->bigInteger('rank_tick_roids')->default(0);
            $table->bigInteger('day_rank_round_roids')->default(0);
            $table->bigInteger('day_rank_lost_roids')->default(0);
            $table->bigInteger('day_rank_tick_roids')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('galaxies', function (Blueprint $table) {
            $table->dropColumn('rank_round_roids');
            $table->dropColumn('rank_lost_roids');
            $table->dropColumn('rank_tick_roids');
            $table->dropColumn('day_rank_round_roids');
            $table->dropColumn('day_rank_lost_roids');
            $table->dropColumn('day_rank_tick_roids');
        });
    }
};
