<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRoidsToPlanetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planets', function (Blueprint $table) {
            $table->bigInteger('round_roids')->default(0);
            $table->bigInteger('tick_roids')->default(0);
            $table->bigInteger('lost_roids')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planets', function (Blueprint $table) {
            $table->dropColumn('round_roids');
            $table->dropColumn('lost_roids');
            $table->dropColumn('tick_roids');
        });
    }
}
