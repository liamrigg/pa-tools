<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::rename('planet_scans', 'scans_planet');
        Schema::rename('development_scans', 'scans_development');
        Schema::rename('news_scans', 'scans_news');
        Schema::rename('jgp_scans', 'scans_jgp');
        Schema::rename('advanced_unit_scans', 'scans_advanced_unit');
        Schema::rename('unit_scans', 'scans_unit');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::rename('scans_planet', 'planet_scans');
        Schema::rename('scans_development', 'development_scans');
        Schema::rename('scans_news', 'news_scans');
        Schema::rename('scans_jgp', 'jgp_scans');
        Schema::rename('scans_advanced_unit', 'advanced_unit_scans');
        Schema::rename('scans_unit', 'unit_scans');
    }
};
