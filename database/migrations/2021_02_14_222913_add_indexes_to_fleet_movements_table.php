<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesToFleetMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fleet_movements', function (Blueprint $table) {
            $table->index('planet_from_id');
            $table->index('planet_to_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fleet_movements', function (Blueprint $table) {
            $table->dropIndex(['planet_from_id']);
            $table->dropIndex(['planet_to_id']);
        });
    }
}
