<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('attack_bookings', function (Blueprint $table) {
            $table->boolean('crash_announced')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('attack_bookings', function (Blueprint $table) {
            $table->dropColumn('crash_announced');
        });
    }
};
