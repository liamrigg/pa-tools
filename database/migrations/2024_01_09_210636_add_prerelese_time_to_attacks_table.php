<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPrereleseTimeToAttacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attacks', function (Blueprint $table) {
            $table->string('prerelease_time')->nullable();
            $table->integer('is_prereleased')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attacks', function (Blueprint $table) {
            $table->dropColumn('prerelease_time');
            $table->dropColumn('is_prereleased');
        });
    }
}
