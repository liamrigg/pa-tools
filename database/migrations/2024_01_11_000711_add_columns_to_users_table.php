<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('allow_night')->default(true);
            $table->boolean('allow_notifications')->default(true);
            $table->string('notification_email')->nullable();
            $table->string('notification_email_forward')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('allow_night');
            $table->dropColumn('allow_notifications');
            $table->dropColumn('notification_email');
            $table->dropColumn('notification_email_forward');
        });
    }
}
