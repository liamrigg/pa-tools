<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            [
                'name' => 'overview',
                'value' => null,
            ],
            [
                'name' => 'alliance',
                'value' => null,
            ],
            [
                'name' => 'attack',
                'value' => null,
            ],
            [
                'name' => 'tg_channels',
                'value' => null,
            ],
            [
                'name' => 'tg_notifications_channel',
                'value' => null,
            ],
            [
                'name' => 'tg_scans_channel',
                'value' => null,
            ],
            [
                'name' => 'enable_web_scans',
                'value' => null,
            ],
            [
                'name' => 'show_political_deals',
                'value' => null,
            ],
            [
                'name' => 'scans_per_second',
                'value' => 2
            ]
        ];

        foreach ($settings as $setting) {
            if (!DB::table('settings')->where('name', $setting['name'])->exists()) {
                DB::table('settings')->insert(
                    ['name' => $setting['name']],
                    ['value' => $setting['value']]
                );
            }
        }

        $roles = [
            ['name' => 'Admin'],
            ['name' => 'BC'],
            ['name' => 'Member'],
        ];

        foreach ($roles as $role) {
            if (!DB::table('roles')->where('name', $role['name'])->exists()) {
                DB::table('roles')->insert(
                    ['name' => $role['name']],
                    ['name' => $role['name']]
                );
            }
        }
    }
}
