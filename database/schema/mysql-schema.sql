/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activity` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activity_user_id_index` (`user_id`),
  KEY `activity_created_at_index` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `alliance_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alliance_history` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `rank` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int NOT NULL,
  `members` int NOT NULL,
  `counted_score` bigint NOT NULL,
  `points` bigint NOT NULL,
  `total_score` bigint NOT NULL,
  `total_value` bigint NOT NULL,
  `tick` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `alliance_id` int NOT NULL DEFAULT '0',
  `avg_size` bigint NOT NULL,
  `avg_value` bigint NOT NULL,
  `avg_score` bigint NOT NULL,
  `change_value` bigint NOT NULL DEFAULT '0',
  `change_score` bigint NOT NULL DEFAULT '0',
  `change_xp` bigint NOT NULL DEFAULT '0',
  `change_size` bigint NOT NULL DEFAULT '0',
  `rank_value` bigint NOT NULL DEFAULT '0',
  `rank_score` bigint NOT NULL DEFAULT '0',
  `rank_xp` bigint NOT NULL DEFAULT '0',
  `rank_size` bigint NOT NULL DEFAULT '0',
  `rank_avg_size` bigint NOT NULL DEFAULT '0',
  `rank_avg_value` bigint NOT NULL DEFAULT '0',
  `rank_avg_score` bigint NOT NULL DEFAULT '0',
  `change_members` int NOT NULL DEFAULT '0',
  `xp` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `alliance_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alliance_relations` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alliance_one_id` int NOT NULL,
  `alliance_two_id` int NOT NULL,
  `expires` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `alliances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alliances` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `hidden_resources` bigint NOT NULL DEFAULT '0',
  `rank_size` bigint NOT NULL DEFAULT '0',
  `rank_value` bigint NOT NULL DEFAULT '0',
  `rank_score` bigint NOT NULL DEFAULT '0',
  `rank_avg_size` bigint NOT NULL DEFAULT '0',
  `rank_avg_value` bigint NOT NULL DEFAULT '0',
  `rank_avg_score` bigint NOT NULL DEFAULT '0',
  `prod_resources` bigint NOT NULL DEFAULT '0',
  `size` bigint NOT NULL DEFAULT '0',
  `score` bigint NOT NULL DEFAULT '0',
  `value` bigint NOT NULL DEFAULT '0',
  `avg_size` bigint NOT NULL DEFAULT '0',
  `avg_value` bigint NOT NULL DEFAULT '0',
  `avg_score` bigint NOT NULL DEFAULT '0',
  `members` int NOT NULL DEFAULT '0',
  `total_score` bigint NOT NULL DEFAULT '0',
  `day_rank_value` int NOT NULL DEFAULT '0',
  `day_rank_score` int NOT NULL DEFAULT '0',
  `day_rank_size` int NOT NULL DEFAULT '0',
  `day_rank_avg_value` int NOT NULL DEFAULT '0',
  `day_rank_avg_score` int NOT NULL DEFAULT '0',
  `day_rank_avg_size` int NOT NULL DEFAULT '0',
  `day_value` bigint NOT NULL DEFAULT '0',
  `day_score` bigint NOT NULL DEFAULT '0',
  `day_size` bigint NOT NULL DEFAULT '0',
  `day_avg_value` bigint NOT NULL DEFAULT '0',
  `day_avg_score` bigint NOT NULL DEFAULT '0',
  `day_avg_size` bigint NOT NULL DEFAULT '0',
  `growth_value` bigint NOT NULL DEFAULT '0',
  `growth_score` bigint NOT NULL DEFAULT '0',
  `growth_size` bigint NOT NULL DEFAULT '0',
  `growth_rank_value` int NOT NULL DEFAULT '0',
  `growth_rank_score` int NOT NULL DEFAULT '0',
  `growth_rank_size` int NOT NULL DEFAULT '0',
  `growth_percentage_value` double(8,2) NOT NULL DEFAULT '0.00',
  `growth_percentage_score` double(8,2) NOT NULL DEFAULT '0.00',
  `growth_percentage_size` double(8,2) NOT NULL DEFAULT '0.00',
  `growth_avg_value` bigint NOT NULL DEFAULT '0',
  `growth_avg_score` bigint NOT NULL DEFAULT '0',
  `growth_avg_size` bigint NOT NULL DEFAULT '0',
  `growth_rank_avg_value` int NOT NULL DEFAULT '0',
  `growth_rank_avg_score` int NOT NULL DEFAULT '0',
  `growth_rank_avg_size` int NOT NULL DEFAULT '0',
  `growth_percentage_avg_value` double(8,2) NOT NULL DEFAULT '0.00',
  `growth_percentage_avg_score` double(8,2) NOT NULL DEFAULT '0.00',
  `growth_percentage_avg_size` double(8,2) NOT NULL DEFAULT '0.00',
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relation` enum('neutral','friendly','hostile') COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_locked` int NOT NULL DEFAULT '0',
  `day_members` int NOT NULL DEFAULT '0',
  `growth_members` int NOT NULL DEFAULT '0',
  `xp` bigint NOT NULL DEFAULT '0',
  `day_xp` bigint NOT NULL DEFAULT '0',
  `size_diff` bigint NOT NULL DEFAULT '0',
  `xp_diff` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `attack_booking_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `attack_booking_users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `booking_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `attack_booking_users_booking_id_index` (`booking_id`),
  KEY `attack_booking_users_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `attack_bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `attack_bookings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `attack_id` int NOT NULL,
  `attack_target_id` int NOT NULL,
  `land_tick` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `battle_calc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
  PRIMARY KEY (`id`),
  KEY `attack_bookings_attack_id_index` (`attack_id`),
  KEY `attack_bookings_attack_target_id_index` (`attack_target_id`),
  KEY `attack_bookings_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `attack_targets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `attack_targets` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `attack_id` int NOT NULL,
  `planet_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `attack_targets_attack_id_index` (`attack_id`),
  KEY `attack_targets_planet_id_index` (`planet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `attacks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `attacks` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `land_tick` int NOT NULL,
  `waves` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `is_closed` tinyint(1) NOT NULL DEFAULT '0',
  `scheduled_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_opened` tinyint(1) NOT NULL DEFAULT '1',
  `attack_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prerelease_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_prereleased` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `attacks_attack_id_index` (`attack_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `battlegroup_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `battlegroup_users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `battlegroup_id` int NOT NULL,
  `user_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_pending` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `battlegroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `battlegroups` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int NOT NULL,
  `value` bigint NOT NULL DEFAULT '0',
  `score` bigint NOT NULL DEFAULT '0',
  `size` bigint NOT NULL DEFAULT '0',
  `xp` bigint NOT NULL DEFAULT '0',
  `day_value` bigint NOT NULL DEFAULT '0',
  `day_score` bigint NOT NULL DEFAULT '0',
  `day_size` bigint NOT NULL DEFAULT '0',
  `day_xp` bigint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `growth_percentage_value` double(8,2) NOT NULL DEFAULT '0.00',
  `growth_percentage_score` double(8,2) NOT NULL DEFAULT '0.00',
  `growth_percentage_size` double(8,2) NOT NULL DEFAULT '0.00',
  `growth_percentage_xp` double(8,2) NOT NULL DEFAULT '0.00',
  `growth_value` bigint NOT NULL DEFAULT '0',
  `growth_score` bigint NOT NULL DEFAULT '0',
  `growth_size` bigint NOT NULL DEFAULT '0',
  `growth_xp` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bot_callback_query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_callback_query` (
  `id` bigint unsigned NOT NULL COMMENT 'Unique identifier for this query',
  `user_id` bigint DEFAULT NULL COMMENT 'Unique user identifier',
  `chat_id` bigint DEFAULT NULL COMMENT 'Unique chat identifier',
  `message_id` bigint unsigned DEFAULT NULL COMMENT 'Unique message identifier',
  `inline_message_id` char(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'Identifier of the message sent via the bot in inline mode, that originated the query',
  `chat_instance` char(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '' COMMENT 'Global identifier, uniquely corresponding to the chat to which the message with the callback button was sent',
  `data` char(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '' COMMENT 'Data associated with the callback button',
  `game_short_name` char(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '' COMMENT 'Short name of a Game to be returned, serves as the unique identifier for the game',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Entry date creation',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `chat_id` (`chat_id`),
  KEY `message_id` (`message_id`),
  KEY `chat_id_2` (`chat_id`,`message_id`),
  CONSTRAINT `bot_callback_query_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `bot_user` (`id`),
  CONSTRAINT `bot_callback_query_ibfk_2` FOREIGN KEY (`chat_id`, `message_id`) REFERENCES `bot_message` (`chat_id`, `id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bot_chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_chat` (
  `id` bigint NOT NULL COMMENT 'Unique identifier for this chat',
  `type` enum('private','group','supergroup','channel') COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT 'Type of chat, can be either private, group, supergroup or channel',
  `title` char(255) COLLATE utf8mb4_unicode_520_ci DEFAULT '' COMMENT 'Title, for supergroups, channels and group chats',
  `username` char(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'Username, for private chats, supergroups and channels if available',
  `first_name` char(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'First name of the other party in a private chat',
  `last_name` char(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'Last name of the other party in a private chat',
  `is_forum` tinyint(1) DEFAULT '0' COMMENT 'True, if the supergroup chat is a forum (has topics enabled)',
  `all_members_are_administrators` tinyint(1) DEFAULT '0' COMMENT 'True if a all members of this group are admins',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Entry date creation',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Entry date update',
  `old_id` bigint DEFAULT NULL COMMENT 'Unique chat identifier, this is filled when a group is converted to a supergroup',
  PRIMARY KEY (`id`),
  KEY `old_id` (`old_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bot_chat_join_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_chat_join_request` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier for this entry',
  `chat_id` bigint NOT NULL COMMENT 'Chat to which the request was sent',
  `user_id` bigint NOT NULL COMMENT 'User that sent the join request',
  `date` timestamp NOT NULL COMMENT 'Date the request was sent in Unix time',
  `bio` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Optional. Bio of the user',
  `invite_link` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Optional. Chat invite link that was used by the user to send the join request',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Entry date creation',
  PRIMARY KEY (`id`),
  KEY `chat_id` (`chat_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `bot_chat_join_request_ibfk_1` FOREIGN KEY (`chat_id`) REFERENCES `bot_chat` (`id`),
  CONSTRAINT `bot_chat_join_request_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `bot_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bot_chat_member_updated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_chat_member_updated` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier for this entry',
  `chat_id` bigint NOT NULL COMMENT 'Chat the user belongs to',
  `user_id` bigint NOT NULL COMMENT 'Performer of the action, which resulted in the change',
  `date` timestamp NOT NULL COMMENT 'Date the change was done in Unix time',
  `old_chat_member` text COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT 'Previous information about the chat member',
  `new_chat_member` text COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT 'New information about the chat member',
  `invite_link` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Chat invite link, which was used by the user to join the chat; for joining by invite link events only',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Entry date creation',
  PRIMARY KEY (`id`),
  KEY `chat_id` (`chat_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `bot_chat_member_updated_ibfk_1` FOREIGN KEY (`chat_id`) REFERENCES `bot_chat` (`id`),
  CONSTRAINT `bot_chat_member_updated_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `bot_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bot_chosen_inline_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_chosen_inline_result` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier for this entry',
  `result_id` char(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '' COMMENT 'The unique identifier for the result that was chosen',
  `user_id` bigint DEFAULT NULL COMMENT 'The user that chose the result',
  `location` char(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'Sender location, only for bots that require user location',
  `inline_message_id` char(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'Identifier of the sent inline message',
  `query` text COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT 'The query that was used to obtain the result',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Entry date creation',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `bot_chosen_inline_result_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `bot_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bot_conversation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_conversation` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier for this entry',
  `user_id` bigint DEFAULT NULL COMMENT 'Unique user identifier',
  `chat_id` bigint DEFAULT NULL COMMENT 'Unique user or chat identifier',
  `status` enum('active','cancelled','stopped') COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'active' COMMENT 'Conversation state',
  `command` varchar(160) COLLATE utf8mb4_unicode_520_ci DEFAULT '' COMMENT 'Default command to execute',
  `notes` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Data stored from command',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Entry date creation',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Entry date update',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `chat_id` (`chat_id`),
  KEY `status` (`status`),
  CONSTRAINT `bot_conversation_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `bot_user` (`id`),
  CONSTRAINT `bot_conversation_ibfk_2` FOREIGN KEY (`chat_id`) REFERENCES `bot_chat` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bot_edited_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_edited_message` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier for this entry',
  `chat_id` bigint DEFAULT NULL COMMENT 'Unique chat identifier',
  `message_id` bigint unsigned DEFAULT NULL COMMENT 'Unique message identifier',
  `user_id` bigint DEFAULT NULL COMMENT 'Unique user identifier',
  `edit_date` timestamp NULL DEFAULT NULL COMMENT 'Date the message was edited in timestamp format',
  `text` text COLLATE utf8mb4_unicode_520_ci COMMENT 'For text messages, the actual UTF-8 text of the message max message length 4096 char utf8',
  `entities` text COLLATE utf8mb4_unicode_520_ci COMMENT 'For text messages, special entities like usernames, URLs, bot commands, etc. that appear in the text',
  `caption` text COLLATE utf8mb4_unicode_520_ci COMMENT 'For message with caption, the actual UTF-8 text of the caption',
  PRIMARY KEY (`id`),
  KEY `chat_id` (`chat_id`),
  KEY `message_id` (`message_id`),
  KEY `user_id` (`user_id`),
  KEY `chat_id_2` (`chat_id`,`message_id`),
  CONSTRAINT `bot_edited_message_ibfk_1` FOREIGN KEY (`chat_id`) REFERENCES `bot_chat` (`id`),
  CONSTRAINT `bot_edited_message_ibfk_2` FOREIGN KEY (`chat_id`, `message_id`) REFERENCES `bot_message` (`chat_id`, `id`),
  CONSTRAINT `bot_edited_message_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `bot_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bot_inline_query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_inline_query` (
  `id` bigint unsigned NOT NULL COMMENT 'Unique identifier for this query',
  `user_id` bigint DEFAULT NULL COMMENT 'Unique user identifier',
  `location` char(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'Location of the user',
  `query` text COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT 'Text of the query',
  `offset` char(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'Offset of the result',
  `chat_type` char(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'Optional. Type of the chat, from which the inline query was sent.',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Entry date creation',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `bot_inline_query_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `bot_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bot_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_message` (
  `chat_id` bigint NOT NULL COMMENT 'Unique chat identifier',
  `sender_chat_id` bigint DEFAULT NULL COMMENT 'Sender of the message, sent on behalf of a chat',
  `id` bigint unsigned NOT NULL COMMENT 'Unique message identifier',
  `message_thread_id` bigint DEFAULT NULL COMMENT 'Unique identifier of a message thread to which the message belongs; for supergroups only',
  `user_id` bigint DEFAULT NULL COMMENT 'Unique user identifier',
  `date` timestamp NULL DEFAULT NULL COMMENT 'Date the message was sent in timestamp format',
  `forward_from` bigint DEFAULT NULL COMMENT 'Unique user identifier, sender of the original message',
  `forward_from_chat` bigint DEFAULT NULL COMMENT 'Unique chat identifier, chat the original message belongs to',
  `forward_from_message_id` bigint DEFAULT NULL COMMENT 'Unique chat identifier of the original message in the channel',
  `forward_signature` text COLLATE utf8mb4_unicode_520_ci COMMENT 'For messages forwarded from channels, signature of the post author if present',
  `forward_sender_name` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Sender''s name for messages forwarded from users who disallow adding a link to their account in forwarded messages',
  `forward_date` timestamp NULL DEFAULT NULL COMMENT 'date the original message was sent in timestamp format',
  `is_topic_message` tinyint(1) DEFAULT '0' COMMENT 'True, if the message is sent to a forum topic',
  `is_automatic_forward` tinyint(1) DEFAULT '0' COMMENT 'True, if the message is a channel post that was automatically forwarded to the connected discussion group',
  `reply_to_chat` bigint DEFAULT NULL COMMENT 'Unique chat identifier',
  `reply_to_message` bigint unsigned DEFAULT NULL COMMENT 'Message that this message is reply to',
  `via_bot` bigint DEFAULT NULL COMMENT 'Optional. Bot through which the message was sent',
  `edit_date` timestamp NULL DEFAULT NULL COMMENT 'Date the message was last edited in Unix time',
  `has_protected_content` tinyint(1) DEFAULT '0' COMMENT 'True, if the message can''t be forwarded',
  `media_group_id` text COLLATE utf8mb4_unicode_520_ci COMMENT 'The unique identifier of a media message group this message belongs to',
  `author_signature` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Signature of the post author for messages in channels',
  `text` text COLLATE utf8mb4_unicode_520_ci COMMENT 'For text messages, the actual UTF-8 text of the message max message length 4096 char utf8mb4',
  `entities` text COLLATE utf8mb4_unicode_520_ci COMMENT 'For text messages, special entities like usernames, URLs, bot commands, etc. that appear in the text',
  `caption_entities` text COLLATE utf8mb4_unicode_520_ci COMMENT 'For messages with a caption, special entities like usernames, URLs, bot commands, etc. that appear in the caption',
  `audio` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Audio object. Message is an audio file, information about the file',
  `document` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Document object. Message is a general file, information about the file',
  `animation` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Message is an animation, information about the animation',
  `game` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Game object. Message is a game, information about the game',
  `photo` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Array of PhotoSize objects. Message is a photo, available sizes of the photo',
  `sticker` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Sticker object. Message is a sticker, information about the sticker',
  `video` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Video object. Message is a video, information about the video',
  `voice` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Voice Object. Message is a Voice, information about the Voice',
  `video_note` text COLLATE utf8mb4_unicode_520_ci COMMENT 'VoiceNote Object. Message is a Video Note, information about the Video Note',
  `caption` text COLLATE utf8mb4_unicode_520_ci COMMENT 'For message with caption, the actual UTF-8 text of the caption',
  `has_media_spoiler` tinyint(1) DEFAULT '0' COMMENT 'True, if the message media is covered by a spoiler animation',
  `contact` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Contact object. Message is a shared contact, information about the contact',
  `location` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Location object. Message is a shared location, information about the location',
  `venue` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Venue object. Message is a Venue, information about the Venue',
  `poll` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Poll object. Message is a native poll, information about the poll',
  `dice` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Message is a dice with random value from 1 to 6',
  `new_chat_members` text COLLATE utf8mb4_unicode_520_ci COMMENT 'List of unique user identifiers, new member(s) were added to the group, information about them (one of these members may be the bot itself)',
  `left_chat_member` bigint DEFAULT NULL COMMENT 'Unique user identifier, a member was removed from the group, information about them (this member may be the bot itself)',
  `new_chat_title` char(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'A chat title was changed to this value',
  `new_chat_photo` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Array of PhotoSize objects. A chat photo was change to this value',
  `delete_chat_photo` tinyint(1) DEFAULT '0' COMMENT 'Informs that the chat photo was deleted',
  `group_chat_created` tinyint(1) DEFAULT '0' COMMENT 'Informs that the group has been created',
  `supergroup_chat_created` tinyint(1) DEFAULT '0' COMMENT 'Informs that the supergroup has been created',
  `channel_chat_created` tinyint(1) DEFAULT '0' COMMENT 'Informs that the channel chat has been created',
  `message_auto_delete_timer_changed` text COLLATE utf8mb4_unicode_520_ci COMMENT 'MessageAutoDeleteTimerChanged object. Message is a service message: auto-delete timer settings changed in the chat',
  `migrate_to_chat_id` bigint DEFAULT NULL COMMENT 'Migrate to chat identifier. The group has been migrated to a supergroup with the specified identifier',
  `migrate_from_chat_id` bigint DEFAULT NULL COMMENT 'Migrate from chat identifier. The supergroup has been migrated from a group with the specified identifier',
  `pinned_message` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Message object. Specified message was pinned',
  `invoice` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Message is an invoice for a payment, information about the invoice',
  `successful_payment` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Message is a service message about a successful payment, information about the payment',
  `user_shared` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Optional. Service message: a user was shared with the bot',
  `chat_shared` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Optional. Service message: a chat was shared with the bot',
  `connected_website` text COLLATE utf8mb4_unicode_520_ci COMMENT 'The domain name of the website on which the user has logged in.',
  `write_access_allowed` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Service message: the user allowed the bot added to the attachment menu to write messages',
  `passport_data` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Telegram Passport data',
  `proximity_alert_triggered` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Service message. A user in the chat triggered another user''s proximity alert while sharing Live Location.',
  `forum_topic_created` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Service message: forum topic created',
  `forum_topic_edited` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Service message: forum topic edited',
  `forum_topic_closed` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Service message: forum topic closed',
  `forum_topic_reopened` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Service message: forum topic reopened',
  `general_forum_topic_hidden` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Service message: the General forum topic hidden',
  `general_forum_topic_unhidden` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Service message: the General forum topic unhidden',
  `video_chat_scheduled` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Service message: video chat scheduled',
  `video_chat_started` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Service message: video chat started',
  `video_chat_ended` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Service message: video chat ended',
  `video_chat_participants_invited` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Service message: new participants invited to a video chat',
  `web_app_data` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Service message: data sent by a Web App',
  `reply_markup` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Inline keyboard attached to the message',
  PRIMARY KEY (`chat_id`,`id`),
  KEY `user_id` (`user_id`),
  KEY `forward_from` (`forward_from`),
  KEY `forward_from_chat` (`forward_from_chat`),
  KEY `reply_to_chat` (`reply_to_chat`),
  KEY `reply_to_message` (`reply_to_message`),
  KEY `via_bot` (`via_bot`),
  KEY `left_chat_member` (`left_chat_member`),
  KEY `migrate_from_chat_id` (`migrate_from_chat_id`),
  KEY `migrate_to_chat_id` (`migrate_to_chat_id`),
  KEY `reply_to_chat_2` (`reply_to_chat`,`reply_to_message`),
  CONSTRAINT `bot_message_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `bot_user` (`id`),
  CONSTRAINT `bot_message_ibfk_2` FOREIGN KEY (`chat_id`) REFERENCES `bot_chat` (`id`),
  CONSTRAINT `bot_message_ibfk_3` FOREIGN KEY (`forward_from`) REFERENCES `bot_user` (`id`),
  CONSTRAINT `bot_message_ibfk_4` FOREIGN KEY (`forward_from_chat`) REFERENCES `bot_chat` (`id`),
  CONSTRAINT `bot_message_ibfk_5` FOREIGN KEY (`reply_to_chat`, `reply_to_message`) REFERENCES `bot_message` (`chat_id`, `id`),
  CONSTRAINT `bot_message_ibfk_6` FOREIGN KEY (`via_bot`) REFERENCES `bot_user` (`id`),
  CONSTRAINT `bot_message_ibfk_7` FOREIGN KEY (`left_chat_member`) REFERENCES `bot_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bot_poll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_poll` (
  `id` bigint unsigned NOT NULL COMMENT 'Unique poll identifier',
  `question` text COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT 'Poll question',
  `options` text COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT 'List of poll options',
  `total_voter_count` int unsigned DEFAULT NULL COMMENT 'Total number of users that voted in the poll',
  `is_closed` tinyint(1) DEFAULT '0' COMMENT 'True, if the poll is closed',
  `is_anonymous` tinyint(1) DEFAULT '1' COMMENT 'True, if the poll is anonymous',
  `type` char(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'Poll type, currently can be “regular” or “quiz”',
  `allows_multiple_answers` tinyint(1) DEFAULT '0' COMMENT 'True, if the poll allows multiple answers',
  `correct_option_id` int unsigned DEFAULT NULL COMMENT '0-based identifier of the correct answer option. Available only for polls in the quiz mode, which are closed, or was sent (not forwarded) by the bot or to the private chat with the bot.',
  `explanation` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'Text that is shown when a user chooses an incorrect answer or taps on the lamp icon in a quiz-style poll, 0-200 characters',
  `explanation_entities` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Special entities like usernames, URLs, bot commands, etc. that appear in the explanation',
  `open_period` int unsigned DEFAULT NULL COMMENT 'Amount of time in seconds the poll will be active after creation',
  `close_date` timestamp NULL DEFAULT NULL COMMENT 'Point in time (Unix timestamp) when the poll will be automatically closed',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Entry date creation',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bot_poll_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_poll_answer` (
  `poll_id` bigint unsigned NOT NULL COMMENT 'Unique poll identifier',
  `user_id` bigint NOT NULL COMMENT 'The user, who changed the answer to the poll',
  `option_ids` text COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT '0-based identifiers of answer options, chosen by the user. May be empty if the user retracted their vote.',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Entry date creation',
  PRIMARY KEY (`poll_id`,`user_id`),
  CONSTRAINT `bot_poll_answer_ibfk_1` FOREIGN KEY (`poll_id`) REFERENCES `bot_poll` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bot_pre_checkout_query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_pre_checkout_query` (
  `id` bigint unsigned NOT NULL COMMENT 'Unique query identifier',
  `user_id` bigint DEFAULT NULL COMMENT 'User who sent the query',
  `currency` char(3) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'Three-letter ISO 4217 currency code',
  `total_amount` bigint DEFAULT NULL COMMENT 'Total price in the smallest units of the currency',
  `invoice_payload` char(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '' COMMENT 'Bot specified invoice payload',
  `shipping_option_id` char(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'Identifier of the shipping option chosen by the user',
  `order_info` text COLLATE utf8mb4_unicode_520_ci COMMENT 'Order info provided by the user',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Entry date creation',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `bot_pre_checkout_query_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `bot_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bot_request_limiter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_request_limiter` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier for this entry',
  `chat_id` char(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'Unique chat identifier',
  `inline_message_id` char(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'Identifier of the sent inline message',
  `method` char(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'Request method',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Entry date creation',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bot_shipping_query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_shipping_query` (
  `id` bigint unsigned NOT NULL COMMENT 'Unique query identifier',
  `user_id` bigint DEFAULT NULL COMMENT 'User who sent the query',
  `invoice_payload` char(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '' COMMENT 'Bot specified invoice payload',
  `shipping_address` char(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '' COMMENT 'User specified shipping address',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Entry date creation',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `bot_shipping_query_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `bot_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bot_telegram_update`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_telegram_update` (
  `id` bigint unsigned NOT NULL COMMENT 'Update''s unique identifier',
  `chat_id` bigint DEFAULT NULL COMMENT 'Unique chat identifier',
  `message_id` bigint unsigned DEFAULT NULL COMMENT 'New incoming message of any kind - text, photo, sticker, etc.',
  `edited_message_id` bigint unsigned DEFAULT NULL COMMENT 'New version of a message that is known to the bot and was edited',
  `channel_post_id` bigint unsigned DEFAULT NULL COMMENT 'New incoming channel post of any kind - text, photo, sticker, etc.',
  `edited_channel_post_id` bigint unsigned DEFAULT NULL COMMENT 'New version of a channel post that is known to the bot and was edited',
  `inline_query_id` bigint unsigned DEFAULT NULL COMMENT 'New incoming inline query',
  `chosen_inline_result_id` bigint unsigned DEFAULT NULL COMMENT 'The result of an inline query that was chosen by a user and sent to their chat partner',
  `callback_query_id` bigint unsigned DEFAULT NULL COMMENT 'New incoming callback query',
  `shipping_query_id` bigint unsigned DEFAULT NULL COMMENT 'New incoming shipping query. Only for invoices with flexible price',
  `pre_checkout_query_id` bigint unsigned DEFAULT NULL COMMENT 'New incoming pre-checkout query. Contains full information about checkout',
  `poll_id` bigint unsigned DEFAULT NULL COMMENT 'New poll state. Bots receive only updates about polls, which are sent or stopped by the bot',
  `poll_answer_poll_id` bigint unsigned DEFAULT NULL COMMENT 'A user changed their answer in a non-anonymous poll. Bots receive new votes only in polls that were sent by the bot itself.',
  `my_chat_member_updated_id` bigint unsigned DEFAULT NULL COMMENT 'The bot''s chat member status was updated in a chat. For private chats, this update is received only when the bot is blocked or unblocked by the user.',
  `chat_member_updated_id` bigint unsigned DEFAULT NULL COMMENT 'A chat member''s status was updated in a chat. The bot must be an administrator in the chat and must explicitly specify “chat_member” in the list of allowed_updates to receive these updates.',
  `chat_join_request_id` bigint unsigned DEFAULT NULL COMMENT 'A request to join the chat has been sent',
  PRIMARY KEY (`id`),
  KEY `message_id` (`message_id`),
  KEY `chat_message_id` (`chat_id`,`message_id`),
  KEY `edited_message_id` (`edited_message_id`),
  KEY `channel_post_id` (`channel_post_id`),
  KEY `edited_channel_post_id` (`edited_channel_post_id`),
  KEY `inline_query_id` (`inline_query_id`),
  KEY `chosen_inline_result_id` (`chosen_inline_result_id`),
  KEY `callback_query_id` (`callback_query_id`),
  KEY `shipping_query_id` (`shipping_query_id`),
  KEY `pre_checkout_query_id` (`pre_checkout_query_id`),
  KEY `poll_id` (`poll_id`),
  KEY `poll_answer_poll_id` (`poll_answer_poll_id`),
  KEY `my_chat_member_updated_id` (`my_chat_member_updated_id`),
  KEY `chat_member_updated_id` (`chat_member_updated_id`),
  KEY `chat_id` (`chat_id`,`channel_post_id`),
  KEY `chat_join_request_id` (`chat_join_request_id`),
  CONSTRAINT `bot_telegram_update_ibfk_1` FOREIGN KEY (`chat_id`, `message_id`) REFERENCES `bot_message` (`chat_id`, `id`),
  CONSTRAINT `bot_telegram_update_ibfk_10` FOREIGN KEY (`poll_id`) REFERENCES `bot_poll` (`id`),
  CONSTRAINT `bot_telegram_update_ibfk_11` FOREIGN KEY (`poll_answer_poll_id`) REFERENCES `bot_poll_answer` (`poll_id`),
  CONSTRAINT `bot_telegram_update_ibfk_12` FOREIGN KEY (`my_chat_member_updated_id`) REFERENCES `bot_chat_member_updated` (`id`),
  CONSTRAINT `bot_telegram_update_ibfk_13` FOREIGN KEY (`chat_member_updated_id`) REFERENCES `bot_chat_member_updated` (`id`),
  CONSTRAINT `bot_telegram_update_ibfk_14` FOREIGN KEY (`chat_join_request_id`) REFERENCES `bot_chat_join_request` (`id`),
  CONSTRAINT `bot_telegram_update_ibfk_2` FOREIGN KEY (`edited_message_id`) REFERENCES `bot_edited_message` (`id`),
  CONSTRAINT `bot_telegram_update_ibfk_3` FOREIGN KEY (`chat_id`, `channel_post_id`) REFERENCES `bot_message` (`chat_id`, `id`),
  CONSTRAINT `bot_telegram_update_ibfk_4` FOREIGN KEY (`edited_channel_post_id`) REFERENCES `bot_edited_message` (`id`),
  CONSTRAINT `bot_telegram_update_ibfk_5` FOREIGN KEY (`inline_query_id`) REFERENCES `bot_inline_query` (`id`),
  CONSTRAINT `bot_telegram_update_ibfk_6` FOREIGN KEY (`chosen_inline_result_id`) REFERENCES `bot_chosen_inline_result` (`id`),
  CONSTRAINT `bot_telegram_update_ibfk_7` FOREIGN KEY (`callback_query_id`) REFERENCES `bot_callback_query` (`id`),
  CONSTRAINT `bot_telegram_update_ibfk_8` FOREIGN KEY (`shipping_query_id`) REFERENCES `bot_shipping_query` (`id`),
  CONSTRAINT `bot_telegram_update_ibfk_9` FOREIGN KEY (`pre_checkout_query_id`) REFERENCES `bot_pre_checkout_query` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bot_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_user` (
  `id` bigint NOT NULL COMMENT 'Unique identifier for this user or bot',
  `is_bot` tinyint(1) DEFAULT '0' COMMENT 'True, if this user is a bot',
  `first_name` char(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '' COMMENT 'User''s or bot''s first name',
  `last_name` char(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'User''s or bot''s last name',
  `username` char(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'User''s or bot''s username',
  `language_code` char(10) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT 'IETF language tag of the user''s language',
  `is_premium` tinyint(1) DEFAULT '0' COMMENT 'True, if this user is a Telegram Premium user',
  `added_to_attachment_menu` tinyint(1) DEFAULT '0' COMMENT 'True, if this user added the bot to the attachment menu',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Entry date creation',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Entry date update',
  PRIMARY KEY (`id`),
  KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bot_user_chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_user_chat` (
  `user_id` bigint NOT NULL COMMENT 'Unique user identifier',
  `chat_id` bigint NOT NULL COMMENT 'Unique user or chat identifier',
  PRIMARY KEY (`user_id`,`chat_id`),
  KEY `chat_id` (`chat_id`),
  CONSTRAINT `bot_user_chat_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `bot_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `bot_user_chat_ibfk_2` FOREIGN KEY (`chat_id`) REFERENCES `bot_chat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cookies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cookies` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `defence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `defence` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `planet_to_id` int NOT NULL,
  `land_tick` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `feed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feed` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tick` int NOT NULL,
  `item_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `planet_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `fleet_movements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fleet_movements` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `launch_tick` int DEFAULT NULL,
  `fleet_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `planet_from_id` int NOT NULL,
  `planet_to_id` int NOT NULL,
  `mission_type` enum('Attack','Defend') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `land_tick` int NOT NULL,
  `tick` int NOT NULL,
  `eta` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ship_count` bigint DEFAULT NULL,
  `source` enum('incoming','launch','jgp','notification','parser','userscript_alliance_defence','userscript_galaxy_status','userscript_alliance_fleets') COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_recalled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fleet_movements_planet_from_id_index` (`planet_from_id`),
  KEY `fleet_movements_planet_to_id_index` (`planet_to_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `galaxies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `galaxies` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `x` int NOT NULL,
  `y` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `hidden_resources` bigint NOT NULL DEFAULT '0',
  `rank_size` bigint NOT NULL DEFAULT '0',
  `rank_value` bigint NOT NULL DEFAULT '0',
  `rank_score` bigint NOT NULL DEFAULT '0',
  `rank_xp` bigint NOT NULL DEFAULT '0',
  `prod_resources` bigint NOT NULL DEFAULT '0',
  `size` bigint NOT NULL DEFAULT '0',
  `score` bigint NOT NULL DEFAULT '0',
  `value` bigint NOT NULL DEFAULT '0',
  `xp` bigint NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `day_rank_value` int NOT NULL DEFAULT '0',
  `day_rank_score` int NOT NULL DEFAULT '0',
  `day_rank_size` int NOT NULL DEFAULT '0',
  `day_rank_xp` int NOT NULL DEFAULT '0',
  `day_value` bigint NOT NULL DEFAULT '0',
  `day_score` bigint NOT NULL DEFAULT '0',
  `day_size` bigint NOT NULL DEFAULT '0',
  `day_xp` bigint NOT NULL DEFAULT '0',
  `growth_value` bigint NOT NULL DEFAULT '0',
  `growth_score` bigint NOT NULL DEFAULT '0',
  `growth_size` bigint NOT NULL DEFAULT '0',
  `growth_xp` bigint NOT NULL DEFAULT '0',
  `growth_rank_value` int NOT NULL DEFAULT '0',
  `growth_rank_score` int NOT NULL DEFAULT '0',
  `growth_rank_size` int NOT NULL DEFAULT '0',
  `growth_rank_xp` int NOT NULL DEFAULT '0',
  `growth_percentage_value` double(8,2) NOT NULL DEFAULT '0.00',
  `growth_percentage_score` double(8,2) NOT NULL DEFAULT '0.00',
  `growth_percentage_size` double(8,2) NOT NULL DEFAULT '0.00',
  `growth_percentage_xp` double(8,2) NOT NULL DEFAULT '0.00',
  `ratio` double(8,2) NOT NULL DEFAULT '0.00',
  `planet_count` int NOT NULL DEFAULT '0',
  `day_planet_count` int NOT NULL DEFAULT '0',
  `growth_planet_count` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `galaxy_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `galaxy_history` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `x` int NOT NULL,
  `y` int NOT NULL,
  `galaxy_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` bigint NOT NULL,
  `score` bigint NOT NULL,
  `value` bigint NOT NULL,
  `xp` bigint NOT NULL,
  `tick` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `galaxy_id` int NOT NULL DEFAULT '0',
  `change_value` bigint NOT NULL DEFAULT '0',
  `change_score` bigint NOT NULL DEFAULT '0',
  `change_xp` bigint NOT NULL DEFAULT '0',
  `change_size` bigint NOT NULL DEFAULT '0',
  `rank_value` bigint NOT NULL DEFAULT '0',
  `rank_score` bigint NOT NULL DEFAULT '0',
  `rank_xp` bigint NOT NULL DEFAULT '0',
  `rank_size` bigint NOT NULL DEFAULT '0',
  `planet_count` int NOT NULL DEFAULT '0',
  `change_planet_count` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `intel_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `intel_change` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `planet_id` int NOT NULL,
  `alliance_from_id` int DEFAULT NULL,
  `alliance_to_id` int DEFAULT NULL,
  `tick` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_seen` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `planet_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `planet_history` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `planet_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `x` int NOT NULL,
  `y` int NOT NULL,
  `z` int NOT NULL,
  `planet_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ruler_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `race` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int NOT NULL,
  `score` bigint NOT NULL,
  `value` bigint NOT NULL,
  `xp` bigint NOT NULL,
  `tick` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `change_value` bigint NOT NULL DEFAULT '0',
  `change_score` bigint NOT NULL DEFAULT '0',
  `change_xp` bigint NOT NULL DEFAULT '0',
  `change_size` bigint NOT NULL DEFAULT '0',
  `rank_value` bigint NOT NULL DEFAULT '0',
  `rank_score` bigint NOT NULL DEFAULT '0',
  `rank_xp` bigint NOT NULL DEFAULT '0',
  `rank_size` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `planet_movements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `planet_movements` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `planet_id` int NOT NULL,
  `from_x` int DEFAULT NULL,
  `from_y` int DEFAULT NULL,
  `from_z` int DEFAULT NULL,
  `to_x` int NOT NULL,
  `to_y` int NOT NULL,
  `to_z` int NOT NULL,
  `tick` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `planets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `planets` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `planet_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nick` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alliance_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `galaxy_id` int NOT NULL DEFAULT '0',
  `latest_p` int DEFAULT NULL,
  `latest_d` int DEFAULT NULL,
  `latest_u` int DEFAULT NULL,
  `latest_au` int DEFAULT NULL,
  `amps` int NOT NULL DEFAULT '0',
  `dists` int NOT NULL DEFAULT '0',
  `waves` int NOT NULL DEFAULT '0',
  `min_alert` int NOT NULL DEFAULT '0',
  `max_alert` int NOT NULL DEFAULT '0',
  `total_cons` int NOT NULL DEFAULT '0',
  `x` int NOT NULL,
  `y` int NOT NULL,
  `z` int NOT NULL,
  `planet_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ruler_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `race` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int NOT NULL,
  `score` bigint NOT NULL,
  `value` bigint NOT NULL,
  `xp` bigint NOT NULL,
  `tick` int NOT NULL,
  `covop_hit` tinyint(1) NOT NULL DEFAULT '0',
  `rank_size` bigint NOT NULL DEFAULT '0',
  `rank_value` bigint NOT NULL DEFAULT '0',
  `rank_score` bigint NOT NULL DEFAULT '0',
  `rank_xp` bigint NOT NULL DEFAULT '0',
  `stock_resources` bigint NOT NULL DEFAULT '0',
  `prod_resources` bigint NOT NULL DEFAULT '0',
  `government` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day_rank_value` int NOT NULL DEFAULT '0',
  `day_rank_score` int NOT NULL DEFAULT '0',
  `day_rank_size` int NOT NULL DEFAULT '0',
  `day_rank_xp` int NOT NULL DEFAULT '0',
  `day_value` bigint NOT NULL DEFAULT '0',
  `day_score` bigint NOT NULL DEFAULT '0',
  `day_size` bigint NOT NULL DEFAULT '0',
  `day_xp` bigint NOT NULL DEFAULT '0',
  `growth_value` int NOT NULL DEFAULT '0',
  `growth_score` int NOT NULL DEFAULT '0',
  `growth_size` int NOT NULL DEFAULT '0',
  `growth_xp` int NOT NULL DEFAULT '0',
  `growth_rank_value` int NOT NULL DEFAULT '0',
  `growth_rank_score` int NOT NULL DEFAULT '0',
  `growth_rank_size` int NOT NULL DEFAULT '0',
  `growth_rank_xp` int NOT NULL DEFAULT '0',
  `growth_percentage_value` double(8,2) NOT NULL DEFAULT '0.00',
  `growth_percentage_score` double(8,2) NOT NULL DEFAULT '0.00',
  `growth_percentage_size` double(8,2) NOT NULL DEFAULT '0.00',
  `growth_percentage_xp` double(8,2) NOT NULL DEFAULT '0.00',
  `round_roids` bigint NOT NULL DEFAULT '0',
  `tick_roids` bigint NOT NULL DEFAULT '0',
  `lost_roids` bigint NOT NULL DEFAULT '0',
  `ticks_roided` int NOT NULL DEFAULT '0',
  `ticks_roiding` int NOT NULL DEFAULT '0',
  `rank_round_roids` int NOT NULL DEFAULT '0',
  `rank_lost_roids` int NOT NULL DEFAULT '0',
  `day_rank_round_roids` int NOT NULL DEFAULT '0',
  `day_rank_lost_roids` int NOT NULL DEFAULT '0',
  `latest_j` int DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `last_covopped` int DEFAULT NULL,
  `latest_n` int DEFAULT NULL,
  `age` int NOT NULL DEFAULT '0',
  `anarchy` int DEFAULT NULL,
  `rank_tick_roids` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `planets_alliance_id_index` (`alliance_id`),
  KEY `planets_galaxy_id_index` (`galaxy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `politics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `politics` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `alliance_id` int NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `max_planets` int DEFAULT NULL,
  `max_waves` int DEFAULT NULL,
  `max_fleets` int DEFAULT NULL,
  `expire` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `scan_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scan_queue` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `scan_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `processed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `scan_queue_scan_id_unique` (`scan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `scan_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scan_requests` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `scan_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `planet_id` int NOT NULL,
  `user_id` int NOT NULL,
  `tick` int NOT NULL,
  `scan_id` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `telegram_chat_id` bigint DEFAULT NULL,
  `telegram_message_id` bigint DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `scans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scans` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `scan_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pa_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `planet_id` int NOT NULL,
  `tick` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `time` time NOT NULL DEFAULT '00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `scans_advanced_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scans_advanced_unit` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `scan_id` int NOT NULL,
  `ship_id` int NOT NULL,
  `amount` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `scans_development`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scans_development` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `scan_id` int NOT NULL,
  `light_factory` int NOT NULL,
  `medium_factory` int NOT NULL,
  `heavy_factory` int NOT NULL,
  `wave_amplifier` int NOT NULL,
  `wave_distorter` int NOT NULL,
  `metal_refinery` int NOT NULL,
  `crystal_refinery` int NOT NULL,
  `eonium_refinery` int NOT NULL,
  `research_lab` int NOT NULL,
  `finance_centre` int NOT NULL,
  `security_centre` int NOT NULL,
  `military_centre` int NOT NULL,
  `structure_defence` int NOT NULL,
  `travel` int NOT NULL,
  `infrastructure` int NOT NULL,
  `hulls` int NOT NULL,
  `waves` int NOT NULL,
  `core` int NOT NULL,
  `covert_op` int NOT NULL,
  `mining` int NOT NULL,
  `population` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `scans_jgp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scans_jgp` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `scan_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `scans_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scans_news` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `scan_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `scans_planet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scans_planet` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `scan_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roid_metal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roid_crystal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roid_eonium` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `res_metal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `res_crystal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `res_eonium` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_usage_light` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_usage_medium` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_usage_heavy` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prod_res` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agents` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guards` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `scans_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scans_unit` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `scan_id` int NOT NULL,
  `ship_id` int NOT NULL,
  `amount` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schedule` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `datetime` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `settings` (
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  UNIQUE KEY `settings_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ships` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `t1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `t2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `init` int NOT NULL,
  `guns` int NOT NULL,
  `armor` int NOT NULL,
  `damage` int DEFAULT NULL,
  `empres` int NOT NULL,
  `metal` int NOT NULL,
  `crystal` int NOT NULL,
  `eonium` int NOT NULL,
  `total_cost` int NOT NULL,
  `race` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `eta` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `short_urls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `short_urls` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ticks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ticks` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tick` int NOT NULL,
  `time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `length` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `planet_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `galaxy_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `alliance_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_notifications` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `notification` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tick` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `planet_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `distorters` int DEFAULT NULL,
  `military_centres` int DEFAULT NULL,
  `is_new` tinyint(1) NOT NULL DEFAULT '0',
  `last_login` timestamp NULL DEFAULT NULL,
  `role_id` int NOT NULL DEFAULT '0',
  `telegram_user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stealth` int NOT NULL DEFAULT '0',
  `allow_calls` tinyint(1) NOT NULL DEFAULT '1',
  `allow_night` tinyint(1) NOT NULL DEFAULT '1',
  `allow_notifications` tinyint(1) NOT NULL DEFAULT '1',
  `notification_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notification_email_forward` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_notifications` int DEFAULT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_user_script` tinyint(1) NOT NULL DEFAULT '0',
  `is_scanner` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (1,'2014_10_12_000000_create_users_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (2,'2014_10_12_100000_create_password_resets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (3,'2018_06_15_212257_create_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (4,'2018_06_15_232351_add_planet_id_to_user_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (5,'2018_06_16_172330_create_galaxies_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (6,'2018_06_16_172336_create_alliances_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (7,'2018_06_16_223221_create_planet_history_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (8,'2018_06_20_125751_add_enabled_flag_to_users_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (9,'2018_06_20_131123_add_admin_flag_to_users_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (10,'2018_06_20_211303_rename_alliance_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (11,'2018_06_20_211354_create_alliance_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (12,'2018_06_20_212401_add_alliance_id_to_alliance_history_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (13,'2018_06_21_032333_rename_galaxy_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (14,'2018_06_21_032421_create_galaxy_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (15,'2018_06_21_032510_add_galaxy_id_to_galaxyy_history_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (16,'2018_06_21_035610_add_galaxy_id_to_planet_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (17,'2018_06_22_050227_create_scan_tables',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (18,'2018_06_22_050243_create_ship_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (19,'2018_06_22_090835_add_eta_to_ships_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (20,'2018_06_23_033117_add_latest_scans_to_planet_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (21,'2018_06_23_045309_add_resources_to_alliance_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (22,'2018_06_23_054243_add_amps_dists_and_waves_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (23,'2018_06_23_180308_add_alert_columns_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (24,'2018_06_23_181417_add_total_cons_to_planet_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (25,'2018_06_24_112700_add_phone_and_tz_to_users_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (26,'2018_07_06_063900_create_attack_tables',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (27,'2018_07_06_171132_add_notes_to_attacks_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (28,'2018_07_06_192322_add_distorter_and_mc_column_to_user',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (29,'2018_07_07_190109_add_hidden_resources_to_galaxy_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (30,'2018_07_08_175042_create_tick_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (31,'2018_07_09_000353_add_scheduled_time_to_attacks_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (32,'2018_07_09_031440_add_planet_details_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (33,'2018_07_09_035328_add_tick_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (34,'2018_07_24_203919_add_hit_covop_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (35,'2018_07_24_230906_create_settings_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (36,'2018_07_25_194712_add_avgs_to_alliance_history_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (37,'2018_07_25_220002_add_ranks_to_galaxy_tables',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (38,'2018_07_25_220101_add_ranks_to_alliances_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (39,'2018_07_25_232106_add_xp_rank_to_galaxies_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (40,'2018_07_25_232338_add_ranks_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (41,'2018_08_20_180501_add_changes_to_planet_history_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (42,'2018_08_21_183423_add_length_to_ticks_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (43,'2018_08_21_184915_add_timings_to_ticks_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (44,'2018_08_21_191826_change_timing_columns_ticks_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (45,'2018_08_22_102701_add_resources_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (46,'2018_08_22_170519_add_prod_resources_to_alliances_tables',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (47,'2018_08_23_000650_add_government_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (48,'2018_08_23_081741_add_scores_to_galaxies_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (49,'2018_08_23_083012_add_name_to_galaxies_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (50,'2018_08_23_154208_add_scores_to_alliances_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (51,'2018_08_23_160007_add_avgs_ranks_to_alliance_history_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (52,'2018_08_23_193154_add_members_to_alliances_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (53,'2018_08_23_193232_add_change_members_to_alliance_history_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (54,'2018_08_23_194017_add_total_score_to_alliances_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (55,'2018_08_24_020241_add_default_value_to_members_alliances_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (56,'2018_08_24_023449_add_default_value_to_total_score_on_alliances_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (57,'2018_08_25_000306_add_day_scores_and_ranks_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (58,'2018_08_25_004043_add_day_scores_and_ranks_to_galaxies_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (59,'2018_08_25_004129_add_day_scores_and_ranks_to_alliances_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (60,'2018_08_27_035843_add_growth_scores_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (61,'2018_08_27_080950_add_roids_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (62,'2018_08_27_081401_add_roided_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (63,'2018_08_27_091943_add_round_roid_ranks_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (64,'2018_08_28_025934_add_growth_scores_to_galaxies_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (65,'2018_08_28_030803_add_growth_scores_to_alliances_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (66,'2018_10_28_135921_add_is_new_column_to_users_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (67,'2018_10_28_221906_add_nickname_column_to_alliances_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (68,'2018_10_29_182505_add_relation_column_to_alliances_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (69,'2018_10_29_231230_create_news_scan_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (70,'2018_10_30_000406_create_fleet_movements_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (71,'2018_10_30_023301_add_ship_count_to_fleet_movements_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (72,'2018_10_30_025659_add_source_column_to_fleet_movements_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (73,'2018_10_30_045456_create_jgp_scan_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (74,'2018_10_30_050247_add_latest_j_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (75,'2018_10_30_124914_add_is_locked_column_to_alliances_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (76,'2018_11_11_002603_add_day_members_column_to_alliances_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (77,'2018_11_11_015228_add_soft_deletes_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (78,'2018_11_11_015408_add_soft_deletes_to_fleet_movements_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (79,'2018_11_11_195827_add_xp_column_to_alliances_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (80,'2018_11_11_200116_add_xp_column_to_alliance_history_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (81,'2018_11_11_200512_add_day_xp_column_to_alliances_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (82,'2018_11_11_203708_add_intel_diff_columns_to_alliances_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (83,'2018_11_13_055935_create_intel_log_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (84,'2019_01_10_180814_create_activity_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (85,'2019_01_10_181244_add_last_login_to_users_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (86,'2019_03_16_141600_add_location_column_to_activity_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (87,'2019_03_19_100316_add_last_covopped_column_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (88,'2019_03_19_221334_create_scan_requests_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (89,'2019_04_25_034008_add_latest_n_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (90,'2019_06_01_195042_add_is_seen_column_to_intel_change_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (91,'2019_06_01_234018_create_scan_queue_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (92,'2019_06_01_235750_add_processed_column_to_scan_queue_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (93,'2019_06_11_063712_change_url_to_scan_id_and_make_unique_on_scan_queue_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (94,'2019_06_11_132048_add_ratio_and_planets_to_galaxies_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (95,'2019_06_11_133721_add_planet_count_and_planet_count_change_to_galaxy_history_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (96,'2019_06_11_213548_add_time_column_to_scans_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (97,'2019_06_12_023430_add_to_source_options_on_fleet_movements_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (98,'2019_06_23_080213_add_attack_id_to_attacks_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (99,'2019_07_07_234001_add_day_planet_count_to_galaxies_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (100,'2019_08_05_212418_create_battlegroups_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (101,'2019_08_05_212845_create_battelgroup_users_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (102,'2019_08_05_215222_add_growth_percentage_columns_to_battlegroups_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (103,'2019_08_05_231722_add_is_pending_column_to_battlegroup_users_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (104,'2019_08_06_120907_create_planet_movements_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (105,'2019_08_07_021905_add_signup_tick_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (106,'2019_10_13_130717_create_roles_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (107,'2019_10_13_142444_add_role_id_to_users_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (108,'2019_10_13_143115_add_roles_to_existing_users',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (109,'2020_01_04_223120_add_tg_user_to_users_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (110,'2020_01_05_122755_create_table_attack_booking_users',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (111,'2020_01_05_171449_add_notes_and_calc_to_attack_bookings_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (112,'2020_01_07_133629_add_stealth_to_users_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (113,'2020_01_08_145441_add_order_column_to_attack_targets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (114,'2021_01_31_233802_add_notification_to_fleet_movement_source',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (115,'2021_02_07_231818_add_allow_calls_column_to_users_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (116,'2021_02_11_004927_add_index_to_user_id_on_activities_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (117,'2021_02_11_010016_add_index_to_created_at_on_activity_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (118,'2021_02_11_011921_add_index_to_alliance_id_on_planet_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (119,'2021_02_11_184944_add_indexes_to_attack_tables',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (120,'2021_02_14_222913_add_indexes_to_fleet_movements_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (121,'2021_06_14_171118_create_telegram_bot_structure',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (122,'2022_02_18_175100_update_to_0.75.0',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (123,'2022_04_24_175700_update_to_0.77.0',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (124,'2022_10_04_221900_update_to_0.78.0',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (125,'2022_11_11_130500_update_to_0.80.0',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (126,'2023_05-07_101600_update_to_0.81.0',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (127,'2024_01_09_180120_add_politics_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (128,'2024_01_09_195650_create_schedule_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (129,'2024_01_09_210636_add_prerelese_time_to_attacks_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (130,'2024_01_11_000711_add_columns_to_users_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (131,'2024_01_25_112308_add_has_notifications_column_to_users_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (132,'2024_01_25_214414_create_cookies_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (133,'2024_01_26_112402_create_feed_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (134,'2024_01_26_142109_add_anarchy_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (135,'2024_01_26_214426_add_planet_id_to_feed_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (136,'2024_01_26_215820_create_alliance_relations_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (137,'2024_01_30_135640_add_telegram_columns_to_scan_requests_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (138,'2024_01_30_143603_add_soft_deletes_to_scan_requests_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (139,'2024_02_03_233128_add_token_column_to_users_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (140,'2024_02_04_000150_add_has_user_script_column_to_users_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (141,'2024_02_25_144717_add_is_recalled_column_to_fleet_movements_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (142,'2024_02_25_161614_create_defence_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (143,'2024_02_25_170257_change_source_column_type_in_fleet_movements_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (144,'2024_02_25_204916_rename_scan_tables',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (145,'2024_02_25_225232_create_short_urls_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (146,'2024_03_14_224608_add_status_column_to_attack_bookings_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (147,'2024_03_23_183934_add_default_value_to_attacks_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (148,'2024_03_27_235434_add_userscript_enum_to_fleet_movements_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (149,'2024_03_30_225149_create_user_notifications_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (150,'2024_03_30_234738_add_rank_tick_roids_to_planets_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (151,'2024_04_02_224432_add_is_scanner_to_users_table',1);
