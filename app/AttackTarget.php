<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttackTarget extends Model
{
    protected $table = 'attack_targets';

    protected $fillable = [
        'attack_id',
        'planet_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function planet()
    {
        return $this->hasOne(Planet::class, 'id', 'planet_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function attack()
    {
        return $this->hasOne(Attack::class, 'id', 'attack_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings()
    {
        return $this->hasMany(AttackBooking::class, 'attack_target_id', 'id');
    }
}
