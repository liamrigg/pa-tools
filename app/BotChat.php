<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BotChat extends Model
{
    protected $table = 'bot_chat';
}
