<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttackBooking extends Model
{
    protected $table = 'attack_bookings';

    protected $fillable = [
        'attack_id',
        'attack_target_id',
        'land_tick',
        'user_id',
        'notes',
        'battle_calc',
        'status',
        'bg_id',
        'crash_announced',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function attack()
    {
        return $this->hasOne(Attack::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function target()
    {
        return $this->hasOne(AttackTarget::class, 'id', 'attack_target_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'attack_booking_users', 'booking_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function teamups()
    {
        return $this->belongsToMany(User::class, 'attack_booking_battlegroup', 'booking_id', 'teamup_id');
    }

    public function battlegroup()
    {
        return $this->hasOne(BattleGroup::class, 'id', 'bg_id');
    }
}
