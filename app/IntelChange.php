<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntelChange extends Model
{
    protected $table = 'intel_change';

    protected $fillable = [
        'planet_id',
        'alliance_from_id',
        'alliance_to_id',
        'tick',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function planet()
    {
        return $this->hasOne(Planet::class, 'id', 'planet_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function allianceFrom()
    {
        return $this->hasOne(Alliance::class, 'id', 'alliance_from_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function allianceTo()
    {
        return $this->hasOne(Alliance::class, 'id', 'alliance_to_id');
    }
}
