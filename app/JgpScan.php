<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JgpScan extends Model
{
    protected $table = 'scans_jgp';

    protected $fillable = [
        'scan_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function scan()
    {
        return $this->hasOne(Scan::class, 'id', 'scan_id');
    }
}
