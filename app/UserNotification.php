<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    use HasFactory;

    protected $table = 'user_notifications';

    protected $fillable = [
        'user_id',
        'notification',
        'tick',
        'notification_type',
    ];

    public function users()
    {
        return $this->hasOne(User::class, 'user_id');
    }
}
