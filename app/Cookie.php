<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cookie extends Model
{
    protected $table = 'cookies';

    protected $fillable = [
        'user_id',
        'message',
    ];

    public function user()
    {
        return $this->belongstO(User::class);
    }
}
