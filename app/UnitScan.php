<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitScan extends Model
{
    protected $table = 'scans_unit';

    protected $fillable = [
        'scan_id',
        'ship_id',
        'amount',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function scan()
    {
        return $this->hasOne(Scan::class, 'id', 'scan_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ship()
    {
        return $this->hasOne(Ship::class, 'id', 'ship_id');
    }
}
