<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllianceRelation extends Model
{
    protected $table = 'alliance_relations';

    protected $fillable = [
        'type',
        'alliance_one_id',
        'alliance_two_id',
        'expires',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function allianceOne()
    {
        return $this->hasOne(Alliance::class, 'id', 'alliance_one_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function allianceTwo()
    {
        return $this->hasOne(Alliance::class, 'id', 'alliance_two_id');
    }
}
