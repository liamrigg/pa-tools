<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attack extends Model
{
    protected $table = 'attacks';

    protected $fillable = [
        'land_tick',
        'waves',
        'notes',
        'scheduled_time',
        'prerelease_time',
        'is_prereleased',
        'is_opened',
        'is_closed',
        'attack_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function targets()
    {
        return $this->hasMany(AttackTarget::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings()
    {
        return $this->hasMany(AttackBooking::class);
    }
}
