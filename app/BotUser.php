<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BotUser extends Model
{
    protected $table = 'bot_user';

    public function user($value)
    {
        return $this->belongsTo(User::class, 'id', 'telegram_user_id');
    }
}
