<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Politics extends Model
{
    protected $table = 'politics';

    protected $fillable = [
        'alliance_id',
        'status',
        'max_planets',
        'max_waves',
        'max_fleets',
        'expire',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function alliance()
    {
        return $this->hasOne(Alliance::class, 'id', 'alliance_id');
    }
}
