<?php

namespace App\Telegram\Commands;

use App;
use App\Services\BcalcParser;
use App\Services\Bot\BotCommand;
use App\Services\ScanParser;
use App\User;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;

/**
 * Generic message command.
 */
class GenericmessageCommand extends SystemCommand
{
    private $parseHtml = false;

    /**
     * @var string
     */
    protected $name = Telegram::GENERIC_MESSAGE_COMMAND;

    /**
     * @var string
     */
    protected $description = 'Handle generic message';

    /**
     * @var string
     */
    protected $version = '1.2.0';

    /**
     * @var bool
     */
    protected $need_mysql = false;

    /**
     * Execution if MySQL is required but not available.
     *
     * @throws TelegramException
     */
    public function executeNoDb(): ServerResponse
    {
        $message = $this->getMessage();

        $chat_id = $message->getChat()->getId();
        $userId = $message->from['id'];

        $data = [
            'chat_id' => $chat_id,
            'text' => $userId,
        ];

        return Request::sendMessage($data);

        // Try to execute any deprecated system commands.
        if (self::$execute_deprecated && $deprecated_system_command_response = $this->executeDeprecatedSystemCommand()) {
            return $deprecated_system_command_response;
        }

        return Request::emptyResponse();
    }

    /**
     * Execute command.
     *
     * @throws TelegramException
     */
    public function execute(): ServerResponse
    {
        // a telegram message
        $message = $this->getMessage();

        // the text of the telegram message
        $messageText = $message->getText();

        // the user writing the message
        $telegramUser = $message->getFrom();

        // telegram message chat - we'll respond to this
        $request['chat_id'] = $message->getChat()->getId();

        $userId = 0;

        $user = User::where('telegram_user_id', $telegramUser->getId())->first();

        if ($user) {
            $userId = $user->id;
        }

        $response = '';

        // if message starts with ! or . then handle a command
        if (substr($messageText, 0, 1) == '!' || substr($messageText, 0, 1) == '.') {
            $botCommand = App::make(BotCommand::class);
            $response = $botCommand
                ->setMessage($messageText)
                ->setUserId($userId)
                ->setBotUserId($telegramUser->getId())
                ->setBotChatId($message->getChat()->getId())
                ->execute();

            if ($response) {
                $request['text'] = $response;

                // if response contains html tags
                $request = $this->checkHtml($response, $request);

                if (substr($messageText, 0, 1) == '.') {
                    $request['chat_id'] = $telegramUser->getId();
                }

                try {
                    Request::sendMessage($request);
                } catch (TelegramException $e) {
                    report($e);
                }
            }
        }

        $parser = App::make(ScanParser::class);
        $scans = $parser
            ->setRespond(true)
            ->queue($messageText);

        if ($scans) {
            $request['text'] = $scans;
            Request::sendMessage($request);
        }

        if (preg_match("/(?i)(https:\/\/[^\/]+\/bcalc.pl\?id=[0-9a-zA-Z]+)/", $messageText, $link)) {
            $bcalc = App::make(BcalcParser::class);
            $calc = $bcalc->parse($messageText);

            if ($calc) {
                $request['text'] = $calc;
                $request = $this->checkHtml($calc, $request);
                Request::sendMessage($request);
            }
        }

        return Request::emptyResponse();
    }

    private function checkHtml($response, $request)
    {
        if ($response != strip_tags($response)) {
            $request['parse_mode'] = 'HTML';
        }

        return $request;
    }
}
