<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Defence extends Model
{
    protected $table = 'defence';

    protected $fillable = [
        'planet_to_id',
        'land_tick',
    ];
}
