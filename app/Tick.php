<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Tick extends Model
{
    protected $table = 'ticks';

    protected $fillable = [
        'tick',
        'time',
        'length',
        'planet_time',
        'galaxy_time',
        'alliance_time',
    ];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('D d/m H:00');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function scans()
    {
        return $this->hasMany(Scan::class, 'tick', 'tick');
    }
}
