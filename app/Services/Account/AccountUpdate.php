<?php

namespace App\Services\Account;

use App\Planet;
use App\User;
use Hash;

class AccountUpdate
{
    private $userId;

    private $name;

    private $x;

    private $y;

    private $z;

    private $timezone;

    private $phone;

    private $notes;

    private $distorters;

    private $militaryCentres;

    private $stealth;

    private $allowCalls;

    private $allowNight;

    private $allowNotifications;

    private $notificationEmail;

    private $notificationEmailForward;

    private $password;

    public function setUserId($id)
    {
        $this->userId = $id;

        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function setX($x)
    {
        $this->x = $x;

        return $this;
    }

    public function setY($y)
    {
        $this->y = $y;

        return $this;
    }

    public function setZ($z)
    {
        $this->z = $z;

        return $this;
    }

    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    public function setDistorters($distorters)
    {
        $this->distorters = $distorters;

        return $this;
    }

    public function setMilitaryCentres($militaryCentres)
    {
        $this->militaryCentres = $militaryCentres;

        return $this;
    }

    public function setStealth($stealth)
    {
        $this->stealth = $stealth;

        return $this;
    }

    public function setAllowCalls($allowCalls)
    {
        $this->allowCalls = $allowCalls;

        return $this;
    }

    public function setAllowNight($allowNight)
    {
        $this->allowNight = $allowNight;

        return $this;
    }

    public function setAllowNotifications($allowNotifications)
    {
        $this->allowNotifications = $allowNotifications;

        return $this;
    }

    public function setNotificationEmail($notificationEmail)
    {
        $this->notificationEmail = $notificationEmail;

        return $this;
    }

    public function setNotificationEmailForward($notificationEmailForward)
    {
        $this->notificationEmailForward = $notificationEmailForward;

        return $this;
    }

    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function execute()
    {
        if ($this->x && $this->y && $this->z) {
            $planet = Planet::where([
                'x' => $this->x,
                'y' => $this->y,
                'z' => $this->z,
            ])->first();
        }

        $values = [
            'name' => $this->name,
            'planet_id' => (isset($planet)) ? $planet->id : 0,
            'timezone' => $this->timezone,
            'phone' => $this->phone,
            'notes' => $this->notes,
            'distorters' => $this->distorters,
            'military_centres' => $this->militaryCentres,
            'stealth' => $this->stealth,
            'allow_calls' => $this->allowCalls,
            'allow_night' => $this->allowNight,
            'allow_notifications' => $this->allowNotifications,
            'notification_email' => $this->notificationEmail,
            'notification_email_forward' => $this->notificationEmailForward,
        ];

        if ($this->password) {
            $values['password'] = Hash::make('password');
        }

        return User::where('id', $this->userId)->update($values);
    }
}
