<?php

namespace App\Services;

class BcalcParser
{
    public function parse($string)
    {
        preg_match('!https://[^/]+/(?:bcalc).pl\?id=([0-9a-zA-Z]+)!', $string, $bcalcMatch);

        if (! isset($bcalcMatch[0])) {
            return;
        }

        try {
            $html = file_get_contents($bcalcMatch[0]);
        } catch (\Exception $e) {
            return "Couldn't read url!";
        }

        $lines = explode("\n", $html);

        $target_x = 0;
        $target_y = 0;
        $target_z = 0;
        $def_value_difference = 0;
        // $def_ships_value_stolen = 0;
        // $def_salvage_value = 0;
        // $att_xp_gained = 0;
        $att_xp_score_gained = 0;
        $att_ships_score_difference = 0;
        $att_score_difference = 0;
        $roids_stolen = 0;

        foreach ($lines as $line) {
            if (str_contains($line, 'def_coords_x_1')) {
                preg_match("/<input type=\"number\" min=\"0\" class=\"input_coords\" id=\"def_coords_x_1\" name=\"def_coords_x_1\" value=\"(\d+)\" placeholder=\"x\">/", $line, $x);
                $target_x = $x[1];
            }

            if (str_contains($line, 'def_coords_y_1')) {
                preg_match("/<input type=\"number\" min=\"0\" class=\"input_coords\" id=\"def_coords_y_1\" name=\"def_coords_y_1\" value=\"(\d+)\" placeholder=\"y\">/", $line, $y);
                $target_y = $y[1];
            }

            if (str_contains($line, 'def_coords_z_1')) {
                preg_match("/<input type=\"number\" min=\"0\" class=\"input_coords\" id=\"def_coords_z_1\" name=\"def_coords_z_1\" value=\"(\d+)\" placeholder=\"z\">/", $line, $z);
                $target_z = $z[1];
            }

            if (str_contains($line, '<td class="def_value_lost" id="def_value_change"')) {
                preg_match("/<td class=\"def_value_lost\" id=\"def_value_change\" title=\".*\">(.*)<\/td>$/", $line, $def_value_lost);
                $def_value_difference = '-'.$def_value_lost[1];

                continue;
            }

            if (str_contains($line, '<td class="def_value_gained" id="def_value_change"')) {
                preg_match("/<td class=\"def_value_gained\" id=\"def_value_change\" title=\".*\">(.*)<\/td>$/", $line, $def_value_gained);
                $def_value_difference = '+'.$def_value_gained[1];

                continue;
            }

            // if(str_contains($line, '<td class="def_value_gained" id="def_ships_value_stolen"')) {
            //    preg_match("/<td class=\"def_value_gained\" id=\"def_ships_value_stolen\" title=\".*\">(.*)<\/td>$/", $line, $def_value_stolen);
            //        $def_ships_value_stolen = "+" . $def_value_stolen[1];
            //    continue;
            // }

            // if(str_contains($line, '<td class="def_value_gained" id="def_salvage_value"')) {
            //    preg_match("/<td class=\"def_value_gained\" id=\"def_salvage_value\" title=\".*\">(.*)<\/td>$/", $line, $def_salvage);
            //        $def_salvage_value = "+" . $def_salvage[1];
            //    continue;
            // }

            // if(str_contains($line, '<td class="att_score_gained" id="att_xp_gained"')) {
            //    preg_match("/<td class=\"att_score_gained\" id=\"att_xp_gained\" title=\".*\">(.*)<\/td>$/", $line, $xp);
            //        $att_xp_gained = $xp[1];
            //    continue;
            // }

            if (str_contains($line, '<td class="att_score_gained" id="att_xp_score_gained"')) {
                preg_match("/<td class=\"att_score_gained\" id=\"att_xp_score_gained\" title=\".*\">(.*)<\/td>$/", $line, $xp_score);
                if ($xp_score[1] == '0 - 0') {
                    $att_xp_score_gained = 0;
                } else {
                    $att_xp_score_gained = $xp_score[1];
                }

                continue;
            }

            if (str_contains($line, '<td class="att_score_lost" id="att_ships_score_lost"')) {
                preg_match("/<td class=\"att_score_lost\" id=\"att_ships_score_lost\" title=\".*\">(.*)<\/td>$/", $line, $att_ships_score_lost);
                $att_ships_score_difference = '-'.$att_ships_score_lost[1];

                continue;
            }

            if (str_contains($line, '<td class="att_score_lost" id="att_score_change"')) {
                preg_match("/<td class=\"att_score_lost\" id=\"att_score_change\" title=\".*\">(.*)<\/td>$/", $line, $att_score_lost);
                $att_score_difference = '-'.$att_score_lost[1];

                continue;
            }

            if (str_contains($line, '<td class="att_score_gained" id="att_score_change"')) {
                preg_match("/<td class=\"att_score_gained\" id=\"att_score_change\" title=\".*\">(.*)<\/td>$/", $line, $att_score_gained);
                $att_score_difference = '+'.$att_score_gained[1];

                continue;
            }

            if (str_contains($line, '<td class="def_metal_asteroids_lost" colspan="2">')) {
                preg_match("/<td class=\"def_metal_asteroids_lost\" colspan=\"2\"><span class=\"def_ro_lost\">(\d+)<\/span>/", $line, $m_roids);
                $roids_stolen = $roids_stolen + intval($m_roids[1]);
            }

            if (str_contains($line, '<td class="def_crystal_asteroids_lost" colspan="2">')) {
                preg_match("/<td class=\"def_crystal_asteroids_lost\" colspan=\"2\"><span class=\"def_ro_lost\">(\d+)<\/span>/", $line, $c_roids);
                $roids_stolen = $roids_stolen + intval($c_roids[1]);
            }

            if (str_contains($line, '<td class="def_eonium_asteroids_lost" colspan="2">')) {
                preg_match("/<td class=\"def_eonium_asteroids_lost\" colspan=\"2\"><span class=\"def_ro_lost\">(\d+)<\/span>/", $line, $e_roids);
                $roids_stolen = $roids_stolen + intval($e_roids[1]);
            }
        }

        $response = sprintf(
            "<strong>BCALC SUMMARY ON %d:%d:%d </strong>\n<strong>Defence</strong>: <u>Value</u>: %s - <u>Roids Stolen</u>: %s\n<strong>Attack</strong>: <u>XP</u>: %s - <u>V</u>: %s - <u>S</u>: %s",
            $target_x,
            $target_y,
            $target_z,
            $def_value_difference,
            $roids_stolen,
            $att_xp_score_gained,
            $att_ships_score_difference,
            $att_score_difference
        );

        return $response;
    }
}
