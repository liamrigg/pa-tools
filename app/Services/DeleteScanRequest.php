<?php

namespace App\Services;

use App\ScanRequest;
use App\User;
use Config;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;

class DeleteScanRequest
{
    private $id;

    private $userId;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    public function execute()
    {
        $user = User::find($this->userId);
        $request = ScanRequest::find($this->id);

        if (! $request) {
            return "Can't find a request with that ID";
        }

        if ($user->role->name != 'Admin' && $request->user_id !== $user->id) {
            return 'You do not have permission to delete that scan request.';
        }

        $request->delete();

        if ($request->scan_id) {
            return;
        }

        if ($request->telegram_chat_id && $request->telegram_message_id) {
            $telegram = new Telegram(Config::get('telegram.bot.api_token'), Config::get('telegram.bot.username'));

            Request::deleteMessage([
                'chat_id' => $request->telegram_chat_id,
                'message_id' => $request->telegram_message_id,
            ]);
        }

        return 'Scan request cancelled.';
    }
}
