<?php

namespace App\Services\Url;

use App\ShortUrl;

class ShortUrlCreate
{
    private $url;

    public function setUrl($url): self
    {
        $this->url = $url;

        return $this;
    }

    public function execute()
    {
        if ($url = ShortUrl::where('url', $this->url)->first()) {
            return secure_url('/').'/short/'.$url->key;
        }

        $shortUrl = new ShortUrl();
        $shortUrl->key = md5(uniqid());
        $shortUrl->url = $this->url;
        $shortUrl->save();

        return secure_url('/').'/short/'.$shortUrl->key;
    }
}
