<?php

namespace App\Services;

use App\Ship;

/**
 * A helper class for finding a ship with a search string.
 */
class FindShip
{
    protected $name;

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Find ship by name.
     *
     * return Model App\Ship | String
     */
    public function execute()
    {
        $ship = '';

        if ($this->name) {
            if (Ship::where('name', $this->name)->count() == 1) {
                $ship = Ship::where('name', $this->name)->first();
            } elseif (Ship::where('name', 'LIKE', '%'.$this->name.'%')->count() == 1) {
                $ship = Ship::where('name', 'LIKE', '%'.$this->name.'%')->first();
            } else {
                if (Ship::where('name', 'LIKE', '%'.$this->name.'%')->count() == 0) {
                    return "Can't find a ship with that name";
                } else {
                    // Check for actual name
                    if ($ship = Ship::where('name', 'LIKE', '%'.$this->name)->count() == 1) {
                        $ship = Ship::where('name', 'LIKE', '%'.$this->name)->first();
                    } else {
                        $ships = Ship::where('name', 'LIKE', '%'.$this->name.'%')->pluck('name');

                        if ($ships) {
                            $ships = $ships->toArray();
                        }

                        return 'Ship name is too ambiguous ('.implode(', ', $ships).')';
                    }
                }
            }
        }

        return $ship;
    }
}
