<?php

namespace App\Services\Settings;

use App\Setting;

trait SettingsTrait
{
    public function getSetting($name)
    {
        return Setting::where('name', $name)->first()->value;
    }
}
