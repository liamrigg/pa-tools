<?php

namespace App\Services\AllianceRelation;

use App\AllianceRelation;
use App\Services\Pager\PagerTrait;
use App\Tick;

/**
 * A helper class for finding a ship with a search string.
 */
class AllianceRelationList
{
    use PagerTrait;

    private $query;

    private $tick;

    private $type;

    private $status;

    private $alliance;

    public function __construct()
    {
        $this->query = AllianceRelation::query()->with([
            'allianceOne:id,name',
            'allianceTwo:id,name',
        ])->orderBy('expires', 'desc');
        $this->tick = Tick::orderBy('tick', 'desc')->first()->tick;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function setAlliance($alliance): self
    {
        $this->alliance = $alliance;

        return $this;
    }

    public function execute()
    {
        if ($this->type) {
            $this->query->where('type', $this->type);
        }

        if ($this->alliance) {
            $this->query->whereRaw('(alliance_one_id = ? OR alliance_two_id = ?)', [
                $this->alliance,
                $this->alliance,
            ]);
        }

        switch ($this->status) {
            case 'current':
                $this->query->where('expires', '>', $this->tick);
                break;
            case 'expired':
                $this->query->where('expires', '<=', $this->tick);
                break;
        }

        $relations = $this->paginate()->appends([
            'status' => $this->status,
            'type' => $this->type,
            'alliance' => $this->alliance,
        ]);

        foreach ($relations as $relation) {
            if ($relation->type == 'war') {
                $relation->war_count = AllianceRelation::where([
                    'alliance_one_id' => $relation->alliance_one_id,
                    'alliance_two_id' => $relation->alliance_two_id,
                    'type' => 'war',
                ])->count();
                $relation->total_war_count = AllianceRelation::where([
                    'alliance_one_id' => $relation->alliance_one_id,
                    'type' => 'war',
                ])
                    ->count();
            }
        }

        return $relations;
    }
}
