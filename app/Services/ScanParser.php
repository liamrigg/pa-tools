<?php

namespace App\Services;

use App\Planet;
use App\Scan;
use App\ScanQueue;
use App\Services\Scans\AdvancedUnitScanParser;
use App\Services\Scans\DevelopmentScanParser;
use App\Services\Scans\JumpgateScanParser;
use App\Services\Scans\NewsScanParser;
use App\Services\Scans\PlanetScanParser;
use App\Services\Scans\UnitScanParser;
use Config;

class ScanParser
{
    protected $parsed = [
        'p' => 0,
        'd' => 0,
        'u' => 0,
        'au' => 0,
        'n' => 0,
        'j' => 0,
    ];

    protected $scanIds = [];

    protected $groups = [];

    protected $respond = false;

    private $tick = 0;

    public function setRespond($respond)
    {
        $this->respond = $respond;

        return $this;
    }

    public function queue($string)
    {
        preg_match_all('!https://[^/]+/(?:showscan|waves).pl\?scan_id=([0-9a-zA-Z]+)!', $string, $single);
        preg_match_all('!https://[^/]+/(?:showscan|waves).pl\?scan_grp=([0-9a-zA-Z]+)!', $string, $group);

        // Didn't find any URL's
        if (! count($single[1]) && ! count($group[1])) {
            return;
        }

        if (count($single[1])) {
            $this->scanIds = array_values($single[1]);
        }
        if (count($group[1])) {
            $this->parseGroups(array_values($group[1]));
        }

        $ids = array_unique($this->scanIds);

        $existing = ScanQueue::whereIn('scan_id', $ids)->get()->keyBy('scan_id')->toArray();

        $newScans = [];

        foreach ($ids as $id) {
            if (! isset($existing[$id])) {
                $newScans[] = [
                    'scan_id' => $id,
                ];
            }
        }

        ScanQueue::insert($newScans);

        if ($this->respond && count($newScans)) {
            $msg = count($newScans).' new scans detected';

            if (count($newScans)) {
                $msg .= '- queued for parsing';
            }

            return $msg;
        }
    }

    public function parse($url)
    {
        $scanId = '';

        preg_match('/scan_id=([0-9a-zA-Z]+)/', $url, $id);

        if (isset($id[1])) {
            $scanId = $id[1];
        }

        if (! $scanId) {
            return;
        }

        try {
            $scan = file_get_contents($url);
        } catch (\Exception $e) {
            return;
        }

        preg_match('/>Scan time\: .* (\d+\:\d+\:\d+)/', $scan, $time);
        preg_match('/>([^>]+) on (\d+)\:(\d+)\:(\d+) in tick (\d+)/', $scan, $tick);

        if (! $tick) {
            return;
        }

        $scanType = strtoupper($tick[1]);
        $x = $tick[2];
        $y = $tick[3];
        $z = $tick[4];
        $tick = $tick[5];
        $time = $time[1];

        $planet = Planet::where([
            'x' => $x,
            'y' => $y,
            'z' => $z,
        ])->first();

        // Planet gone
        if (! $planet) {
            return;
        }

        $planetId = $planet->id;

        $scan = preg_replace("/\s+/", '', $scan);

        // If scan exists or its already queued for parsing, skip it
        if ($existingScan = Scan::where('pa_id', $scanId)->first()) {
            return;
        }

        // If scan exists in the queue already, then flag it processed and continue processing
        if ($existingQueue = ScanQueue::where('scan_id', $scanId)->first()) {
            $existingQueue->processed = 1;
            $existingQueue->save();
        } else {
            ScanQueue::create([
                'scan_id' => $scanId,
                'processed' => 1,
            ]);
        }

        if ($scanType == 'PLANET SCAN') {
            $planetScan = new PlanetScanParser();
            $planetScan = $planetScan->setScan($scan)
                ->setScanId($scanId)
                ->setPlanetId($planetId)
                ->setTick($tick)
                ->setTime($time)
                ->execute();
        }

        if ($scanType == 'DEVELOPMENT SCAN') {
            $devScan = new DevelopmentScanParser();
            $devScan = $devScan->setScan($scan)
                ->setScanId($scanId)
                ->setPlanetId($planetId)
                ->setTick($tick)
                ->setTime($time)
                ->execute();
        }

        if ($scanType == 'UNIT SCAN') {
            $unitScan = new UnitScanParser();
            $unitScan = $unitScan->setScan($scan)
                ->setScanId($scanId)
                ->setPlanetId($planetId)
                ->setTick($tick)
                ->setTime($time)
                ->execute();
        }

        if ($scanType == 'ADVANCED UNIT SCAN') {
            $auScan = new AdvancedUnitScanParser();
            $auScan = $auScan->setScan($scan)
                ->setScanId($scanId)
                ->setPlanetId($planetId)
                ->setTick($tick)
                ->setTime($time)
                ->execute();
        }

        if ($scanType == 'NEWS SCAN') {
            $newsScan = new NewsScanParser();
            $newsScan = $newsScan->setScan($scan)
                ->setScanId($scanId)
                ->setPlanetId($planetId)
                ->setTick($tick)
                ->setTime($time)
                ->execute();
        }

        if ($scanType == 'JUMPGATE PROBE') {
            $jumpgateScan = new JumpgateScanParser();
            $jumpgateScan = $jumpgateScan->setScan($scan)
                ->setScanId($scanId)
                ->setPlanetId($planetId)
                ->setTick($tick)
                ->setTime($time)
                ->execute();
        }
    }

    private function parseGroups($groups)
    {
        foreach ($groups as $group) {
            $groupUrl = Config::get('pa.group_scan_url').$group;
            try {
                $html = file_get_contents($groupUrl);
                preg_match_all('/scan_id=([0-9a-zA-Z]+)/', $html, $ids);
                if (count($ids[1])) {
                    $this->scanIds = array_merge($this->scanIds, array_values($ids[1]));
                }
            } catch (\Exception $e) {
                continue;
            }
        }
    }
}
