<?php

namespace App\Services\User;

use App\User;
use Carbon\Carbon;
use Config;

class UserList
{
    private $query;

    private $type;

    public function __construct()
    {
        $this->query = User::with('role', 'planet')
            ->withCount('notifications')
            ->orderBy('name', 'ASC');
    }

    public function setType($type): self
    {
        $this->type = $type;

        return $this;
    }

    public function execute()
    {
        switch ($this->type) {
            case 'enabled':
                $this->query = $this->query->with('planet.latestA', 'planet.latestA.au', 'botUser')->where(['is_enabled' => 1]);
                break;
            case 'disabled':
                $this->query = $this->query->where(['is_enabled' => 0, 'is_new' => 0]);
                break;
            case 'new':
                $this->query = $this->query->where(['is_enabled' => 0, 'is_new' => 1]);
                break;
        }

        $users = $this->query->get();

        if ($this->type == 'enabled') {
            foreach ($users as $user) {
                $user->timezone = ($user->timezone) ? Carbon::parse(Carbon::now($user->timezone))->format('H:i:s') : null;
                $user->ships = $this->getShips($user);
                if (Config::get('TELEGRAM_ENABLED')) {
                    $user->tg_user = '';
                    if ($user->telegram_user_id) {
                        $user->tg_user = User::where('id', $user->telegram_user_id)->first();
                    }
                }
            }
        }

        return $users;
    }

    private function getShips($user)
    {
        $ships = [];

        $totalValue = 0;

        if ($user->planet && $user->planet->latestA) {
            foreach ($user->planet->latestA->au as $ship) {
                $value = (($ship->ship->metal + $ship->ship->crystal + $ship->ship->eonium) * $ship->amount) / 100;
                $ships[] = [
                    'name' => $ship->ship->name,
                    'amount' => number_format($ship->amount, 0),
                    'value' => $value,
                ];
                $totalValue = $totalValue + $value;
            }
        }

        foreach ($ships as $key => $ship) {
            $ships[$key]['percentage'] = number_format(($ship['value'] / $totalValue) * 100, 2);
        }

        return $ships;
    }
}
