<?php

namespace App\Services\User;

use App\Role;
use App\User;

class UserEnable
{
    private $id;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function execute()
    {
        $memberRole = Role::where('name', 'Member')->first();

        User::where('id', $this->id)->update([
            'is_enabled' => true,
            'is_new' => false,
            'role_id' => $memberRole->id,
        ]);
    }
}
