<?php

namespace App\Services\User;

use App\User;

class UserDisable
{
    private $id;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function execute()
    {
        User::where('id', $this->id)->update([
            'is_enabled' => false,
            'token' => null,
        ]);
    }
}
