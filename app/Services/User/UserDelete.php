<?php

namespace App\Services\User;

use App\User;

class UserDelete
{
    private $id;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function execute()
    {
        if ($this->id != 1) {
            User::where('id', $this->id)->delete();
        }
    }
}
