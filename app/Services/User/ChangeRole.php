<?php

namespace App\Services\User;

use App\User;

class ChangeRole
{
    private $id;

    private $roleId;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;

        return $this;
    }

    public function execute()
    {
        $user = User::find($this->id);

        $user->role_id = $this->roleId;
        $user->save();
    }
}
