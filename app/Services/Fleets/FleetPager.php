<?php

namespace App\Services\Fleets;

use App\FleetMovement;
use App\Planet;

class FleetPager
{
    private $id;

    private $type;

    private $landTick;

    private $mission;

    private $movement;

    private $limit;

    private $prelaunched;

    private $tick;

    /**
     * Set id of the planet|galaxy|alliance
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set fleet movement type
     *
     * @param  string  $type  planet|galaxy|alliance
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set tick for fleet movements since
     */
    public function setLandTick(int $tick): self
    {
        $this->landTick = $tick;

        return $this;
    }

    /**
     * Set the mission type
     */
    public function setMission(array $mission): self
    {
        $this->mission = $mission;

        return $this;
    }

    /**
     * Set the movement type outgoing or incoming
     *
     * @param  string  $movement  outgoing|incoming
     */
    public function setMovement(string $movement): self
    {
        $this->movement = $movement;

        return $this;
    }

    /**
     * Set limit for pagination
     */
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * Set whether to include prelaunched fleets or not
     *
     * @param  string|null  $prelaunched
     */
    public function setPrelaunched($prelaunched): self
    {
        $this->prelaunched = is_null($prelaunched) ? $prelaunched : intval($prelaunched);

        return $this;
    }

    /**
     * Set the current tick
     */
    public function setTick(int $tick): self
    {
        $this->tick = $tick;

        return $this;
    }

    public function execute()
    {
        $planets = [];

        if ($this->id == 0) {
            $this->id = null;
        }

        switch ($this->type) {
            case 'planet':
                $planets = [$this->id];
                break;
            case 'galaxy':
                $planets = Planet::where('galaxy_id', $this->id)->pluck('id');
                break;
            case 'alliance':
                $planets = Planet::where('alliance_id', $this->id)->pluck('id');
                break;
        }

        $query = FleetMovement::with([
            'planetTo' => function ($q) {
                $q->select(['id', 'x', 'y', 'z', 'alliance_id', 'galaxy_id']);
            },
            'planetTo.alliance' => function ($q) {
                $q->select(['id', 'name']);
            },
            'planetFrom' => function ($q) {
                $q->select(['id', 'x', 'y', 'z', 'alliance_id', 'galaxy_id', 'race']);
            },
            'planetFrom.alliance',
        ])->orderBy('land_tick', 'DESC')
            ->orderBy('mission_type');

        // prelaunched fleets dont have land ticks
        if ($this->prelaunched === 0) {
            $query->where('land_tick', '>', $this->landTick);
        }

        $missions = array_keys($this->mission, true);

        $misses = [];

        foreach ($missions as $mission) {
            if ($mission == 'unknown') {
                $misses[] = 'mission_type is null';
            } else {
                $misses[] = "mission_type = '".ucfirst($mission)."'";
            }
        }

        if ($misses) {
            $query->whereRaw('('.implode(' OR ', $misses).')');
        }

        if ($this->movement == 'outgoing') {
            $query->whereIn('planet_from_id', $planets);
        }
        if ($this->movement == 'incoming') {
            $query->whereIn('planet_to_id', $planets);
        }

        if ($this->prelaunched === 0 || $this->prelaunched === 1) {
            $query->where('is_prelaunched', $this->prelaunched);
            // Only include prelaunch up to +11 since thats as high as you can prelaunch
            if ($this->prelaunched === 1) {
                $query->where('tick', '>', $this->tick - 12);
            }
        }

        return $query->paginate($this->limit);
    }
}
