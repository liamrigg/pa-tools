<?php

namespace App\Services\Scans;

use App\ScanRequest;
use App\User;

class ScanRequestList
{
    private $query;

    private $userId;

    private $requesterId;

    public function __construct()
    {
        $this->query = ScanRequest::with(['planet:id,galaxy_id,x,y,z,dists', 'user:id,name', 'scan:id,pa_id'])
            ->orderBy('created_at', 'desc')
            ->distinct('planet_id', 'scan_type');
    }

    public function setUserId(int $id): self
    {
        $this->userId = $id;

        return $this;
    }

    public function setRequesterId(int $id): self
    {
        $this->requesterId = $id;

        return $this;
    }

    public function execute()
    {
        if ($this->requesterId) {
            $this->query = $this->query->where('user_id', $this->requesterId);

            return $this->query->get();
        }

        if ($this->userId) {
            $user = User::find($this->userId);
            if ($user->is_scanner || $user->role->name == 'Admin') {

                return $this->query->whereNull('scan_id')->get();
            }
        }

        return collect();
    }
}
