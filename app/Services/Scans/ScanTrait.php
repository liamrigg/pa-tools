<?php

namespace App\Services\Scans;

use App\Scan;
use Config;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;

trait ScanTrait
{
    protected $scan;

    protected $scanId;

    protected $planetId;

    protected $tick;

    protected $time;

    public function setScan($scan)
    {
        $this->scan = $scan;

        return $this;
    }

    public function setScanId($scanId)
    {
        $this->scanId = $scanId;

        return $this;
    }

    public function setTick($tick)
    {
        $this->tick = $tick;

        return $this;
    }

    public function setPlanetId($planetId)
    {
        $this->planetId = $planetId;

        return $this;
    }

    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    public function requestFulfilled($req)
    {
        $telegram = new Telegram(Config::get('telegram.bot.api_token'), Config::get('telegram.bot.username'));

        $this->messageScanToUser($req);
        $this->deleteRequestFromScanChannel($req);
    }

    private function messageScanToUser($req)
    {
        $chatId = $req->user->telegram_user_id;

        $scan = Scan::find($req->scan_id);

        $scan = 'https://game.planetarion.com/showscan.pl?scan_id='.$scan->pa_id;

        if ($chatId) {
            $data = [
                'chat_id' => $chatId,
                'text' => '[#'.$req->id.'] '.strtoupper($req->scan_type).' scan on '.$req->planet->x.':'.$req->planet->y.':'.$req->planet->z.' complete: '.$scan,
            ];

            Request::sendMessage($data);
        }
    }

    private function deleteRequestFromScanChannel($req)
    {
        if ($req->telegram_message_id && $req->telegram_chat_id) {
            Request::deleteMessage([
                'chat_id' => $req->telegram_chat_id,
                'message_id' => $req->telegram_message_id,
            ]);
        }
    }
}
