<?php

namespace App\Services\Scans;

use App\FleetMovement;
use App\JgpScan;
use App\Planet;
use App\Scan;
use App\ScanRequest;
use App\User;

class JumpgateScanParser
{
    use ScanTrait;

    public $movement;

    private bool $analysis = false;

    public function setAnalysis(bool $analysis): self
    {
        $this->analysis = $analysis;

        return $this;
    }

    public function execute()
    {
        $scan = $this->scan;
        $scanId = $this->scanId;
        $planetId = $this->planetId;
        $tick = $this->tick;
        $time = $this->time;

        $movement = [];

        $newScan = Scan::create([
            'pa_id' => $scanId,
            'planet_id' => $planetId,
            'scan_type' => JgpScan::class,
            'tick' => $tick,
            'time' => $time,
        ]);

        $jgpScan = JgpScan::create([
            'scan_id' => $newScan->id,
        ]);

        preg_match_all('!<tr[^>]*><td[^>]*><a[^>]*>(\d+):(\d+):(\d+)<\/a>\(<span[^>]*>(\w+)<\/span>\)<\/td><td[^>]*>(\w+)<\/td><td[^>]*>([a-zA-Z0-9\-\!\+\=\_\?]+)<\/td><td[^>]*>(\d+)<\/td><td[^>]*>(\d+)<\/td><\/tr>!', $scan, $fleets);

        $count = count($fleets[0]);

        for ($int = 0; $int < $count; $int++) {
            $x = $fleets[1][$int];
            $y = $fleets[2][$int];
            $z = $fleets[3][$int];
            $missionType = $fleets[5][$int];
            $fleetName = $fleets[6][$int];
            $eta = $fleets[7][$int];
            $shipCount = $fleets[8][$int];
            $planetFrom = Planet::where(['x' => $x, 'y' => $y, 'z' => $z])->first();
            $planetTo = Planet::find($planetId);
            $galMates = Planet::where('galaxy_id', $planetTo->galaxy_id)->get()->keyBy('id');

            if (! $planetFrom) {
                continue;
            }
            //Dont record returning fleets
            if ($missionType == 'Return') {
                continue;
            }

            $movement[$int]['is_prelaunched'] = 0;

            // Dont record any attacks eta 8 and above (could be PL)
            if ($missionType == 'Attack' && $eta > 7) {
                $movement[$int]['is_prelaunched'] = 1;
            }
            // Dont include any defences eta 7 and above (could be PL)
            if ($missionType == 'Defend' && $eta > 6) {
                $movement[$int]['is_prelaunched'] = 1;
            }
            // Dont record eta 5 defences from gal mates (is PL)
            if ($missionType == 'Defend' && isset($galMates[$planetFrom->id]) && $eta == 5) {
                $movement[$int]['is_prelaunched'] = 1;
            }

            $movement[$int]['fleet_name'] = $fleetName;
            $movement[$int]['mission_type'] = $missionType;
            $movement[$int]['planet_from_id'] = $planetFrom->id;
            $movement[$int]['planet_to_id'] = $planetId;
            $movement[$int]['tick'] = $tick;
            $movement[$int]['eta'] = $eta;
            $movement[$int]['ship_count'] = $shipCount;
            $movement[$int]['source'] = 'jgp';
            $movement[$int]['land_tick'] = null;
            if (!$movement[$int]['is_prelaunched']) {
                $movement[$int]['land_tick'] = $tick + $movement[$int]['eta'];
            }
        }

        $this->movement = $movement;

        foreach ($movement as $fleet) {
            $launched = FleetMovement::where([
                'land_tick' => $fleet['land_tick'],
                'planet_from_id' => $fleet['planet_from_id'],
                'planet_to_id' => $fleet['planet_to_id'],
            ])->first();

            if (! $launched) {
                FleetMovement::create($fleet);

                // If we had to create a fleet movement for a member planet being attacked we
                // know they dont have notifications configured
                $user = User::where('planet_id', $fleet['planet_to_id'])->first();
                if ($user && $fleet['mission_type'] == 'Attack' && ! $fleet['is_prelaunched']) {
                    $user->update([
                        'has_notifications' => 0,
                    ]);
                }
            }
        }

        if ($request = ScanRequest::with(['planet', 'user'])->whereNull('scan_id')->where(['planet_id' => $planetId, 'scan_type' => 'j'])->get()) {
            foreach ($request as $req) {
                $req->scan_id = $newScan->id;
                $req->save();
                $this->requestFulfilled($req);
            }
        }

        $planet = Planet::with('latestJ', 'latestJ.scan')->where('id', $planetId)->first();

        if ((isset($planet->latestJ) && $planet->latestJ->scan->tick < $tick) || ! $planet->latest_j || $planet->latestJ->scan->time < $newScan->time) {
            $planet->latest_j = $jgpScan->id;
            $planet->save();
        }

        return true;
    }
}
