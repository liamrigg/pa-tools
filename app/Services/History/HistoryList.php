<?php

namespace App\Services\History;

use App\AllianceHistory;
use App\GalaxyHistory;
use App\PlanetHistory;

class HistoryList
{
    private $query;

    private $id;

    private $type;

    private $limit;

    private $orderBy;

    /**
     * The id of the planet|galaxy|alliance
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * The type of history
     *
     * @param  string  $type  planet|galaxy|alliance
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Optional limit
     */
    public function setLimit(?int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    public function execute()
    {
        switch ($this->type) {
            case 'planet':
                $this->query = PlanetHistory::where('planet_id', $this->id);
                break;
            case 'galaxy':
                $this->query = GalaxyHistory::where('galaxy_id', $this->id);
                break;
            case 'alliance':
                $this->query = AllianceHistory::where('alliance_id', $this->id);
                break;
        }

        return $this->query
            ->with('tick')
            ->orderBy('tick', 'DESC')
            ->paginate($this->limit);
    }
}
