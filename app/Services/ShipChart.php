<?php

namespace App\Services;

use App\Planet;
use App\Ship;

class ShipChart
{
    private $id;

    private $type;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function execute()
    {
        $planets = Planet::with([
            'latestA', 'latestA.au', 'latestA.au.ship',
        ]);

        if ($this->type == 'alliance') {
            $planets = $planets->where('alliance_id', $this->id);
        }
        if ($this->type == 'galaxy') {
            $planets = $planets->where('galaxy_id', $this->id);
        }
        if ($this->type == 'planet') {
            $planets = $planets->where('id', $this->id);
        }

        $planets = $planets->get();

        $ships = [];

        foreach ($planets as $planet) {
            if ($planet->latestA) {
                foreach ($planet->latestA->au as $ship) {
                    if (isset($ships[$ship->ship->race][$ship->ship->class][$ship->ship->name]['amount'])) {
                        $ships[$ship->ship->race][$ship->ship->class][$ship->ship->name]['amount'] = $ships[$ship->ship->race][$ship->ship->class][$ship->ship->name]['amount'] + $ship->amount;
                        $ships[$ship->ship->race][$ship->ship->class][$ship->ship->name]['value'] = $ships[$ship->ship->race][$ship->ship->class][$ship->ship->name]['value'] + (($ship->ship->metal + $ship->ship->crystal + $ship->ship->eonium) * $ship->amount) / 100;
                    } else {
                        $ships[$ship->ship->race][$ship->ship->class][$ship->ship->name]['amount'] = $ship->amount;
                        $ships[$ship->ship->race][$ship->ship->class][$ship->ship->name]['value'] = (($ship->ship->metal + $ship->ship->crystal + $ship->ship->eonium) * $ship->amount) / 100;
                    }
                }
            }
        }

        $races = Ship::groupBy('race')->pluck('race')->toArray();

        $ships = array_merge(array_flip($races), $ships);

        $classes = Ship::groupBy('class')->pluck('class')->toArray();

        foreach ($ships as $key => $race) {
            if (is_array($race)) {
                $ships[$key] = array_merge(array_flip($classes), $race);
            } else {
                unset($ships[$key]);
            }
        }

        $allyValue = 0;
        $classValue = [];

        foreach ($classes as $class) {
            $classValue[$class] = 0;
        }

        $items = [];

        foreach ($ships as $key => $race) {
            foreach ($race as $index => $class) {
                if (! is_array($class)) {
                    unset($ships[$key][$index]);
                } else {
                    foreach ($class as $ship => $item) {
                        $items[] = [
                            'race' => $key,
                            'name' => $ship,
                            'amount' => number_format($item['amount'], 0),
                            'value' => $item['value'],
                        ];
                        $classValue[$index] = $classValue[$index] + $item['value'];
                        $allyValue = $allyValue + $item['value'];
                    }
                }
            }
        }

        foreach ($classValue as $key => $value) {
            unset($classValue[$key]);
            $classValue[$key]['value'] = $value;
            $classValue[$key]['percentage'] = ($value) ? number_format(($value / $allyValue) * 100, 2) : 0;
        }

        foreach ($items as $key => $item) {
            $items[$key]['value_percentage'] = ($item['value']) ? number_format(($item['value'] / $allyValue) * 100, 2) : 0;
        }

        $classesArr = [
            ['Class', 'Amount']
        ];

        foreach ($classes as $class) {
            $classesArr[] = [$class, $classValue[$class]['value']];
        }

        $arrays = [
            'ships' => $items,
            'classes' => $classesArr,
        ];

        return collect($arrays);
    }
}
