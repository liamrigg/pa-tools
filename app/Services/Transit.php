<?php

namespace App\Services;

use App\Schedule;
use App\User;
use Carbon\Carbon;
use Config;
use Illuminate\Support\Facades\Http;
use Infobip\Api\SendSmsApi;
use Infobip\ApiException;
use Infobip\Configuration;
use Infobip\Model\SmsAdvancedTextualRequest;
use Infobip\Model\SmsDestination;
use Infobip\Model\SmsTextualMessage;
use Log;

class Transit
{
    private $config;

    private $client;

    private $from;

    public function __construct()
    {
        $this->config = (new Configuration())
            ->setHost(Config::get('infobip.api_url'))
            ->setApiKeyPrefix('Authorization', 'App')
            ->setApiKey('Authorization', Config::get('infobip.api_key'));

        $this->from = Config::get('infobip.from');
    }

    /**
     * Send an SMS to member.
     */
    public function sms($to, $message): string
    {
        // sanitize data
        $username = addslashes($to);
        $message = addslashes($message);

        // find the user
        $user = User::where('name', 'LIKE', '%'.$username.'%')->first();

        if (! $user) {
            return 'Unable to find anyone by that name';
        }

        $user->phone = preg_replace('/[^\d]+/', '', $user->phone);

        // check if sms should be sent
        if (! $user->phone) {
            return 'User "'.$user->name.'" does not have a phone number entered.';
        }
        if (! $this->isAllowed($user, 'sms')) {
            return 'SMS disabled for "'.$user->name.'" for this tick';
        }

        // send sms
        $text = str_replace('\\', '', $message);

        $sendSmsApi = new SendSmsApi(config: $this->config);
        $to = new SmsDestination([
            'to' => $user->phone,
        ]);

        $message = new SmsTextualMessage([
            'destinations' => $to,
            'from' => $this->from,
            'text' => $text,
        ]);

        $request = new SmsAdvancedTextualRequest(['messages' => $message]);

        try {
            $sendSmsApi->sendSmsMessage($request);
        } catch (ApiException $apiException) {
            Log::info($apiException->getMessage());

            return 'Message sending failed [code: '.$apiException->getCode().'].';
        }

        if (strlen($user['timezone']) > 2) {
            return sprintf('Sending message "%s" to "%s" at %s (%s)', $text, $user->name, date('H:m:s', strtotime(Carbon::parse(Carbon::now($user->timezone)))), Carbon::parse(Carbon::now($user->timezone))->timezoneName);
        } else {
            return sprintf('Sending message "%s" to "%s" at %s (%s)', $text, $user->name, date('H:m:s', strtotime(Carbon::parse(Carbon::now()))), Carbon::parse(Carbon::now())->timezoneName);
        }
    }

    /**
     * Place a call to user.
     */
    public function call($to): string
    {
        // validate input
        $username = addslashes($to);
        $user = User::where('name', 'LIKE', '%'.$username.'%')->first();

        if (! $user) {
            return 'Unable to find anyone by that name';
        }

        // fetch the user
        $user->phone = preg_replace('/[^\d]+/', '', $user->phone);

        // check if call should be made
        if (! $user->phone) {
            return 'User "'.$user->name.'" does not have a phone number entered.';
        }
        if (! $this->isAllowed($user, 'call')) {
            return 'Calls disabled for "'.$user->name.'" at nightime ('.Carbon::parse(Carbon::now($user['timezone']))->timezoneName.')';
        }

        // place call
        $postUrl = 'https://'.Config::get('infobip.api_url').'/tts/3/advanced';

        $postData = [
            'messages' => [
                'from' => $this->from,
                'destinations' => [
                    'to' => $user->phone,
                ],
                'language' => 'en',
                'voice' => [
                    'name' => 'Joanna',
                    'gender' => 'female',
                ],
                'text' => 'You are needed - see Telegram',
            ],
        ];

        $response = Http::withHeaders([
            'Authorization' => 'App '.Config::get('infobip.api_key'),
        ])->post($postUrl, $postData);

        if ($response->failed()) {
            Log::info('Call failed - '.$response->getStatusCode().' '.$response->getReasonPhrase());

            return 'Call failed [code: '.$response->getStatusCode().'].';
        }

        if (strlen($user['timezone']) > 2) {
            return sprintf('Call placed to "%s" at %s (%s)', $user->name, date('H:m:s', strtotime(Carbon::parse(Carbon::now($user['timezone'])))), Carbon::parse(Carbon::now($user['timezone']))->timezoneName);
        } else {
            return sprintf('Call placed to "%s" at %s (%s)', $user->name, date('H:m:s', strtotime(Carbon::parse(Carbon::now()))), Carbon::parse(Carbon::now())->timezoneName);
        }
    }

    /**
     * Check if user has enabled nightime calls.
     */
    public function isAllowed($user, $type): bool
    {
        if ($type == 'sms' && ! $user->allow_notifications) {
            return false;
        }

        if ($type == 'call' && ! $user->allow_calls) {
            return false;
        }

        if (! $user->allow_night && strlen($user->timezone) > 2) {
            $time = Carbon::parse(Carbon::now($user->timezone)->startOfHour());
            $day = strtolower(date('l', strtotime($time)));
            $hour = date('H', strtotime($time));
            $schedule = Schedule::where(['user_id' => $user->id, 'datetime' => $day.'_'.$hour])->first();

            if (isset($schedule->datetime)) {
                return false;
            }
        }

        // allow action
        return true;
    }
}
