<?php

namespace App\Services;

use App\Planet;

class RaceChart
{
    private $id;

    private $type;

    private $races = [
        'Ter' => 0,
        'Cat' => 0,
        'Xan' => 0,
        'Zik' => 0,
        'Etd' => 0,
        'Kin' => 0,
        'Sly' => 0,
    ];

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function execute()
    {
        $planets = '';
        if ($this->type == 'alliance') {
            $planets = Planet::select('race')->where('alliance_id', $this->id)->get();
        }
        if ($this->type == 'galaxy') {
            $planets = Planet::select('race')->where('galaxy_id', $this->id)->get();
        }

        foreach ($planets as $planet) {
            $this->races[$planet->race]++;
        }

        $chart = [
            ['Race', 'Number'],
            ['Ter', $this->races['Ter']],
            ['Cat', $this->races['Cat']],
            ['Xan', $this->races['Xan']],
            ['Zik', $this->races['Zik']],
            ['Etd', $this->races['Etd']],
            ['Kin', $this->races['Kin']],
            ['Sly', $this->races['Sly']],
        ];

        return $chart;
    }
}
