<?php

namespace App\Services\Activity;

use App\Activity;
use Carbon\Carbon;

class ActivityList
{
    private $days;

    private $userId;

    public function setDays($days)
    {
        $this->days = $days;

        return $this;
    }

    public function setUserId($id)
    {
        $this->userId = $id;

        return $this;
    }

    public function execute()
    {
        $from = Carbon::now()->addDays($this->days)->startOfDay();
        $to = Carbon::parse($from)->endOfDay();

        $activities = Activity::with(['user' => function ($q) {
            $q->select(['id', 'name']);
        }])->whereBetween('created_at', [$from, $to])
            ->orderBy('created_at', 'DESC');

        if ($this->userId) {
            $activities->where('user_id', $this->userId);
        }

        return $activities->paginate(50)->appends(['user_id' => $this->userId]);
    }
}
