<?php

namespace App\Services\Races;

use App\Ship;
use Config;

trait RacesTrait
{
    public function getRaces()
    {
        $races = [];
        $configRaces = Config::get('pa.races');
        $shipRaces = Ship::select('race')->distinct()->get()->keyBy('race')->keys();

        foreach ($shipRaces as $race) {
            $races[] = $configRaces[$race];
        }

        return $races;

    }
}
