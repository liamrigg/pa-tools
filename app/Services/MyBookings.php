<?php

namespace App\Services;

use App;
use App\AttackBooking;
use App\FleetMovement;
use App\Services\Misc\MakeBattleCalc;
use App\Tick;
use App\User;
use Auth;

class MyBookings
{
    protected $id;

    protected $userId;

    protected $attackId;

    protected $battleGroupId;

    private $tick;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setUserId($id)
    {
        $this->userId = $id;

        return $this;
    }

    public function setAttackId($id)
    {
        $this->attackId = $id;

        return $this;
    }

    public function setBattleGroupId($id)
    {
        $this->battleGroupId = $id;

        return $this;
    }

    public function __construct()
    {
        $this->tick = Tick::orderBy('tick', 'DESC')->first()->tick;
    }

    public function execute()
    {
        $userBookings = [];
        $bgBookings = collect();

        if ($this->userId) {
            $owner = AttackBooking::where('user_id', $this->userId)->get();
            if ($owner) {
                $owner = $owner->pluck('id');
            }
            $sharedUser = AttackBooking::whereHas('users', function ($q) {
                $q->where('user_id', $this->userId);
            })->get();
            if ($sharedUser) {
                $sharedUser = $sharedUser->pluck('id');
            }
    
            $userBookings = array_merge($owner->toArray(), $sharedUser->toArray());
        }

        if (!$this->userId) {
            $this->userId = Auth::user()->id;
        }

        if ($this->id || $this->battleGroupId) {
            $bgs = User::find($this->userId)->battlegroups;
            if ($bgs) {
                $bgs = $bgs->pluck('id');
    
                $bgBookings = AttackBooking::whereIn('bg_id', $bgs)->get();
                if($bgBookings) {
                    $bgBookings = $bgBookings->pluck('id');
                }
            }
        }
        
       
        $attackBookings = array_merge($userBookings, $bgBookings->toArray());

        $bookings = AttackBooking::with([
            'battlegroup',
            'target',
            'target.planet',
            'target.attack',
            'target.planet',
            'target.planet.alliance',
            'target.planet.latestP',
            'target.planet.latestP.scan',
            'target.planet.latestD',
            'target.planet.latestD.scan',
            'target.planet.latestN',
            'target.planet.latestN.scan',
            'target.planet.latestJ',
            'target.planet.latestJ.scan',
            'target.planet.latestU',
            'target.planet.latestU.u',
            'target.planet.latestU.u.ship',
            'target.planet.latestA',
            'target.planet.latestA.au',
            'user',
            'users',
            'users.planet',
        ])
            ->orderBy('land_tick', 'ASC')
            ->where('land_tick', '>', $this->tick);

        if ($attackBookings) {
            $bookings = $bookings->whereIn('id', $attackBookings);
        }

        $bookings = $bookings->whereHas('target', function ($q1) {
            $q1->whereHas('attack', function ($q2) {
                $q2->where('is_closed', 0);
                if ($this->attackId) {
                    $q2->where('attack_id', $this->attackId);
                }
            });
        });

        if ($this->id) {
            $booking = $bookings->find($this->id);

            $calc = App::make(MakeBattleCalc::class);
            $booking->target->calc = $calc->setX($booking->target->planet->x)
                ->setY($booking->target->planet->y)
                ->setZ($booking->target->planet->z)
                ->setLandTick($booking->land_tick)
                ->execute();

            if (!$booking->bg_id) $booking->bg_id = 0;

            $booking = $this->calculateFleets($booking);

            return $booking;
        }

        if ($this->battleGroupId) {
            $bookings = $bookings->where('bg_id', $this->battleGroupId);
        }

        $bookings = $bookings->get();
        $bookings->each(function ($booking, $key) {
            if (!$booking->bg_id) $booking->bg_id = 0;
            return $this->calculateFleets($booking);
        });

        return $bookings;
    }

    private function calculateFleets($booking)
    {
        $attackCount = 0;
        $attackFleets = 0;
        $defCount = 0;
        $defFleets = 0;
        $calc = App::make(MakeBattleCalc::class);

        $booking->auto_calc = $calc->setX($booking->target->planet->x)
            ->setY($booking->target->planet->y)
            ->setZ($booking->target->planet->z)
            ->setLandTick($booking->land_tick)
            ->execute();

        $attackMovements = FleetMovement::where([
            'planet_to_id' => $booking->target->planet->id,
            'land_tick' => $booking->land_tick,
            'mission_type' => 'Attack',
        ]);

        $attackCount = $attackMovements->sum('ship_count');
        $attackFleets = $attackMovements->count();

        $defMovements = FleetMovement::where([
            'planet_to_id' => $booking->target->planet->id,
            'land_tick' => $booking->land_tick,
            'mission_type' => 'Defend',
        ]);

        $defCount = $defMovements->sum('ship_count');
        $defFleets = $defMovements->count();

        $booking->attack_count = $attackCount;
        $booking->defence_count = $defCount;
        $booking->attack_fleets = $attackFleets;
        $booking->defence_fleets = $defFleets;

        return $booking;
    }
}
