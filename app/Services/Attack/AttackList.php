<?php

namespace App\Services\Attack;

use App\Alliance;
use App\Attack;
use Auth;

class AttackList
{
    private $query;

    private $isClosed;

    private $isOpened;

    private $isPrereleased;

    public function __construct()
    {
        $this->query = Attack::with([
            'targets' => function ($query) {
                $query->withCount(['bookings as claimed_count' => function ($q2) {
                    $q2->whereNotNull('user_id');
                }]);
            },
            'targets.bookings',
            'targets.planet' => function ($query) {
                $query->select(['id', 'alliance_id']);
            },
            'targets.planet.alliance' => function ($query) {
                $query->select(['id', 'name']);
            },
        ])
            ->withCount('bookings')
            ->withCount('targets')
            ->orderBy('land_tick', 'DESC');
    }

    public function setIsClosed($isClosed)
    {
        $this->isClosed = $isClosed;

        return $this;
    }

    public function setIsOpened($isOpened)
    {
        $this->isOpened = $isOpened;

        return $this;
    }

    public function setIsPrereleased($isPrereleased)
    {
        $this->isPrereleased = $isPrereleased;

        return $this;
    }

    public function execute()
    {
        if ($this->isClosed == 0 && $this->isOpened == 0 && $this->isPrereleased == 0) {
            if (Auth::user()->role->name != 'BC' && Auth::user()->role->name != 'Admin') {
                return response()->json(['error' => 'Not authorised.'], 422);
            }
        }

        if (isset($this->isClosed)) {
            $this->query = $this->query->where('is_closed', $this->isClosed);
        }

        if (isset($this->isOpened)) {
            $this->query = $this->query->where('is_opened', $this->isOpened);
        }

        if (isset($this->isPrereleased)) {
            $this->query = $this->query->where('is_prereleased', $this->isPrereleased);
        }

        $attacks = $this->query->get();

        $attacks = $this->addClaimedCountAndPercentage($attacks);

        if (isset($this->isClosed)) {
            $attacks = $this->addAlliances($attacks);
        }

        return $attacks;
    }

    private function addAlliances($attacks)
    {
        foreach ($attacks as $attack) {
            $alliances = [];
            foreach ($attack->targets as $target) {
                // Count alliance targets
                if (isset($target->planet->alliance)) {
                    if (! isset($alliances[$target->planet->alliance->name])) {
                        $alliances[$target->planet->alliance->name] = 0;
                    }
                    $alliances[$target->planet->alliance->name]++;
                }
            }
            $attack['alliances'] = $this->allianceCounts($alliances);
        }

        return $attacks;
    }

    private function allianceCounts($alliances)
    {
        $allianceCounts = [];
        $allies = Alliance::all()->keyBy('name')->toArray();
        uasort($alliances, function ($a, $b) {
            return 1;
        });
        foreach ($alliances as $alliance => $count) {
            $allianceCounts[] = [
                'name' => $alliance,
                'nickname' => $allies[$alliance]['nickname'] ?? str_replace(' ', '', substr($alliance, 0, 3)),
                'count' => $count,
            ];
        }

        return $allianceCounts;
    }

    private function addClaimedCountAndPercentage($attacks)
    {
        foreach ($attacks as $attack) {
            $attack->claimed_count = 0;
            foreach ($attack->targets as $target) {
                $attack->claimed_count = $attack->claimed_count + $target->claimed_count;
            }
            $attack->claimed_percentage = ($attack->bookings_count) ? number_format(($attack->claimed_count / $attack->bookings_count) * 100, 0) : 0;
        }

        return $attacks;
    }
}
