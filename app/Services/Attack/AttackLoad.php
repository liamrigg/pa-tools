<?php

namespace App\Services\Attack;

use App;
use App\Attack;
use App\AttackBooking;
use App\AttackTarget;
use App\Planet;
use App\Politics;
use App\Services\Misc\MakeBattleCalc;
use App\Tick;
use App\User;
use Auth;

class AttackLoad
{
    use AttackTrait;

    private $id;

    private $sort;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    public function execute()
    {
        $user = User::with('planet')->find(Auth::user()->id);

        if (! $user->planet) {
            return response()->json(['error' => 'You must set your co-ords to access attacks.'], 422);
        }

        $tick = Tick::orderBy('tick', 'DESC')->first();

        $attack = Attack::with([
            'targets' => function ($q) {
                $q->orderBy('order', 'ASC');
            },
            'targets.bookings',
            'targets.bookings.user' => function ($q) {
                $q->select(['id', 'name', 'planet_id']);
            },
            'targets.bookings.user.planet' => function ($q) {
                $q->select(['id', 'x', 'y', 'z']);
            },
            'targets.planet' => function ($q) {
                $q->select(['id', 'galaxy_id', 'x', 'y', 'z', 'value', 'race', 'score', 'size', 'amps', 'waves', 'nick', 'alliance_id', 'latest_p', 'latest_d', 'latest_u', 'latest_au', 'latest_j', 'growth_percentage_size', 'growth_percentage_value', 'growth_percentage_score']);
            },
            'targets.planet.latestP',
            'targets.planet.latestP.scan',
            'targets.planet.latestD',
            'targets.planet.latestD.scan',
            'targets.planet.latestJ',
            'targets.planet.latestJ.scan',
            'targets.planet.latestU',
            'targets.planet.latestU.u',
            'targets.planet.latestU.u.ship',
            'targets.planet.latestA',
            'targets.planet.latestA.au',
            'targets.planet.latestA.au.ship',
            'targets.planet.alliance' => function ($q) {
                $q->select(['id', 'name']);
            },
        ])->where('attack_id', $this->id)->first();

        $alliances = [];
        $bookings = [];

        foreach ($attack->targets as $target) {
            if (Auth::user()->role->name == 'Admin' || Auth::user()->role->name == 'BC') {
                if ($target->planet->alliance) {
                    if (! isset($alliances[$target->planet->alliance->name])) {
                        $alliances[$target->planet->alliance->name] = 0;
                    }
                    $alliances[$target->planet->alliance->name]++;
                }

                foreach ($target->bookings as $booking) {
                    if ($booking->user) {
                        if (! isset($bookings[$booking->user->name])) {
                            $bookings[$booking->user->name] = 0;
                        }
                        $bookings[$booking->user->name]++;
                    }
                }
            }

            $target->showShips = false;

            $anti = [
                'Fighter' => 'na',
                'Corvette' => 'na',
                'Frigate' => 'na',
                'Destroyer' => 'na',
                'Cruiser' => 'na',
                'Battleship' => 'na',
            ];

            $scan = null;

            $calc = App::make(MakeBattleCalc::class);
            $target->calc = $calc->setX($target->planet->x)
                ->setY($target->planet->y)
                ->setZ($target->planet->z)
                ->execute();

            $deet = '';

            if ($target->planet->latestA && $target->planet->latestA->tick >= ($tick->tick - 24)) {
                $scan = 'latestA';
                $deet = 'au';
            }
            if (((! $target->planet->latestA || $target->planet->latestA->tick < ($tick->tick - 24)) && $target->planet->latestU && $target->planet->latestU->tick >= ($tick->tick - 24))) {
                $scan = 'latestU';
                $deet = 'u';
            }

            if ($scan) {
                foreach ($target->planet->{$scan}->{$deet} as $ship) {
                    foreach ($anti as $class => $value) {
                        if ($ship->ship->t1 == $class) {
                            $anti[$class] = true;
                        }
                        if ($ship->ship->t2 == $class) {
                            $anti[$class] = true;
                        }
                        if ($ship->ship->t3 == $class) {
                            $anti[$class] = true;
                        }
                    }
                }

                foreach ($anti as $key => $value) {
                    if ($value === 'na') {
                        $anti[$key] = false;
                    }
                }
            }

            if (! empty($target->planet->latestP) && $target->planet->latestP->scan->tick >= ($tick->tick - 24)) {
                $metal = str_replace(',', '', $target->planet->latestP->res_metal);
                $crystal = str_replace(',', '', $target->planet->latestP->res_crystal);
                $eonium = str_replace(',', '', $target->planet->latestP->res_eonium);
                $resources = $metal + $crystal + $eonium;
                $target->planet->latestP->stock = number_shorten($resources, 1);
                $target->planet->latestP->prod_res = number_shorten(str_replace(',', '', $target->planet->latestP->prod_res), 1);
            }

            $target->anti = $anti;

            $wave = 1;

            $target->cap_xp = '';
            $target->cap_roids = '';
        }

        $attack = $attack->toArray();

        $galX = $user->planet->x;
        $galY = $user->planet->y;

        foreach ($attack['targets'] as $key => $target) {
            $attack['targets'][$key]['x'] = $target['planet']['x'];
            $attack['targets'][$key]['y'] = $target['planet']['y'];
            $attack['targets'][$key]['z'] = $target['planet']['z'];
            $attack['targets'][$key]['size'] = $target['planet']['size'];
            $attack['targets'][$key]['amps'] = $target['planet']['amps'];
            $attack['targets'][$key]['value'] = $target['planet']['value'];
            $attack['targets'][$key]['score'] = $target['planet']['score'];

            $politicalRelation = Politics::where('alliance_id', $target['planet']['alliance_id'])->first();

            if (is_object($politicalRelation)) {
                // calculate number of planets
                $attackBookingClass = AttackBooking::where('attack_id', $attack['id'])->whereNotNull('user_id')->get();
                foreach ($attackBookingClass as $keyInfo => $attackBooking) {
                    $planetId = AttackTarget::where('attack_id', $attack['id'])
                        ->where('id', $attackBooking->attack_target_id)
                        ->first()->planet_id;
                    $planetInfo = Planet::where('id', $planetId)->first();
                    if (! isset($planetUsed[$planetInfo->id])) {
                        if (! isset($maxPlanetsCount[$planetInfo->alliance_id])) {
                            $maxPlanetsCount[$planetInfo->alliance_id] = 0;
                        }
                        $maxPlanetsCount[$planetInfo->alliance_id]++;
                        $registeredPlanets[$planetInfo->id] = true;
                    }
                    $planetUsed[$planetInfo->id] = true;
                }
                if (isset($maxPlanetsCount[$target['planet']['alliance_id']])) {
                    $numberOfPlanets = $maxPlanetsCount[$target['planet']['alliance_id']];
                } else {
                    $numberOfPlanets = 0;
                }

                // calculate number of waves
                $attackTargetClass = AttackTarget::where('attack_id', $attack['id'])
                    ->where('planet_id', $target['planet']['id'])
                    ->first();
                $numberOfWaves = AttackBooking::where('attack_id', $attack['id'])
                    ->where('attack_target_id', $attackTargetClass->id)
                    ->whereNotNull('user_id')
                    ->count();

                if (
                    ($politicalRelation->status == 'nap') ||
                    ($numberOfPlanets >= $politicalRelation->max_planets && ! isset($registeredPlanets[$target['planet']['id']])) ||
                    ($numberOfWaves >= $politicalRelation->max_waves)
                ) {
                    $attack['targets'][$key]['closed'] = true;
                } else {
                    $attack['targets'][$key]['closed'] = false;
                }
            } else {
                $attack['targets'][$key]['closed'] = false;
            }

            // Hide own gal mates
            if ((Auth::user()->role->name != 'Admin' && Auth::user()->role->name != 'BC') && $target['planet']['x'] == $galX && $target['planet']['y'] == $galY) {
                unset($attack['targets'][$key]);
            }
        }

        if (Auth::user()->role->name == 'Admin' || Auth::user()->role->name == 'BC') {
            $books = [];

            uasort($bookings, function ($a, $b) {
                return $a < $b ? 1 : 0;
            });

            foreach ($bookings as $nick => $count) {
                $books[] = [
                    'nick' => $nick,
                    'count' => $count,
                ];
            }

            $attack['bookings'] = $books;

            $attack['alliances'] = $this->allianceCounts($alliances);
        }

        if ($this->sort) {
            $sortSymbol = substr($this->sort, 0, 1);
            $direction = $sortSymbol == '+' ? 'ASC' : 'DESC';

            $sort = substr($this->sort, 1);

            if ($sort == 'coords') {
                switch ($sortSymbol) {
                    case '+':
                        $attack['targets'] = array_orderby($attack['targets'], 'x', SORT_ASC, 'y', SORT_ASC, 'z', SORT_ASC);
                        break;
                    case '-':
                        $attack['targets'] = array_orderby($attack['targets'], 'x', SORT_DESC, 'y', SORT_DESC, 'z', SORT_DESC);
                        break;
                }
            } else {
                usort($attack['targets'], function ($a, $b) use ($sortSymbol, $sort) {
                    $return = '';
                    switch ($sortSymbol) {
                        case '+':
                            $return = ($a[$sort] > $b[$sort] ? 1 : -1);
                            break;
                        case '-':
                            ($return = $a[$sort] < $b[$sort] ? 1 : -1);
                            break;
                    }

                    return $return;
                });
            }
        }

        $attack['current_tick'] = $tick->tick;

        return collect($attack);
    }
}
