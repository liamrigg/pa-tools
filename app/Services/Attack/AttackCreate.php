<?php

namespace App\Services\Attack;

use App\Attack;
use App\Services\ParseCoordsList;
use App\Services\Bot\Notify;
use App;

class AttackCreate
{
    use AttackTrait;

    private $attackList;

    private $parseCoords;

    private $waves;

    private $landTick;

    private $time;

    private $targets;

    private $notes;

    private $prereleaseTime;

    public function __construct(AttackList $list, ParseCoordsList $coords)
    {
        $this->attackList = $list;
        $this->parseCoords = $coords;
    }

    public function setWaves($waves)
    {
        $this->waves = $waves;

        return $this;
    }

    public function setLandTick($landTick)
    {
        $this->landTick = $landTick;

        return $this;
    }

    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    public function setTargets($targets)
    {
        $this->targets = $targets;

        return $this;
    }

    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    public function setPrereleaseTime($prereleaseTime)
    {
        $this->prereleaseTime = $prereleaseTime;

        return $this;
    }

    public function execute()
    {
        $waves = $this->waves;
        $landTick = $this->landTick;
        $scheduleTime = $this->time;
        $targets = $this->targets;
        $attack = Attack::create([
            'land_tick' => $landTick,
            'waves' => $waves,
            'notes' => $this->notes,
            'is_opened' => (! $scheduleTime) ? true : false,
            'scheduled_time' => $this->time,
            'prerelease_time' => $this->prereleaseTime,
            'attack_id' => md5(uniqid()),
            'is_prereleased' => 0,
        ]);

        $attack->save();

        if ($attack->is_opened) {
            $url = App::make('url')->to('/').'/#/attacks/'.$attack->attack_id;
            $notify = App::make(Notify::class);
            $notify
                ->setMessage("Attack released: " . $url)
                ->setPinMessage(true)
                ->execute();
        }

        $targets = $this->parseCoords->setCoords($targets)->execute();

        $this->addTargets($attack, $targets, $landTick, $waves);
        $this->reorderTargets($attack->id);

        return $this->attackList->execute();
    }
}
