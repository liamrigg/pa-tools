<?php

namespace App\Services\Attack;

use App\Alliance;
use App\AttackBooking;
use App\AttackTarget;
use App\Planet;

trait AttackTrait
{
    private function reorderTargets($id)
    {
        $targets = AttackTarget::where('attack_id', $id)->get();

        foreach ($targets as $target) {
            $target->x = $target->planet->x;
            $target->y = $target->planet->y;
            $target->z = $target->planet->z;
        }

        $targets = array_orderby($targets->toArray(), 'x', SORT_ASC, 'y', SORT_ASC, 'z', SORT_ASC);

        $int = 1;

        foreach ($targets as $target) {
            AttackTarget::where('id', $target['id'])->update(['order' => $int++]);
        }
    }

    private function addTargets($attack, $targets, $landTick, $waves)
    {
        $attackTargets = [];

        foreach ($targets as $target) {
            $lt = $landTick;
            $planet = Planet::where(['x' => $target['x'], 'y' => $target['y'], 'z' => $target['z']])->first();

            if ($planet) {
                // check if planet already exists in this attack
                if (! $target = AttackTarget::where(['attack_id' => $attack->id, 'planet_id' => $planet->planet_id])->first()) {
                    $attackTarget = AttackTarget::create([
                        'attack_id' => $attack->id,
                        'planet_id' => $planet->id,
                    ]);

                    for ($x = $waves; $x > 0; $x--) {
                        AttackBooking::create([
                            'attack_id' => $attack->id,
                            'attack_target_id' => $attackTarget->id,
                            'land_tick' => $lt,
                        ]);

                        $lt++;
                    }
                }
            }
        }
    }

    private function allianceCounts($alliances)
    {
        $allianceCounts = [];

        $allies = Alliance::all()->keyBy('name')->toArray();

        uasort($alliances, function ($a, $b) {
            return 1;
        });

        foreach ($alliances as $alliance => $count) {
            $allianceCounts[] = [
                'name' => $alliance,
                'nickname' => $allies[$alliance]['nickname'] ?? str_replace(' ', '', substr($alliance, 0, 3)),
                'count' => $count,
            ];
        }

        return $allianceCounts;
    }
}
