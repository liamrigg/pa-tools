<?php

namespace App\Services\Attack;

use App\Attack;
use App\Services\ParseCoordsList;

class AttackUpdate
{
    use AttackTrait;

    private $id;

    private $attackList;

    private $parseCoords;

    private $landTick;

    private $targets;

    private $notes;

    public function __construct(AttackList $list, ParseCoordsList $coords)
    {
        $this->attackList = $list;
        $this->parseCoords = $coords;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setLandTick($landTick)
    {
        $this->landTick = $landTick;

        return $this;
    }

    public function setTargets($targets)
    {
        $this->targets = $targets;

        return $this;
    }

    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    public function execute()
    {
        $attack = Attack::with('targets', 'targets.bookings')->where('attack_id', $this->id)->first();

        $lt = $this->landTick;

        // lt change
        if ($lt != $attack->land_tick) {
            $diff = $lt - $attack->land_tick;

            $attack->land_tick = $lt;

            foreach ($attack->targets as $target) {
                foreach ($target->bookings as $booking) {
                    $booking->land_tick = $booking->land_tick + $diff;
                    $booking->save();
                }
            }
        }

        $attack->notes = $this->notes;
        $attack->save();

        if ($targets = $this->targets) {
            $targets = $this->parseCoords->setCoords($targets)->execute();

            $this->addTargets($attack, $targets, $attack->land_tick, $attack->waves);
            $this->reorderTargets($attack->id);
        }
    }
}
