<?php

namespace App\Services\Attack;

use App\Attack;

class AttackDelete
{
    private $id;

    private $attackList;

    public function __construct(AttackList $list)
    {
        $this->attackList = $list;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function execute()
    {
        Attack::where('id', $this->id)->delete();

        return $this->attackList->execute();
    }
}
