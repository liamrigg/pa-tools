<?php

namespace App\Services\Misc;

use App\Planet;
use App\Ship;
use Config;

class Bonus
{
    private int $tick;

    /**
     * Set the tick for the bonus calculations
     *
     * @param int $tick
     *
     * @return $this
     */
    public function setTick($tick): self
    {
        $this->tick = $tick;

        return $this;
    }

    public function execute()
    {
        $bonus = [];

        $bonus['resource_bonus'] = round(10000 + ($this->tick * 4800));
        $bonus['asteroid_bonus'] = round(6 + ($this->tick * 0.15));
        $bonus['research_bonus'] = round(4000 + ($this->tick * 24));
        $bonus['construction_bonus'] = round(2000 + ($this->tick * 18));
        $bonus['research_bonus_5percent'] = round((4000 + ($this->tick * 24)) * 1.05);
        $bonus['construction_bonus_5percent'] = round((2000 + ($this->tick * 18)) * 1.05);

        return $bonus;
    }
}
