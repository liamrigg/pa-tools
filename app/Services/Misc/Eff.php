<?php

namespace App\Services\Misc;

use App;
use App\Services\FindShip;
use App\Ship;

class Eff
{
    private $id;

    private $amount;

    private $name;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setName($ship)
    {
        $this->name = $ship;

        return $this;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    public function execute()
    {
        $ship = '';

        $amount = short2num($this->amount);

        if ($this->name) {
            $findShip = App::make(FindShip::class);

            $ship = $findShip
                ->setName($this->name)
                ->execute();

            // We didn't find a ship, return message
            if (is_string($ship)) {
                return $ship;
            }
        }

        if ($this->id) {
            $ship = Ship::find($this->id);
        }

        $damage = $ship->damage * $amount;
        $shots = $ship->guns * $amount;

        $shipValue = number_shorten((($ship->metal + $ship->crystal + $ship->eonium) * $amount) / 100, 1);

        $t1 = $ship->t1;
        $t2 = $ship->t2;
        $t3 = $ship->t3;

        $t1s = [];
        $t2s = [];
        $t3s = [];

        switch ($ship->type) {
            case 'EMP':
                $kill = 'hug';
                break;
            case 'Steal':
                $kill = 'steal';
                break;
            default:
                $kill = 'kill';
                break;
        }

        if ($t1 == 'Roids') {
            $killed = (int) ($damage / 50);
            $reply = sprintf('<b>%s %s (%s) will capture:</b> %s roids', number_format($amount), $ship->name, $shipValue, $killed);

            return $reply;
        }

        if ($t1 == 'Resources') {
            $killed = number_format((int) ($damage * 50));
            $reply = sprintf('<b>%s %s (%s) will steal:</b> %s total resources', number_format($amount), $ship->name, $shipValue, $killed);

            return $reply;
        }

        if ($t1 == 'Structure') {
            $killed = (int) ($damage / 500);
            $reply = sprintf('<b>%s %s (%s) will destroy:</b> %s structures', number_format($amount), $ship->name, $shipValue, $killed);

            return $reply;
        }

        $targets = $this->getTargets($ship);

        $reply = sprintf('<b>%s %s (%s) will %s:</b>', number_format($amount), $ship->name, $shipValue, $kill);

        $dmgMultiplier = [
            1 => 1,
            2 => 0.7,
            3 => 0.5,
        ];

        foreach ($targets as $t => $target) {
            $t++;

            $targetting = Ship::where('class', $target)->get();

            foreach ($targetting as $tgt) {
                $killed = 0;

                if ($ship->type != 'EMP') {
                    $killed = (int) ($damage / $tgt->armor);
                }

                if ($ship->type == 'EMP') {
                    $killed = (int) ($shots) * (100 - $tgt->empres) / 100;
                }

                $killed = $killed * $dmgMultiplier[$t];

                $value = number_shorten((($tgt->metal + $tgt->crystal + $tgt->eonium) * $killed) / 100, 1);

                $reply .= "\n".sprintf('%s %s (%s)', number_format($killed), $tgt->name, $value);
            }
        }

        return $reply;
    }

    private function getTargets($ship)
    {
        $targets = [
            $ship->t1,
        ];

        if ($ship->t2) {
            $targets[] = $ship->t2;
        }
        if ($ship->t3) {
            $targets[] = $ship->t3;
        }

        return $targets;
    }
}
