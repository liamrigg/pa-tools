<?php

namespace App\Services\Misc;

use App;
use App\Services\FindShip;
use App\Ship;
use Config;

class Cost
{
    private $id;

    private $name;

    private $amount;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    public function execute()
    {
        $ship = '';
        $amount = short2num($this->amount);

        if ($this->name) {
            $findShip = App::make(FindShip::class);

            $ship = $findShip->setName($this->name)
                ->execute();

            // We didn't find a ship, return message
            if (is_string($ship)) {
                return $ship;
            }
        }

        if ($this->id) {
            $ship = Ship::find($this->id);
        }

        $metal = number_shorten($ship->metal * $amount, 1);
        $crystal = number_shorten($ship->crystal * $amount, 1);
        $eonium = number_shorten($ship->eonium * $amount, 1);

        $reply = sprintf(
            "Buying %s %s will cost %s metal, %s crystal and %s eonium.\n",
            number_format($amount),
            $ship->name,
            $metal,
            $crystal,
            $eonium
        );

        $governments = Config::get('governments');

        foreach ($governments as $name => $gov) {
            $metal = number_shorten($ship->metal * (1 + $gov['prod_cost']) * $amount, 1);
            $crystal = number_shorten($ship->crystal * (1 + $gov['prod_cost']) * $amount, 1);
            $eonium = number_shorten($ship->eonium * (1 + $gov['prod_cost']) * $amount, 1);

            $reply .= sprintf(
                "%s: %s metal, %s crystal and %s eonium.\n",
                $name,
                $metal,
                $crystal,
                $eonium
            );
        }

        return $reply;
    }
}
