<?php

namespace App\Services\Misc;

use App\Ship;

class GetRaces
{
    private $short = [
        'Terran' => 'Ter',
        'Cathaar' => 'Cat',
        'Xandathrii' => 'Xan',
        'Zikonian' => 'Zik',
        'Kinthia' => 'Kin',
        'Eitraides' => 'Etd',
        'Slythonian' => 'Sly',
    ];

    public function execute()
    {
        $races = [];

        $ships = Ship::select('race')->get()->unique('race');

        $int = 1;

        foreach ($ships as $ship) {
            $races[$this->short[$ship->race]] = $int;
            $int++;
        }

        return $races;
    }
}
