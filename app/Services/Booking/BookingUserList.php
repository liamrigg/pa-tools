<?php

namespace App\Services\Booking;

use App\AttackBooking;
use App\User;

class BookingUserList
{
    protected $id;

    protected $userId;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setUserId($id)
    {
        $this->userId = $id;

        return $this;
    }

    public function users()
    {
        $booking = AttackBooking::with('users')->find($this->id);

        $users = [$booking->user_id];

        foreach ($booking->users as $user) {
            $users[] = $user->id;
        }

        return User::whereIn('id', $users)->get();
    }

    public function nonUsers()
    {
        $bgUsers = [];
        $booking = AttackBooking::with('users')->find($this->id);

        $bookingUsers = [$booking->user_id];

        foreach ($booking->users as $user) {
            $bookingUsers[$user->id] = $user->id;
        }

        if ($booking->bg_id) {
            $bgUsers = User::whereHas('battleGroups', function ($q) use ($booking) {
                $q->where('battlegroups.id', $booking->bg_id);
            })
                ->where('is_enabled', 1)
                ->orderBy('name')
                ->get();
        }

        $users = User::whereHas('planet')
            ->where('is_enabled', 1)
            ->orderBy('name');

        $users = $users->whereNotIn('id', $bookingUsers);

        if ($bgUsers) {
            $users = $users->whereIn('id', $bgUsers->pluck('id'));
        }
        
        return $users->get();
    }

    public function bgUsers()
    {
        $booking = AttackBooking::find($this->id);

        if ($booking->bg_id) {
            return User::whereHas('battleGroups', function ($q) use ($booking) {
                $q->where('battlegroups.id', $booking->bg_id);
            })
                ->where('is_enabled', 1)
                ->orderBy('name')
                ->get();
        }

        return collect();
    }
}
