<?php

namespace App\Services\Booking;

use App\AttackBooking;
use App\User;
use Longman\TelegramBot\Request;

class ChangeBookingStatus
{
    private $id;

    private $newStatus;

    private $user;

    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function setNewStatus($status): self
    {
        $this->newStatus = $status;

        return $this;
    }

    public function execute()
    {
        $booking = AttackBooking::with('users', 'target', 'target.planet')->findOrFail($this->id);

        $status = $booking->status;

        if ($status != $this->newStatus) {
            $booking->status = $this->newStatus;
            $booking->save();

            $bookingUsers = $booking->users->pluck('id');
            $bookingUsers[] = $booking->user_id;

            foreach ($bookingUsers as $userId) {
                if ($userId != $this->user->id) {
                    $user = User::find($userId);
                    if ($user->telegram_user_id) {
                        $message = sprintf(
                            '%s has changed the status of your booking on %d:%d:%d LT%d to %s',
                            $this->user->name,
                            $booking->target->planet->x,
                            $booking->target->planet->y,
                            $booking->target->planet->z,
                            $booking->land_tick,
                            strtoupper($booking->status)
                        );

                        $data = [
                            'chat_id' => $user->telegram_user_id,
                            'text' => $message,
                        ];

                        Request::sendMessage($data);
                    }
                }
            }
        }
    }
}
