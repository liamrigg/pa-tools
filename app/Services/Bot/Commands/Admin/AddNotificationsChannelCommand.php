<?php

namespace App\Services\Bot\Commands\Admin;

use App\Services\Bot\BaseCommand;
use App\Setting;

class AddNotificationsChannelCommand extends BaseCommand
{
    protected $command = 'addnotificationschannel';

    public static $help = 'Sets this channel to receive notifications from webby.';

    protected $requireBotAdmin = true;

    /**
     * Execute the AddNotificationsChannelCommand
     */
    public function handle(): string
    {
        Setting::where('name', 'tg_notifications_channel')->update(['value' => $this->botChatId]);

        return 'This channel has been set to receive tick notifications.';
    }
}
