<?php

namespace App\Services\Bot\Commands\Admin;

use App\Services\Bot\BaseCommand;
use App\Setting;

class AddScansChannelCommand extends BaseCommand
{
    protected $command = 'addscanschannel';

    public static $help = 'Sets this channel to receive scan requests.';

    protected $requireBotAdmin = true;

    /**
     * Execute the AddScansChannelCommand
     */
    public function handle(): string
    {
        Setting::where('name', 'tg_scans_channel')
            ->update(['value' => $this->botChatId]);

        return 'This channel has been set to receive scan requests.';
    }
}
