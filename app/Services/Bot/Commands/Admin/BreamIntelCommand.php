<?php

namespace App\Services\Bot\Commands\Admin;

use App\Services\Bot\BaseCommand;
use App\User;

class BreamIntelCommand extends BaseCommand
{
    protected $command = 'bream';

    protected $requireWebAdmin = true;

    public function handle(): string
    {
        return 'Not implemented!';

        $user = User::find($this->userId);

        $link = 'http://dummschueler.de/dummy.php?ally='.$this->text;

        try {
            $html = file_get_contents($link);
        } catch (\Exception $e) {
            $response = "Couldn't read url!";

            return $response;
        }

        $lines = explode("\n", $html);

        return $lines[0];
    }
}
