<?php

namespace App\Services\Bot\Commands\Admin;

use App\Services\Bot\BaseCommand;
use Artisan;

class ParseShipsCommand extends BaseCommand
{
    protected $command = 'reloadstats';

    protected $requireWebAdmin = true;

    public static $help = 'Reloads the ship stats on webby.';

    /**
     * Handle the ParseShipsCommand
     */
    public function handle(): string
    {
        Artisan::call('parse::shipstats');

        return 'Loaded stats!';
    }
}
