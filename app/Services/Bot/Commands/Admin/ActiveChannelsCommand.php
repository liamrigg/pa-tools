<?php

namespace App\Services\Bot\Commands\Admin;

use App\BotChat;
use App\Services\Bot\BaseCommand;

class ActiveChannelsCommand extends BaseCommand
{
    protected $command = 'activechannels';

    public static $help = 'Shows a list of all channels where the bot has been active in the last 7 days';

    protected $requireWebAdmin = true;

    /**
     * Handle the ActiveChannels command
     */
    public function handle(): string
    {
        $days = date('Y-m-d H:i:s', strtotime('-7 days'));
        $channels = BotChat::where('updated_at', '>', $days)->where('type', 'group')->get();
        $response = "In the last 7 days I've seen activity in the following channels:".PHP_EOL.PHP_EOL;

        $int = 1;

        foreach ($channels as $channel) {
            $response .= $int.'. '.$channel->title.' ('.$channel->id.')'.PHP_EOL;
            $int++;
        }

        return $response;
    }
}
