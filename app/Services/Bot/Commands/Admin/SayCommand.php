<?php

namespace App\Services\Bot\Commands\Admin;

use App;
use App\Services\Bot\BaseCommand;
use App\Services\Bot\Notify;

class SayCommand extends BaseCommand
{
    protected $command = 'say';

    protected $requireWebAdmin = true;

    public static $help = 'Make the bot say something to the notifications channel - usage: !say [message].';

    /**
     * Handle the SayCommand
     */
    public function handle(): string
    {
        $notify = App::make(Notify::class);

        $notify
            ->setMessage(ucfirst($this->text))
            ->execute();

        return 'Message sent to notification channel.';
    }
}
