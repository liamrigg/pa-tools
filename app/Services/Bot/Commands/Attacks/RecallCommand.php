<?php

namespace App\Services\Bot\Commands\Attacks;

use App;
use App\AttackBooking;
use App\Services\Booking\ChangeBookingStatus;
use App\Services\Bot\BaseCommand;
use App\User;

class RecallCommand extends BaseCommand
{
    protected $command = 'recall';

    protected $requireWebUser = true;

    public static $help = 'Sets an attack booking status to RECALL';

    /**
     * Handle the RecallCommand
     */
    public function handle(): string
    {
        $user = User::find($this->userId);

        $string = explode(' ', $this->text);

        if (! isset($string[0])) {
            return 'usage: !recall [booking_id]';
        }

        $booking = AttackBooking::find($string[0]);

        if (! $booking) {
            return 'No booking found with that id';
        }

        if ($booking->status == 'recall') {
            return 'That booking is already set to RECALL';
        }

        if ($booking->user_id != $user->id && ! $booking->users->where('id', $user->id)->count()) {
            return "You can't modify a booking that isn't yours";
        }

        $changeStatus = App::make(ChangeBookingStatus::class);
        $changeStatus->setId($string[0])
            ->setUser($user)
            ->setNewStatus('recall')
            ->execute();

        return 'Set booking to RECALL!';
    }
}
