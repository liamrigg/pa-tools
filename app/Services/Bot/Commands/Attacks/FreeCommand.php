<?php

namespace App\Services\Bot\Commands\Attacks;

use App\Attack;
use App\AttackBooking;
use App\AttackTarget;
use App\Planet;
use App\Services\Bot\BaseCommand;

class FreeCommand extends BaseCommand
{
    protected $command = 'free';

    protected $requireWebUser = true;

    public function handle(): string
    {
        $string = explode(' ', $this->text);

        // setup vars
        $target = [];
        $available = [];

        // process free planets
        $attacks = Attack::where(['is_opened' => 1, 'is_closed' => 0])->get();
        if (count($attacks) > 0) {
            $reply = 'Available targets in Open Attacks:'.PHP_EOL.PHP_EOL;
        } else {
            return 'No open attacks';
        }

        foreach ($attacks as $attack) {
            $bookings = AttackBooking::where('attack_id', $attack->id)->get();

            foreach ($bookings as $booking) {
                if (! isset($booking->user_id) && $booking->user_id == null) {

                    // get planet
                    $attackTarget = AttackTarget::where('attack_id', $attack->id)->where('id', $booking->attack_target_id)->first();

                    if ($attackTarget) {
                        // store for later
                        $target[$booking->attack_target_id] = Planet::where('id', $attackTarget->planet_id)->first();
                        $planetId = $target[$booking->attack_target_id]->id;
                        $available[$planetId][] = sprintf('%s', $booking->land_tick);
                    }
                }
            }
        }

        if (! $available) {
            return 'No available targets';
        }

        // process available fleets
        foreach ($available as $planetId => $list) {
            $planet = Planet::where('id', $planetId)->first();
            $reply .= sprintf('%s:%s:%s - ', $planet->x, $planet->y, $planet->z);

            $reply .= implode(' | ', $list);

            $reply .= PHP_EOL;
        }

        return $reply.PHP_EOL.'Use !book [x:y:z] [land_tick] to claim'.PHP_EOL;
    }
}
