<?php

namespace App\Services\Bot\Commands\Attacks;

use App\Alliance;
use App\Planet;
use App\Services\Bot\BaseCommand;
use App\User;

class BashCommand extends BaseCommand
{
    protected $command = 'bash';

    protected $requireWebUser = true;

    public static $help = 'Tells you what your bash limit is - usage: !bash [optional: alliance]';

    public function handle(): string
    {
        $string = explode(' ', $this->text);

        $user = User::with('planet')->find($this->userId);

        if (! $user->planet) {
            return "You haven't set your coords on webby, use !setplanet <x:y:z>.";
        }

        $planet = $user->planet;

        $score = $planet->score;
        $value = $planet->value;

        $min_value = ceil(0.5 * $value);
        $min_score = ceil(0.6 * $score);

        $reply = 'You cannot attack planets with less than '.number_shorten($min_value).' in value and '.number_shorten($min_score).' in score.'.PHP_EOL;

        $ally = ($string[0]) ?? null;

        if ($ally) {
            $alliance = Alliance::where('name', 'like', '%'.$ally.'%')->orWhere('nickname', $ally)->first();

            if (isset($alliance)) {
                $planets = Planet::where([
                    ['alliance_id', $alliance->id],
                    ['value', '>', $min_value],
                    ['score', '>', $min_score],
                ])->orderBy('x', 'ASC')->orderBy('y', 'ASC')->orderBy('z', 'ASC')->get();

                $data = [];
                $count_planets = count($planets);

                foreach ($planets as $planet) {
                    $data[] = $planet->x.':'.$planet->y.':'.$planet->z;
                }
                $reply = $reply."\n".'You can attack the following planets ('.$count_planets.') in alliance '.$alliance->name.':'."\n\n".implode(' ', $data);
            } else {
                return 'No alliance found for: '.$string[0];
            }
        }

        return $reply;
    }
}
