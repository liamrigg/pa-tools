<?php

namespace App\Services\Bot\Commands\Attacks;

use App\AttackBooking;
use App\Services\Bot\BaseCommand;
use App\User;

class NotesCommand extends BaseCommand
{
    protected $command = 'notes';

    protected $requireWebUser = true;

    public static $help = 'Sets notes for an attack booking - usage: !notes [booking_id] [notes]';

    /**
     * Handle the NotesCommand
     */
    public function handle(): string
    {
        $user = User::find($this->userId);

        $string = explode(' ', $this->text);

        if (! isset($string[0]) || ! isset($string[1])) {
            return 'usage: !notes [booking_id] [notes]';
        }

        //dd($this->text, $string[0]);

        $notes = str_replace($string[0].' ', '', $this->text);

        $booking = AttackBooking::find($string[0]);

        if (! $booking) {
            return 'No booking found with that id';
        }

        if ($booking->user_id != $user->id && ! $booking->users->where('id', $user->id)->count()) {
            return "You can't modify a booking that isn't yours";
        }

        $booking->notes = $notes;
        $booking->save();

        return 'Set booking notes to "'.$booking->notes.'"';
    }
}
