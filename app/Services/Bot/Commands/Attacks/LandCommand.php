<?php

namespace App\Services\Bot\Commands\Attacks;

use App;
use App\AttackBooking;
use App\Services\Booking\ChangeBookingStatus;
use App\Services\Bot\BaseCommand;
use App\User;

class LandCommand extends BaseCommand
{
    protected $command = 'land';

    protected $requireWebUser = true;

    public static $help = 'Sets an attack booking status to LAND';

    /**
     * Handle the LaunchCommand
     */
    public function handle(): string
    {
        $user = User::find($this->userId);

        $string = explode(' ', $this->text);

        if (! isset($string[0])) {
            return 'usage: !land [booking_id]';
        }

        $booking = AttackBooking::find($string[0]);

        if (! $booking) {
            return 'No booking found with that id';
        }

        if ($booking->status == 'land') {
            return 'That booking is already set to LAND';
        }

        if ($booking->user_id != $user->id && ! $booking->users->where('id', $user->id)->count()) {
            return "You can't modify a booking that isn't yours";
        }

        $changeStatus = App::make(ChangeBookingStatus::class);
        $changeStatus->setId($string[0])
            ->setUser($user)
            ->setNewStatus('land')
            ->execute();

        return 'Set booking to LAND!';
    }
}
