<?php

namespace App\Services\Bot\Commands\Attacks;

use App;
use App\Services\Bot\BaseCommand;
use App\Services\MyBookings;
use App\Tick;
use App\User;
use Carbon\Carbon;

class BookingsCommand extends BaseCommand
{
    protected $command = 'bookings';

    protected $requireWebUser = true;

    public static $help = 'Shows your claimed targets';

    /**
     * Handle the LaunchCommand
     */
    public function handle(): string
    {
        $user = User::find($this->userId);

        $currentTick = Tick::orderBy('tick', 'DESC')->first();

        // get bookings
        $reply = '';
        $myBookings = App::make(MyBookings::class);

        $bookings = $myBookings
            ->setUserId($user->id)
            ->execute();

        foreach ($bookings as $booking) {
            $users = $booking->users->pluck('name')->toArray();
            array_unshift($users, $booking->user->name);
            $target = $booking->target;
            $planet = $target->planet;
            $eta = $booking->land_tick - $currentTick->tick;
            $fleets = '';
            if (
                ($booking->attack_fleets || $booking->defence_fleets)
                 && (
                     $booking->target->planet->latestJ
                     && $booking->target->planet->latestJ->scan->tick > $currentTick->tick - 5
                 )
            ) {
                $fleets = sprintf(
                    ' | fleets: %d attack / %d defence',
                    $booking->attack_fleets,
                    $booking->defence_fleets
                );
            }
            $teamup = '';
            if (count($users) > 1) {
                $teamup = ' | teamup: '.implode(', ', $users);
            }
            $notes = '';
            if ($booking->notes) {
                $notes = ' | notes: '.$booking->notes;
            }
            $bcalc = '';
            if ($booking->battle_calc) {
                $bcalc = " <a href='".$booking->battle_calc."'>manual</a>";
            }
            $time = Carbon::parse(Carbon::now($user->timezone)->startOfHour());
            $travelTicks = $booking->land_tick - $currentTick->tick;
            $landingTime = date('H:i', strtotime('+'.$travelTicks.' hours', strtotime($time)));
            $reply .= sprintf(
                'id: %s | %s:%s:%s [%s] eta %s (LT%d %s) | calc: %s%s%s%s%s | status: <b>%s</b>'.PHP_EOL.PHP_EOL,
                "<a href='".url('/').'/#/attacks/'.$booking->target->attack->attack_id.'/booking/'.$booking->id."'>".$booking->id.'</a>',
                $planet->x,
                $planet->y,
                $planet->z,
                $booking->target->planet->race,
                $eta,
                $booking->land_tick,
                $landingTime,
                "<a href='".$booking->auto_calc."'>auto</a>",
                $bcalc,
                $teamup,
                $fleets,
                $notes,
                strtoupper($booking->status)
            );
        }

        if (strlen($reply) > 0) {
            $reply .= 'use !drop [id] to drop booking';
            $reply .= PHP_EOL.'use !land [id] or !recall [id] to change booking status';
            $reply .= PHP_EOL.'use !share [id] [user] to share booking with another user';
            $reply .= PHP_EOL.'use !notes [id] [notes] to add notes to booking';
            $reply .= PHP_EOL.'use !owner [id] [user] to change the owner of a booking';
        }

        return strlen($reply) > 0 ? $reply : 'No bookings found';
    }
}
