<?php

namespace App\Services\Bot\Commands\Attacks;

use App\Attack;
use App\AttackBooking;
use App\AttackTarget;
use App\Planet;
use App\Services\Bot\BaseCommand;
use App\User;

class BookCommand extends BaseCommand
{
    protected $command = 'book';

    protected $requireWebUser = true;

    public function handle(): string
    {
        $string = explode(' ', $this->text);

        // validate
        if (! isset($string[0]) || ! isset($string[1])) {
            return 'Usage: !book [x:y:z] [lt]';
        }

        // setup vars
        if (isset($string[3])) {
            $coords = [$string[0], $string[1], $string[2]];
            $landingTick = $string[3];
        } else {
            $coords = explode(':', $string[0]);
            $landingTick = $string[1];
        }

        // retrieve planet
        $planet = Planet::where('x', $coords[0])->where('y', $coords[1])->where('z', $coords[2])->first();

        // check planet exists
        if (empty($planet)) {
            return 'Could not find coords';
        }

        // check planet is available
        $attacks = Attack::where(['is_opened' => 1, 'is_closed' => 0])->get();

        foreach ($attacks as $attack) {
            $bookings = AttackBooking::where('attack_id', $attack->id)->where('user_id', null)->get();

            foreach ($bookings as $booking) {
                $attackTarget = AttackTarget::where('attack_id', $attack->id)
                    ->where('id', $booking->attack_target_id)
                    ->get();

                foreach ($attackTarget as $target) {
                    $planet = Planet::find($target->planet_id);

                    if ($landingTick == $booking->land_tick && $planet->x == $coords[0] && $planet->y == $coords[1] && $planet->z == $coords[2]) {
                        if ($booking->user_id == null) {
                            $user = User::find($this->userId);
                            if ($user) {
                                $reply = sprintf(
                                    'Target booked (%s:%s:%s LT%s)',
                                    $coords[0],
                                    $coords[1],
                                    $coords[2],
                                    $landingTick
                                );
                                $booking->user_id = $user['id'];
                                $booking->save();
                            }
                        }
                    }
                }
            }
        }

        return isset($reply) ? $reply : 'Target not available';
    }
}
