<?php

namespace App\Services\Bot\Commands\Attacks;

use App\AttackBooking;
use App\Services\Bot\BaseCommand;
use App\User;

class OwnerCommand extends BaseCommand
{
    protected $command = 'owner';

    protected $requireWebUser = true;

    public static $help = 'Changes the owner for a booking - usage: !owner [booking_id] [user]';

    /**
     * Handle the OwnerCommand
     */
    public function handle(): string
    {
        $user = User::find($this->userId);

        $string = explode(' ', $this->text);

        if (! isset($string[0]) || ! isset($string[1])) {
            return 'usage: !owner [booking_id] [user]';
        }

        $booking = AttackBooking::find($string[0]);

        if (! $booking) {
            return 'No booking found with that id';
        }

        if ($booking->user_id != $user->id) {
            return "You can't change the owner of a booking you don't own";
        }

        if (! $addUser = $this->findUser($string[1])) {
            return 'Can not find that user';
        }

        if (! $booking->users->where('id', $addUser->id)->count()) {
            return 'You must share this booking with that user first - !share [booking_id] [user]';
        }

        if (! $addUser) {
            return 'Can not find a user with that name';
        }

        $currentUser = $booking->user_id;

        $booking->user_id = $addUser->id;
        $booking->save();

        $booking->users()->detach($addUser->id);
        $booking->users()->syncWithoutDetaching([$currentUser]);

        return 'Changed the owner to '.$addUser->name;
    }
}
