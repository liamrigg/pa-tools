<?php

namespace App\Services\Bot\Commands\Attacks;

use App;
use App\AttackBooking;
use App\Services\Booking\BookingUser;
use App\Services\Bot\BaseCommand;
use App\User;
use Longman\TelegramBot\Request;

class ShareCommand extends BaseCommand
{
    protected $command = 'share';

    protected $requireWebUser = true;

    public static $help = 'Share a booking with another user';

    /**
     * Handle the ShareCommand
     */
    public function handle(): string
    {
        $user = User::find($this->userId);

        $string = explode(' ', $this->text);

        if (! isset($string[0]) && ! isset($string[1])) {
            return 'usage: !share [booking_id] [user]';
        }

        $booking = AttackBooking::find($string[0]);

        if (! $booking) {
            return 'No booking found with that id';
        }

        if ($booking->user_id != $user->id) {
            return "You can't share a booking if you aren't the owner";
        }

        $addUser = $this->findUser($string[1]);

        if (! $addUser) {
            return 'Can not find a user with that name';
        }

        if ($booking->user_id == $addUser->id || $booking->users->where('id', $addUser->id)->count()) {
            return 'Booking already shared with that user';
        }

        $bookingUser = App::make(BookingUser::class);
        $bookingUser
            ->setId($booking->id)
            ->setUserId($addUser->id)
            ->add();

        $message = sprintf(
            '%s has shared a target with you: %d:%d:%d LT%d',
            $user->name,
            $booking->target->planet->x,
            $booking->target->planet->y,
            $booking->target->planet->z,
            $booking->land_tick
        );

        $data = [
            'chat_id' => $addUser->telegram_user_id,
            'text' => $message,
        ];

        Request::sendMessage($data);

        return sprintf(
            'Added user %s to booking %d',
            $addUser->name,
            $booking->id
        );
    }
}
