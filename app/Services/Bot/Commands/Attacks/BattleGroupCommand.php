<?php

namespace App\Services\Bot\Commands\Attacks;

use App;
use App\Attack;
use App\AttackBooking;
use App\AttackTarget;
use App\BattleGroup;
use App\BattleGroupUser;
use App\DevelopmentScan;
use App\JgpScan;
use App\Planet;
use App\Scan;
use App\Services\Bot\BaseCommand;
use App\Tick;
use App\User;

class BattleGroupCommand extends BaseCommand
{
    protected $command = 'bg';

    protected $requireWebUser = true;

    public function handle(): string
    {
        if (! $this->text) {
            return 'Usage: !bg <members|targets>';
        }

        $currentTick = Tick::orderBy('tick', 'DESC')->first();
        $tick = $currentTick->tick;
        $user = User::find($this->userId);
        $bgu = BattleGroupUser::where('user_id', $user->id)->first();

        if (! isset($bgu)) {
            return "It appears you're not in a BG?";
        }

        $bgId = $bgu->battlegroup_id;

        $bg = BattleGroup::with([
            'members',
            'members.planet',
        ])->find($bgId);

        if ($this->text == 'members') {
            $members = '';
            $members_coords = '';
            foreach ($bg->members as $member) {
                $race = 'Unknown';
                if (isset($member->planet->race)) {
                    $race = $member->planet->race;
                }
                $dists = '?';
                if (isset($member->planet->dists)) {
                    $dists = $member->planet->dists;
                }
                $members .= $member->name.' ('.$member->planet->x.':'.$member->planet->y.':'.$member->planet->z.' - '.$race.' - D:'.$dists.")\n";
                $members_coords .= $member->planet->x.':'.$member->planet->y.':'.$member->planet->z.' ';
            }

            return $bg->name." - Active Members\n\n".$members."\nLazy people scan list:\n".$members_coords;
        }

        if ($this->text == 'targets') {
            $ATO = Attack::where('is_closed', 0)->get();

            if (! count($ATO)) {
                return "It appears there's no open attacks.";
            }

            $targets = '';
            foreach ($ATO as $attack) {
                $Bookings = AttackBooking::where('attack_id', $attack->id)->whereIn('user_id', $bg->members->pluck('id'))->get();

                if (! count($Bookings)) {
                    return "It appears there's no targets claimed.";
                }

                foreach ($Bookings as $target) {
                    $claimed_id = $target->user_id;
                    $claimed_by = User::where('id', $claimed_id)->pluck('name');
                    $target_id = $target->attack_target_id;
                    $target_lt = $target->land_tick;
                    $eta = $target_lt - $tick;
                    $target_attid = Attack::where('is_closed', 0)->where('id', $target->attack_id)->pluck('attack_id');
                    $target_bookid = $target->id;
                    $booking_url = App::make('url')->to('/').'/#/attacks/'.$target_attid[0].'/booking/'.$target_bookid;
                    if (isset($target->notes)) {
                        $target_notes = str_replace('&', '&amp;', $target->notes);
                        $target_notes = str_replace('<', '<', $target_notes);
                        $target_notes = str_replace('>', '>', $target_notes);
                    } else {
                        $target_notes = 'None';
                    }
                    if (isset($target->battle_calc)) {
                        $target_bcalc_name = 'B';
                        $target_bcalc_url = $target->battle_calc;
                    } else {
                        $target_bcalc_name = '<s>B</s>';
                        $target_bcalc_url = '#';
                    }
                    $planet_id = AttackTarget::where('id', $target_id)->pluck('planet_id');
                    $planet = Planet::where('id', $planet_id[0])->get();
                    $planet_url = App::make('url')->to('/').'/#/planets/'.$planet_id[0];
                    $target_x = $planet[0]->x;
                    $target_y = $planet[0]->y;
                    $target_z = $planet[0]->z;
                    $target_size = $planet[0]->size;
                    $target_planet_name = $planet[0]->planet_name;
                    $target_ruler_name = $planet[0]->ruler_name;
                    $target_latestp = $planet[0]->latest_p;
                    $target_latestd = $planet[0]->latest_d;
                    $target_latesta = $planet[0]->latest_au;
                    $target_latestj = $planet[0]->latest_j;

                    $dscan = DevelopmentScan::with('scan')->where('id', $target_latestd)->first();
                    if (isset($dscan)) {
                        $scan_types = [0 => 'P', 1 => 'L', 2 => 'D', 3 => 'U', 4 => 'N', 5 => 'I', 6 => 'J', 7 => 'A', 8 => 'M'];
                        $scan_type = $scan_types[$dscan->waves];
                        $dscan_amps = $dscan->wave_amplifier;
                        $dscan_age = $tick - $dscan->scan->tick;
                    } else {
                        $scan_type = '?';
                        $dscan_amps = '?';
                        $dscan_age = '?';
                    }
                    $jscan = JgpScan::with('scan')->where('id', $target_latestj)->first();
                    if (isset($jscan)) {
                        $jscan_name = 'J';
                        $jscan_url = $jscan->scan->link;
                        $jscan_age = $tick - $jscan->scan->tick;
                    } else {
                        $jscan_name = '<s>J</s>';
                        $jscan_url = '#';
                        $jscan_age = '#';
                    }

                    $targets .= "<a href='".$booking_url."'>".$target_x.':'.$target_y.':'.$target_z."</a> - <a href='".$jscan_url."'>".$jscan_name.'</a>('.$jscan_age.") <a href='".$target_bcalc_url."'>".$target_bcalc_name.'</a> - '.$dscan_amps.'/'.$scan_type.'('.$dscan_age.') - Size: '.$target_size."\n<strong>LT</strong>: ".$target_lt.' (eta '.$eta.') <strong>Notes</strong>: '.$target_notes."\n\n";
                }
            }

            return $bg->name." - Active Targets\n\n".$targets;
        }

        if ($this->text == 'ships') {
            $response = '';
            foreach ($bg->members as $member) {
                $latest_au = $member->planet->latest_au;
                $ascan = Scan::with('au')->where('id', $latest_au)->first();
                if (isset($ascan)) {
                    $ascan_url = $ascan->link;
                    $ascan_age = $tick - $ascan->tick;
                } else {
                    $ascan_url = '#';
                    $ascan_age = '#';
                }

                $response .= "<a href='".$ascan_url."'>".$member->name.' (Age: '.$ascan_age.")</a>\n";
            }

            return $bg->name." - Latest AU Scans\n\n".$response;
        }

        return 'Usage: !bg <members|targets|ships>';
    }
}
