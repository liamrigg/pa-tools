<?php

namespace App\Services\Bot\Commands\Attacks;

use App\AttackBooking;
use App\Services\Bot\BaseCommand;
use App\User;

class DropCommand extends BaseCommand
{
    protected $command = 'drop';

    protected $requireWebUser = true;

    public static $help = 'Drop a booking - usage: !drop [id]';

    public function handle(): string
    {
        $string = explode(' ', $this->text);

        $user = User::find($this->userId);

        // validate
        if (! isset($string[0])) {
            return 'Usage: !drop [id]';
        }

        if (! $booking = AttackBooking::find($string[0])) {
            return 'No booking found with that id';
        }

        if ($booking->user_id != $user->id && ! $booking->users->where('id', $user->id)->count()) {
            return "You can't drop a booking that isn't yours";
        }

        if ($booking->user_id == $user->id && $booking->users->count()) {
            return 'You must change owner before dropping that booking';
        }

        if ($booking->user_id == $user->id) {
            $booking->user_id = null;
            $booking->save();
        }

        if ($booking->users->where('id', $user->id)->count()) {
            $booking->users()->detach($user->id);
        }

        return 'Booking dropped';
    }
}
