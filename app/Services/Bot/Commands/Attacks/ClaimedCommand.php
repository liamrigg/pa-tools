<?php

namespace App\Services\Bot\Commands\Attacks;

use App\Attack;
use App\AttackBooking;
use App\Services\Bot\BaseCommand;

class ClaimedCommand extends BaseCommand
{
    protected $command = 'claimed';

    protected $requireWebUser = true;

    public static $help = 'Shows a list of all claimed targets.';

    /**
     * Handle the ClaimedCommand
     */
    public function handle(): string
    {
        $attacks = Attack::where(['is_opened' => 1, 'is_closed' => 0])->get();

        $claimed = [];

        if (count($attacks) > 0) {
            $reply = 'Claimed targets: ';
        } else {
            return 'No open attacks';
        }

        foreach ($attacks as $attack) {
            $bookings = AttackBooking::with(['target', 'target.planet'])->where('attack_id', $attack->id)
                ->whereNotNull('user_id')->get();

            foreach ($bookings as $booking) {
                $coords = $booking->target->planet->x.':'.$booking->target->planet->y.':'.$booking->target->planet->z;
                $claimed[] = $coords;
            }
        }

        $claimed = array_unique($claimed);

        return $reply.implode(' ', $claimed);
    }
}
