<?php

namespace App\Services\Bot\Commands\Attacks;

use App\Attack;
use App\AttackBooking;
use App\Services\Bot\BaseCommand;
use App\Tick;
use App\User;
use Carbon\Carbon;

class LaunchCommand extends BaseCommand
{
    protected $command = 'launch';

    protected $requireWebUser = true;

    public static $help = 'Shows the launch times for your claimed targets';

    /**
     * Handle the LaunchCommand
     */
    public function handle(): string
    {
        $user = User::find($this->userId);

        $currentTick = Tick::orderBy('tick', 'DESC')->first();

        // get bookings
        $reply = '';
        $attacks = Attack::where(['is_opened' => 1, 'is_closed' => 0])->get();

        foreach ($attacks as $attack) {
            $sharedBookings = AttackBooking::with(['target', 'target.planet'])
                ->whereHas('users', function ($q) use ($user) {
                    $q->where('user_id', $user->id);
                })
                ->where('attack_id', $attack->id)
                ->get();

            $myBookings = AttackBooking::with(['target', 'target.planet'])
                ->where('user_id', $user->id)
                ->where('attack_id', $attack->id)
                ->get();

            $bookings = $sharedBookings->merge($myBookings);

            foreach ($bookings as $booking) {
                $target = $booking->target;

                $planet = $target->planet;

                $time = Carbon::parse(Carbon::now($user->timezone)->startOfHour());
                $travelTicks = $booking->land_tick - $currentTick->tick;
                $landingTime = date('H:i', strtotime('+'.$travelTicks.' hours', strtotime($time)));
                $reply .= sprintf(
                    '%s:%s:%s LT%s landing at %s (eta %s)'.PHP_EOL,
                    $planet->x,
                    $planet->y,
                    $planet->z,
                    $booking->land_tick,
                    $landingTime,
                    $travelTicks
                );
            }
        }

        return strlen($reply) > 0 ? $reply : 'No bookings found';
    }
}
