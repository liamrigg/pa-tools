<?php

namespace App\Services\Bot\Commands;

use App\Services\Bot\BaseCommand;

class WhoAmICommand extends BaseCommand
{
    protected $command = 'whoami';

    public static $help = 'Tells you your user id for your bot provider eg Telegram.';

    /**
     * Execute the WhoAmICommand
     */
    public function handle(): string
    {
        return $this->botUserId;
    }
}
