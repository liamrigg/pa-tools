<?php

namespace App\Services\Bot\Commands;

use App;
use App\Services\Bot\BaseCommand;
use App\Services\FindShip;
use App\Services\Misc\Afford;

class AffordCommand extends BaseCommand
{
    protected $command = 'afford';

    public static $help = 'Tells you how many ships a planet can afford to buy.';

    protected $requireWebUser = true;

    /**
     * Execute AffordCommand
     */
    public function handle(): string
    {
        $afford = App::make(Afford::class);

        $string = explode(' ', $this->text);

        if (! isset($string[0]) && ! isset($string[1])) {
            return 'usage: !afford <coords> <ship>';
        }

        $findShip = App::make(FindShip::class);
        $ship = $findShip->setName($string[1])
            ->execute();

        if (! isset($ship->id)) {
            return $ship;
        }

        preg_match("/^(\d+)[.: ](\d+)[.: ](\d+)$/", $string[0], $planet);

        $afford->setX($planet[1]);
        $afford->setY($planet[2]);
        $afford->setZ($planet[3]);
        $afford->setShip($ship->id);

        return $afford->execute();
    }
}
