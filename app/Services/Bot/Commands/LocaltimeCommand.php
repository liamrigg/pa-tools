<?php

namespace App\Services\Bot\Commands;

use App\Services\Bot\BaseCommand;
use Carbon\Carbon;

class LocaltimeCommand extends BaseCommand
{
    protected $command = 'localtime';

    protected $requireWebUser = true;

    public static $help = 'Shows the local time for a user - usage: !localtime [@username|nick]';

    /**
     * Handle the LocaltimeCommand
     */
    public function handle(): string
    {
        $name = $this->text;
        if (! $name) {
            return 'Usage: !localtime [@username|nick]';
        }

        $user = $this->findUser($name);

        if (! $user) {
            return 'No user found with this @username or nick: '.$name;
        }

        if (isset($user->timezone)) {
            $currentTime = Carbon::parse(Carbon::now($user->timezone));

            return sprintf('Local time for user %s is: %s', $user->name, $currentTime);
        } else {
            return sprintf('User %s has no time offset recorded in webby!', $user->name);
        }
    }
}
