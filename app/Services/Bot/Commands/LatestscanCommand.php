<?php

namespace App\Services\Bot\Commands;

use App\Planet;
use App\Scan;
use App\Services\Bot\BaseCommand;

class LatestscanCommand extends BaseCommand
{
    protected $command = 'latestscan';

    protected $requireWebUser = true;

    public static $help = 'Shows the latest scan of specified type for planet - usage: !latestscan [x:y:z] [pdua]';

    /**
     * Handle the Latestscan Command
     */
    public function handle(): string
    {
        $string = explode(' ', $this->text);
        $a = 0;
        $reply = '';
        while (isset($string[1][$a]) && $string[1][$a] != null) {
            // coords
            $coords = explode(':', $string[0]);

            // retrieve scan
            switch ($string[1][$a]) {
                case 'p':
                case 'd':
                case 'u':
                case 'a':
                    // fetch planet
                    $planet = Planet::where('x', $coords[0])->where('y', $coords[1])->where('z', $coords[2])->first();

                    // fetch scan id
                    $method = 'latest_'.$string[1][$a];
                    $planetScanId = $planet->$method;

                    // fetch scan
                    $scan = Scan::where('id', $planetScanId)->first();

                    if (isset($scan->id)) {
                        $reply .= sprintf('%s: https://game.planetarion.com/showscan.pl?scan_id=%s'.PHP_EOL, strtoupper($string[1][$a]), $scan->pa_id);
                    } else {
                        $reply .= sprintf('Unable to find %s scan for %d:%d:%d'.PHP_EOL, strtoupper($string[1][$a]), $coords[0], $coords[1], $coords[2]);
                    }
                    break;
                default:
                    return 'Usage: !latestscan [x:y:z] [pdua]';
            }
            $a++;
        }

        return $reply;
    }
}
