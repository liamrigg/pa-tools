<?php

namespace App\Services\Bot\Commands;

use App\AllianceRelation;
use App\Services\Bot\BaseCommand;
use App\Tick;

class WarsCommand extends BaseCommand
{
    protected $command = 'wars';

    public static $help = 'Shows a list of on-going wars.';

    /**
     * Execute the WarsCommand
     */
    public function handle(): string
    {
        $tick = Tick::orderBy('tick', 'desc')->first()->tick;

        $relations = AllianceRelation::with([
            'allianceOne',
            'allianceTwo',
        ])->where('type', 'war')
            ->where('expires', '>', $tick)
            ->get();

        $wars = [];

        foreach ($relations as $relation) {
            $wars[] = $relation->allianceOne->name.' is at war with '.$relation->allianceTwo->name.' for '.$relation->expires - $tick.' tick(s)';
        }

        if (! count($wars)) {
            return 'There are no on-going wars';
        } else {
            return implode(PHP_EOL, $wars);
        }
    }
}
