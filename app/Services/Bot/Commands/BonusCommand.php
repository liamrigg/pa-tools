<?php

namespace App\Services\Bot\Commands;

use App\Services\Bot\BaseCommand;
use App\Services\Misc\Bonus;
use App;
use App\Tick;

class BonusCommand extends BaseCommand
{
    protected $command = 'bonus';

    public static $help = 'Shows the current value of the upgrade bonus - Usage: !bonus <optional: tick_number>.';

    /**
     * Execute the BonusCommand
     */
    public function handle(): string
    {
        preg_match("/^(\d+)$/", $this->text, $provided_tick);

        if ($provided_tick) {
            $tick = intval($this->text);
            if ($tick < 1) {
                return 'Please use anything between 1 and 1177';
            }
            if ($tick > 1177) {
                return 'Please use anything between 1 and 1177';
            }
        } else {
            $currentTick = Tick::orderBy('tick', 'DESC')->first();
            $tick = $currentTick->tick;
            if (! $currentTick) {
                return "The game hasn't even started yet!";
            }
        }

        $bonus = App::make(Bonus::class);

        $bonus = $bonus->setTick($tick)->execute();

        return sprintf(
            'Upgrade Bonus at tick %d'.PHP_EOL.PHP_EOL.'Resource: %s of each resource, OR'.PHP_EOL.'Asteroid: %s of each asteroid'.PHP_EOL.PHP_EOL.'AND'.PHP_EOL.PHP_EOL.'Research Points: %s-%s, OR'.PHP_EOL.'Construction Points: %s-%s construction units',
            $tick,
            number_format($bonus['resource_bonus']),
            number_format($bonus['asteroid_bonus']),
            number_format($bonus['research_bonus']),
            number_format($bonus['research_bonus_5percent']),
            number_format($bonus['construction_bonus']),
            number_format($bonus['construction_bonus_5percent'])
        );
    }
}
