<?php

namespace App\Services\Bot\Commands;

use App;
use App\Services\Bot\BaseCommand;
use App\Services\Misc\Eff;

class EffCommand extends BaseCommand
{
    protected $command = 'eff';

    public function handle(): string
    {
        $eff = App::make(Eff::class);

        $string = explode(' ', $this->text);

        if (! isset($string[0]) || ! isset($string[1])) {
            return 'usage: !eff <amount> <ship>';
        }

        $ship = trim(str_replace($string[0], '', $this->text));

        return $eff
            ->setName($ship)
            ->setAmount($string[0])
            ->execute();
    }
}
