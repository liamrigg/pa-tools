<?php

namespace App\Services\Bot\Commands;

use App\Alliance;
use App\Services\Bot\BaseCommand;
use App\Setting;
use App\User;

class WinnersCommand extends BaseCommand
{
    protected $command = 'winners';

    public static $help = 'Shows a list of the biggest score gainers today.';

    protected $requireWebUser = true;

    /**
     * Execute the WinnersCommand
     */
    public function handle(): string
    {
        $setting = Setting::where('name', 'alliance')->first();

        if (! $setting->value) {
            return 'Alliance not set';
        }

        $reply = '';

        // retrieve data
        $alliance = Alliance::with(['planets' => function ($query) {
            $query->orderBy('growth_score', 'desc')->take(10);
        }])->find($setting->value);

        $int = 1;

        // prepare data
        foreach ($alliance->planets as $planet) {
            $user = User::where('planet_id', $planet->id)->first();
            if (empty($user)) {
                $username = $planet->x.':'.$planet->y.':'.$planet->z;
            } else {
                $username = $user->name;
            }

            $reply .= sprintf(
                '#'.$int.' %s (%s)'.PHP_EOL,
                $username,
                number_format($planet->growth_score),
            );

            $int++;
        }

        return $reply;
    }
}
