<?php

namespace App\Services\Bot\Commands;

use App\Services\Bot\BaseCommand;
use App\User;

class FailuresCommand extends BaseCommand
{
    protected $command = 'failures';

    public static $help = "Shows a list of users that haven't linked their telegram user to their webby user and a list of users missing telegram usernames.";

    public $requireWebUser = true;

    /**
     * Execute the FailuresCommand
     */
    public function handle(): string
    {
        $users = User::with('botUser')->orderBy('name', 'ASC')->where([
            'is_enabled' => 1,
        ])->get();

        if (count($users)) {
            $noLinkedUser = [];
            $noUserName = [];

            foreach ($users as $user) {
                $user->tg_user = '';
                if ($user->botUser) {
                    if (! $user->botUser->username) {
                        $noUserName[] = "<a href='tg://user?id=".$user->telegram_user_id."'>".$user->name.'</a>';
                    }
                } else {
                    $noLinkedUser[] = $user->name;
                }
            }

            if (count($noLinkedUser) == 0) {
                $noLinkedUser[] = 'None';
            }

            if (count($noUserName) == 0) {
                $noUserName[] = 'None';
            }

            return '<b>No Telegram Username</b>'.PHP_EOL
                .implode(PHP_EOL, $noUserName).PHP_EOL.PHP_EOL
                .'<b>Not Linked to Webby</b>'.PHP_EOL
                .implode(PHP_EOL, $noLinkedUser);
        }

        return 'No bot users found';
    }
}
