<?php

namespace App\Services\Bot\Commands;

use App\Services\Bot\BaseCommand;
use App\User;
use DB;

class MaydayCommand extends BaseCommand
{
    protected $command = 'mayday';

    public function handle(): string
    {
        return 'Command not built yet';

        $user = User::find($this->userId);

        $params = explode(' ', $this->text);
        $race = strtolower($params[0]);

        $valid_races = ['ter', 'cat', 'xan', 'zik', 'etd', 'all'];

        if (! $race) {
            return 'Usage: !mayday <all|ter|xan|cat|zik|etd>';
        }

        if (! in_array($race, $valid_races)) {
            return 'Usage: !mayday <all|ter|xan|cat|zik|etd>';
        }

        if ($race == 'all') {
            $users = User::orderBy('name', 'ASC')->where([
                'is_enabled' => 1,
            ])->get();
        } else {
            $users = User::with('planet')->orderBy('name', 'ASC')->where([
                'is_enabled' => 1,
            ])->get();
        }

        if (count($users)) {
            $members = [];

            foreach ($users as $user) {

                if ($race == 'all') {
                    if (Config::get('TELEGRAM_ENABLED')) {
                        $user->tg_user = '';
                        if ($user->telegram_user_id) {
                            $user->tg_user = DB::table('user')->where('id', $user->telegram_user_id)->first();
                            if (! $user->tg_user->username == '') {
                                $members[] = $user->tg_user->username;

                                continue;
                            }
                        }
                    }
                }

                if ($user->planet) {
                    if (ucwords($race) == $user->planet->race) {
                        if (Config::get('TELEGRAM_ENABLED')) {
                            $user->tg_user = '';
                            if ($user->telegram_user_id) {
                                $user->tg_user = DB::table('user')->where('id', $user->telegram_user_id)->first();
                                if (! $user->tg_user->username == '') {
                                    $members[] = $user->tg_user->username;
                                }
                            }
                        }
                    }
                }
            }

            if (count($members)) {
                return sprintf('%s members: @%s', ucwords($race), implode(', @', $members));
            } else {
                return 'No members with this race?';
            }
        }

        return 'There are no members';
    }
}
