<?php

namespace App\Services\Bot\Commands;

use App\Galaxy;
use App\Planet;
use App\Services\Bot\BaseCommand;
use App\User;

class GalpenisCommand extends BaseCommand
{
    protected $command = 'galpenis';

    public function handle(): string
    {
        $string = explode(' ', $this->text);

        // find the user if provided
        if (isset($string[0])) {
            $user = $this->findUser($string[0]);
            if (! $user) {
                return 'Unable to find anyone by that name';
            }
        } else {
            $user = User::find($this->userId);
            if (! $user) {
                return 'Please set your coords using !setplanet <x:y:z>';
            }
        }

        // retrieve planet score change
        $planet = Planet::find($user->planet_id);
        $galaxy = Galaxy::where('x', $planet->x)->where('y', $planet->y)->first();

        return sprintf(
            'In the last 24 hours %s:%s had a total score gain of %s',
            $planet->x,
            $planet->y,
            number_format($galaxy->growth_score)
        );
    }
}
