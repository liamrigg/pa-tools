<?php

namespace App\Services\Bot\Commands\Scans;

use App\ScanRequest;
use App\Services\Bot\BaseCommand;
use App\User;
use DB;

class FundDestroyerCommand extends BaseCommand
{
    protected $command = 'funddestroyer';

    protected $requireWebUser = true;

    public static $help = 'Shows a list of users who requested the most scans';

    /**
     * Handle the FundDestroyerCommand
     */
    public function handle(): string
    {
        $requests = ScanRequest::select(DB::raw('distinct user_id, count(*) as requests'))
            ->groupBy('user_id')
            ->orderBy('requests', 'DESC')
            ->take(10)
            ->get();

        $response = "Top 10 Scan Requesters:\n";

        foreach ($requests as $requester) {
            $name = User::where('id', $requester->user_id)->pluck('name');
            $member = isset($name[0]) ? $name[0] : 'Deleted?';
            $response .= $member.' - '.$requester->requests." requests.\n";
        }

        return $response;
    }
}
