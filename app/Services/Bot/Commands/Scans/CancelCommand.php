<?php

namespace App\Services\Bot\Commands\Scans;

use App;
use App\Services\Bot\BaseCommand;
use App\Services\DeleteScanRequest;
use App\User;

class CancelCommand extends BaseCommand
{
    protected $command = 'cancel';

    protected $requireWebUser = true;

    public static $help = 'Cancel a scan request - usage: !cancel [request id]';

    /**
     * Handle CancelCommand
     */
    public function handle(): string
    {
        $params = explode(' ', $this->text);
        $reqId = $params[0];

        $user = User::find($this->userId);

        if (! $reqId) {
            return 'There is no request with that ID.';
        }

        $deleteScanRequest = App::make(DeleteScanRequest::class);

        return $deleteScanRequest
            ->setId($reqId)
            ->setUserId($user->id)
            ->execute();
    }
}
