<?php

namespace App\Services\Bot\Commands\Scans;

use App\Alliance;
use App\Planet;
use App\Services\Bot\BaseCommand;
use App\Services\Scans\JumpgateScanParser;
use App\Setting;
use App\Tick;
use Longman\TelegramBot\Request;

class JanCommand extends BaseCommand
{
    protected $command = 'jan';

    protected $requireWebUser = true;

    public static $help = 'Analyses a group of JGP scans and tells you which members are under attack.';

    public function handle(): string
    {
        // validation
        if (! isset($this->text) || $this->text == '') {
            return 'usage: !jan <scan link> [silent]';
        }

        $alliance = Setting::where('name', 'alliance')->first();

        if (isset($alliance->value) && ! empty($alliance->value)) {
            $allianceId = $alliance->value;
        } else {
            return 'Alliance not configured.';
        }

        $allScans = explode(PHP_EOL, trim(str_replace('silent', '', str_replace('notify', '', $this->text))));

        $tick = Tick::orderBy('tick', 'DESC')->first();

        if (strstr($this->text, ' ')) {
            $params = explode(' ', $this->text);
        } else {
            $params = [];
        }

        $incs = 0;
        $messages = 0;

        foreach ($allScans as $url) {
            if ($url == 'silent') {
                continue;
            }
            preg_match('!https://[^/]+/(?:showscan|waves).pl\?scan_id=([0-9a-zA-Z]+)!', $url, $single);
            preg_match('!https://[^/]+/(?:showscan|waves).pl\?scan_grp=([0-9a-zA-Z]+)!', $url, $group);

            try {
                $html = file_get_contents($url);
            } catch (\Exception $e) {
                return 'URL invalid: '.$url;
            }

            $scans = explode('<hr>', $html);

            foreach ($scans as $scan) {
                $isAlliance = false;

                preg_match('/scan_id=([0-9a-zA-Z]+)/', $scan, $id);

                if (isset($id[1])) {
                    $scanId = $id[1];

                    preg_match('/>Scan time\: .* (\d+\:\d+\:\d+)/', $scan, $time);
                    preg_match('/>([^>]+) on (\d+)\:(\d+)\:(\d+) in tick (\d+)/', $scan, $tick);

                    if (! $tick) {
                        continue;
                    }

                    $scanType = strtoupper($tick[1]);
                    $x = $tick[2];
                    $y = $tick[3];
                    $z = $tick[4];
                    $tick = $tick[5];
                    $time = $time[1];

                    $planet = Planet::with(['user', 'user.botUser'])->where([
                        'x' => $x,
                        'y' => $y,
                        'z' => $z,
                    ])->first();

                    if (! $planet) {
                        continue;
                    }

                    $isAlliance = $planet->alliance_id == $allianceId ? true : false;

                    if ($planet->id) {
                        $scan = preg_replace("/\s+/", '', $scan);

                        if ($scanType == 'JUMPGATE PROBE') {
                            $jumpgateScan = new JumpgateScanParser();
                            $jumpgateScan->setScan($scan)
                                ->setScanId($scanId)
                                ->setPlanetId($planet->id)
                                ->setTick($tick)
                                ->setTime($time)
                                ->setAnalysis(true)
                                ->execute();

                            // loop scan
                            $fleetsString = '';
                            if (! isset($attackers)) {
                                $attackers = [];
                            }

                            if (! isset($alliances)) {
                                $alliances = [];
                            }

                            if (! isset($users)) {
                                $users = [];
                            }

                            $user = $planet->user;

                            $fleets = [];

                            foreach ($jumpgateScan->movement as $fleet) {
                                $fleetPlanet = Planet::with('alliance')->find($fleet['planet_from_id']);
                                if ($fleet['mission_type'] == 'Attack' && $fleet['is_prelaunched']) {
                                    $fleet = sprintf(
                                        "%s:%s:%s (%s)\t%s\t%s\t%s",
                                        $fleetPlanet['x'],
                                        $fleetPlanet['y'],
                                        $fleetPlanet['z'],
                                        $fleetPlanet['race'],
                                        $fleet['fleet_name'],
                                        $fleet['eta'],
                                        $fleet['ship_count']
                                    );

                                    if ($fleetPlanet->alliance) {
                                        $fleet .= ' ['.$fleetPlanet->alliance->name.']';
                                    }

                                    $fleets[] = $fleet;

                                    $attackers[$fleetPlanet['x'].':'.$fleetPlanet['y'].':'.$fleetPlanet['z']] = true;

                                    if (! isset($alliances[$fleetPlanet->alliance_id])) {
                                        $alliances[$fleetPlanet->alliance_id] = 0;
                                    }

                                    $alliances[$fleetPlanet->alliance_id]++;

                                    if ($isAlliance) {
                                        $key = isset($user->botUser->username) && $user->botUser->username ? '@'.$user->botUser->username : $planet->x.':'.$planet->y.':'.$planet->z;
                                        if (! isset($users[$key])) {
                                            $users[$key] = 0;
                                        }
                                        $users[$key]++;
                                    }
                                }
                            }

                            // send message
                            if ($isAlliance && count($fleets) > 0) {
                                if (! in_array('silent', $params)) {
                                    if ($user && $user->telegram_user_id) {
                                        $text = 'You have '.count($fleets).' hostile fleet(s) prelaunched:'.PHP_EOL.PHP_EOL.'<code>'.implode(PHP_EOL, $fleets).'</code>';

                                        Request::sendMessage([
                                            'chat_id' => $user->telegram_user_id,
                                            'text' => $text,
                                            'parse_mode' => 'HTML',
                                        ]);

                                        $messages++;
                                    }
                                }
                                $incs++;
                            }
                        }
                    }
                }
            }
        }

        $output = 'JGP Analysis ('.$incs.' member(s) under attack)'.PHP_EOL.PHP_EOL;

        $allianceNumbersOutput = '';

        if (isset($alliances)) {
            foreach ($alliances as $allianceId => $allianceCount) {
                if (is_numeric($allianceId) && $allianceId > 0) {
                    $alliance = Alliance::where('id', $allianceId)->first();
                    if (isset($alliance['name'])) {
                        $allianceNumbersOutput .= $alliance['name'].' ('.$allianceCount.') ';
                    }
                } else {
                    $allianceNumbersOutput .= 'Unknown ('.$allianceCount.') ';
                }
            }
            $output .= 'Attacking alliances: '.$allianceNumbersOutput.PHP_EOL.PHP_EOL;
        }

        if (isset($users)) {
            $usersOutput = '';

            foreach ($users as $username => $userCount) {
                $usersOutput .= $username.' ('.$userCount.') ';
            }
            $output .= 'Member(s) under attack: '.$usersOutput.PHP_EOL.PHP_EOL;
        }

        if (isset($attackers)) {
            // parse list of attacking planets
            $attackingPlanetsOutput = '';
            foreach ($attackers as $coords => $value) {
                $attackingPlanetsOutput .= $coords.' ';
            }

            $output .= 'Attacking planet(s): '.$attackingPlanetsOutput.PHP_EOL.PHP_EOL;
        }

        if (! in_array('silent', $params)) {
            $output .= sprintf('Sent %s message(s)', $messages);
        } else {
            $output .= sprintf('Silent Mode: no messages sent');
        }

        return $output ? $output : 'Nothing to display.';
    }
}
