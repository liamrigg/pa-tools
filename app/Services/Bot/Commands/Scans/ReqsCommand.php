<?php

namespace App\Services\Bot\Commands\Scans;

use App\Planet;
use App\ScanRequest;
use App\Services\Bot\BaseCommand;
use App\Setting;
use App\Tick;
use App\User;
use Config;
use Longman\TelegramBot\Request;

class ReqsCommand extends BaseCommand
{
    protected $command = 'reqs';

    protected $requireWebUser = true;

    public static $help = 'Shows the oustanding scan requests';

    /**
     * Handle the ReqsCommand
     */
    public function handle(): string
    {
        $chatId = Setting::where('name', 'tg_scans_channel')->first();

        if ($this->botChatId != $chatId->value) {
            return 'You can only use that command in the designated scan channel.';
        }

        $response = "Open Scan Requests:\n";
        $requests = ScanRequest::whereNull('scan_id')->orderBy('created_at', 'desc')->get();

        if (count($requests) == 0) {
            return 'No pending scan requests.';
        }

        $scans = Config::get('scans');
        $currentTick = Tick::orderBy('tick', 'DESC')->first();

        $pscans = '';
        $dscans = '';
        $uscans = '';
        $nscans = '';
        $jscans = '';
        $ascans = '';
        $mscans = '';

        foreach ($requests as $request) {
            $requester = User::where('id', $request->user_id)->pluck('name');
            $planet = Planet::where('id', $request->planet_id)->get();
            $request_age = $currentTick->tick - $request->tick;
            $response .= '[#'.$request->id.'] ['.strtoupper($request->scan_type).'] '.$planet[0]->x.':'.$planet[0]->y.':'.$planet[0]->z.' (D:'.$planet[0]->dists.') - '.$requester[0].' - Tick: '.$request->tick.' (Age:'.$request_age."): <a href='".$request."'>attempt scan</a>\n";

            if ($request->scan_type == 'p') {
                $pscans .= $planet[0]->x.':'.$planet[0]->y.':'.$planet[0]->z.', ';
            } elseif ($request->scan_type == 'd') {
                $dscans .= $planet[0]->x.':'.$planet[0]->y.':'.$planet[0]->z.', ';
            } elseif ($request->scan_type == 'u') {
                $uscans .= $planet[0]->x.':'.$planet[0]->y.':'.$planet[0]->z.', ';
            } elseif ($request->scan_type == 'n') {
                $nscans .= $planet[0]->x.':'.$planet[0]->y.':'.$planet[0]->z.', ';
            } elseif ($request->scan_type == 'j') {
                $jscans .= $planet[0]->x.':'.$planet[0]->y.':'.$planet[0]->z.', ';
            } elseif ($request->scan_type == 'a') {
                $ascans .= $planet[0]->x.':'.$planet[0]->y.':'.$planet[0]->z.', ';
            } elseif ($request->scan_type == 'm') {
                $mscans .= $planet[0]->x.':'.$planet[0]->y.':'.$planet[0]->z.', ';
            }
        }

        if ($this->text == 'bulk') {
            if (strlen($pscans) > 1) {
                Request::sendMessage([
                    'chat_id' => $this->botChatId,
                    'text' => 'Requested Planet scans:',
                ]);

                Request::sendMessage([
                    'chat_id' => $this->botChatId,
                    'text' => substr($pscans, 0, -2),
                ]);
            }

            if (strlen($dscans) > 1) {
                Request::sendMessage([
                    'chat_id' => $this->botChatId,
                    'text' => 'Requested Development scans:',
                ]);

                Request::sendMessage([
                    'chat_id' => $this->botChatId,
                    'text' => substr($dscans, 0, -2),
                ]);
            }

            if (strlen($uscans) > 1) {
                Request::sendMessage([
                    'chat_id' => $this->botChatId,
                    'text' => 'Requested Unit scans:',
                ]);

                Request::sendMessage([
                    'chat_id' => $this->botChatId,
                    'text' => substr($uscans, 0, -2),
                ]);
            }

            if (strlen($nscans) > 1) {
                Request::sendMessage([
                    'chat_id' => $this->botChatId,
                    'text' => 'Requested News scans:',
                ]);

                Request::sendMessage([
                    'chat_id' => $this->botChatId,
                    'text' => substr($nscans, 0, -2),
                ]);
            }

            if (strlen($jscans) > 1) {
                Request::sendMessage([
                    'chat_id' => $this->botChatId,
                    'text' => 'Requested Jumpgate scans:',
                ]);

                Request::sendMessage([
                    'chat_id' => $this->botChatId,
                    'text' => substr($jscans, 0, -2),
                ]);
            }

            if (strlen($ascans) > 1) {
                Request::sendMessage([
                    'chat_id' => $this->botChatId,
                    'text' => 'Requested Advanced Unit scans:',
                ]);

                Request::sendMessage([
                    'chat_id' => $this->botChatId,
                    'text' => substr($ascans, 0, -2),
                ]);
            }

            if (strlen($mscans) > 1) {
                Request::sendMessage([
                    'chat_id' => $this->botChatId,
                    'text' => 'Requested Military scans:',
                ]);

                Request::sendMessage([
                    'chat_id' => $this->botChatId,
                    'text' => substr($mscans, 0, -2),
                ]);
            }
        }

        return $response;
    }
}
