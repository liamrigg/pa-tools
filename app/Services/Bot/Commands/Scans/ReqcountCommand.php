<?php

namespace App\Services\Bot\Commands\Scans;

use App\ScanRequest;
use App\Services\Bot\BaseCommand;
use App\User;
use DB;

class ReqcountCommand extends BaseCommand
{
    protected $command = 'reqcount';

    protected $requireWebUser = true;

    /**
     * Execute the reqcount command.
     */
    public function handle(): string
    {
        $string = explode(' ', $this->text);

        if (strlen($string[0]) != 1) {
            return 'Usage: !reqcount <p|d|u|n|i|j|a|m> - example: !reqcount m';
        }

        $requests = '';

        if (str_contains('pdunijam', $string[0])) {
            $requests = ScanRequest::select(
                DB::raw('distinct user_id, count(*) as requests')
            )->where('scan_type', '=', $string[0])
                ->groupBy('user_id')
                ->orderBy('requests', 'DESC')
                ->take(10)
                ->get();
        }

        $response = 'Top 10 Scan Requesters: ('.$string[0].")\n";

        foreach ($requests as $requester) {
            $user = User::find($requester->user_id);
            $member = isset($user->name) ? $user->name : 'Deleted?';
            $response .= $member.' - '.$requester->requests." requests.\n";
        }

        return $response;
    }
}
