<?php

namespace App\Services\Bot\Commands;

use App\Services\Bot\BaseCommand;

class TestCommand extends BaseCommand
{
    protected $command = '.test';

    public function handle(): string
    {
        $string = explode(' ', $this->text);

        return 'hello world';
    }
}
