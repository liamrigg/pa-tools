<?php

namespace App\Services\Bot\Commands;

use App\Services\Bot\BaseCommand;
use Config;

class HelpCommand extends BaseCommand
{
    protected $command = 'help';

    public static $help = 'Shows a list of all the commands loaded on the bot.';

    /**
     * Execute HelpCommand
     */
    public function handle(): string
    {
        $string = explode(' ', $this->text);

        if (isset($string[0]) && ! empty($string[0])) {
            if ($class = Config::get('bot.commands.map.'.$string[0])) {
                if (! empty($class::$help)) {
                    return $class::$help;
                } else {
                    return "There's no help for that command.";
                }
            } else {
                return "That command doesn't exist.";
            }
        }

        $mapped = Config::get('bot.commands.map');

        ksort($mapped);

        $mapped = array_keys($mapped);

        return "Commands loaded\n\n".implode(', ', $mapped)."\n\nDo !help [command] for more information";
    }
}
