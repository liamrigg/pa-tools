<?php

namespace App\Services\Bot\Commands;

use App;
use App\Attack;
use App\Services\Bot\BaseCommand;

class AttacksCommand extends BaseCommand
{
    protected $command = 'attacks';

    public static $help = 'Shows a list of open attacks.';

    public $requireWebUser = true;

    /**
     * Execute the AttacksCommand
     */
    public function handle(): string
    {
        $return = '';

        $opened = Attack::where([
            'is_closed' => 0,
            'is_opened' => 1,
        ])->get();

        $prereleased = Attack::where([
            'is_closed' => 0,
            'is_prereleased' => 1,
        ])->get();

        if (count($opened)) {
            $urls = [];

            foreach ($opened as $attack) {
                $urls[] = App::make('url')->to('/').'/#/attacks/'.$attack->attack_id;
            }

            $return .= sprintf('Open attacks: %s', implode(',', $urls));
        }

        if (count($prereleased)) {
            $urls = [];

            foreach ($prereleased as $attack) {
                $urls[] = App::make('url')->to('/').'/#/attacks/'.$attack->attack_id;
            }

            $return .= PHP_EOL.PHP_EOL.sprintf('Pre-released attacks: %s', implode(',', $urls));
        }

        if ($return) {
            return $return;
        }

        return 'There are no open attacks';
    }
}
