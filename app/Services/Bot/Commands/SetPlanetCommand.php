<?php

namespace App\Services\Bot\Commands;

use App\Planet;
use App\Services\Bot\BaseCommand;
use App\User;

class SetPlanetCommand extends BaseCommand
{
    protected $command = 'setplanet';

    public static $help = 'Sets your planet on webby - Usage: !setplanet [x:y:z]';

    protected $requireWebUser = true;

    /**
     * Execute the SetPlanetCommand
     */
    public function handle(): string
    {
        $user = User::find($this->userId);

        preg_match("/^(\d+)[.: ](\d+)[.: ](\d+)$/", $this->text, $planet);

        $psearch = ($planet) ? $planet : false;

        if ($psearch) {
            $x = $psearch[1];
            $y = $psearch[2];
            $z = $psearch[3];

            $planet = Planet::where([
                'x' => $x,
                'y' => $y,
                'z' => $z,
            ])->first();

            if (! $planet) {
                return sprintf('No planet found at %d:%d:%d', $x, $y, $z);
            } else {
                $user->planet_id = $planet->id;
                $user->save();

                return 'Set planet.';
            }
        }

        return 'usage: !setplanet <x:y:z>';
    }
}
