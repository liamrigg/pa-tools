<?php

namespace App\Services\Bot\Commands;

use App\Services\Bot\BaseCommand;
use App\User;

class SetNickCommand extends BaseCommand
{
    protected $command = 'setnick';

    public static $help = 'Links your telegram user to the webby user - Usage: !setnick [webby_nick]';

    /**
     * Execute the SetNickCommand
     */
    public function handle(): string
    {
        if (! $this->text) {
            return 'usage: !setnick [nick]';
        }

        if (! $user = User::where('name', $this->text)->first()) {
            return "There's no user on webby with that name.";
        }

        if ($user->telegram_user_id) {
            return 'That web user has already been linked to a TG user.';
        }

        $user->telegram_user_id = $this->botUserId;
        $user->save();

        return 'Successfully linked TG user to web user';
    }
}
