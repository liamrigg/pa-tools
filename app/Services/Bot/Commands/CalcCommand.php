<?php

namespace App\Services\Bot\Commands;

use App;
use App\Services\Bot\BaseCommand;
use App\Services\Misc\MakeBattleCalc;
use App\Services\Url\ShortUrlCreate;

class CalcCommand extends BaseCommand
{
    protected $command = 'calc';

    public function handle(): string
    {
        $calc = App::make(MakeBattleCalc::class);

        $string = explode(' ', $this->text);

        if (! isset($string[0]) || ! isset($string[1])) {
            return 'usage: !calc <x:y:z> <land_tick>';
        }

        preg_match("/^(\d+)[.:](\d+)[.:](\d+)$/", $string[0], $coords);

        $url = $calc->setX($coords[1])
            ->setY($coords[2])
            ->setZ($coords[3])
            ->setLandTick($string[1])
            ->execute();

        $shortUrl = App::make(ShortUrlCreate::class);
        $surl = $shortUrl->setUrl($url)
            ->execute();

        return 'Calc generated: '.$surl;
    }
}
