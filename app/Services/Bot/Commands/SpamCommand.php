<?php

namespace App\Services\Bot\Commands;

use App\Alliance;
use App\Planet;
use App\Services\Bot\BaseCommand;
use App\Setting;

class SpamCommand extends BaseCommand
{
    protected $command = 'spam';

    protected $requireWebUser = true;

    public static $help = 'Spams the coords of an alliance - usage: !spam [alliance]';

    /**
     * Handle the Spam Command
     */
    public function handle(): string
    {
        if ($this->text) {
            // If text is provided, search for the alliance based on the provided name or alias
            $alliance = Alliance::where('name', 'like', '%'.$this->text.'%')
                ->orWhere('nickname', $this->text)
                ->first();
        } else {
            // If no text is provided, retrieve the default alliance from the database
            $defaultAllianceId = Setting::where('name', 'alliance')->value('value');
            $alliance = Alliance::find($defaultAllianceId);
        }

        if ($alliance) {
            // If alliance is found, retrieve the coordinates of its planets
            $coords = [];
            $planets = Planet::where('alliance_id', $alliance->id)
                ->orderBy('x')
                ->orderBy('y')
                ->orderBy('z')
                ->get();
            foreach ($planets as $planet) {
                $coords[] = $planet->x.':'.$planet->y.':'.$planet->z;
            }

            return sprintf('Spam for alliance '.$alliance->name.': '.implode(' ', $coords));
        } else {
            // If no alliance is found, return an error message
            return 'There is no alliance matching that name or alias.';
        }
    }
}
