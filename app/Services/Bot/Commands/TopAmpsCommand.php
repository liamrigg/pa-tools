<?php

namespace App\Services\Bot\Commands;

use App\Alliance;
use App\Planet;
use App\Services\Bot\BaseCommand;

class TopAmpsCommand extends BaseCommand
{
    protected $command = 'topamps';

    public static $help = 'Shows a list of the highest amp counts we have data for.';

    protected $requireWebUser = true;

    /**
     * Execute the TopAmpsCommand
     */
    public function handle(): string
    {
        $subject = $this->text;
        $pattern = '/(?i)(ter|cat|xan|zik|etd)/';

        preg_match($pattern, $subject, $race);

        $race = ($race[1]) ?? null;

        $pattern = "/(?i)\b(?!ter|cat|xan|zik|etd)(\S+)/";

        preg_match($pattern, $subject, $ally);

        $ally = ($ally[1]) ?? null;

        if ($ally) {

            $alliance = Alliance::where('name', 'like', '%'.$ally.'%')->orWhere('nickname', $ally)->first();

            if ($race) {
                $planets = Planet::where([
                    ['alliance_id', $alliance->id],
                    ['race', $race],
                ])->orderby('amps', 'desc')->take(10)->get();
                $response = 'Showing top 10 highest known Amp counts for '.strtoupper($race)."'s for alliance: ".$alliance->name."\n";
            } else {
                $planets = Planet::where('alliance_id', $alliance->id)->orderby('amps', 'desc')->take(10)->get();
                $response = 'Showing top 10 highest known Amp counts for alliance: '.$alliance->name."\n";
            }
        } else {

            if ($race) {
                $planets = Planet::where('race', $race)->orderby('amps', 'desc')->take(10)->get();
                $response = 'Showing top 10 highest known Amp counts for '.strtoupper($race)."'s:\n";
            } else {
                $planets = Planet::orderby('amps', 'desc')->take(10)->get();
                $response = "Showing top 10 highest known Amp counts:\n";
            }
        }

        $data = [];
        foreach ($planets as $planet) {
            $nick = ($planet->nick) ? $planet->nick : 'Unknown';
            $alliance = Alliance::where('id', $planet->alliance_id)->first();
            if (isset($alliance)) {
                $alliance = ($alliance->name) ? $alliance->name : 'Unknown';
            } else {
                $alliance = 'Unknown';
            }
            $data[] = '['.$alliance.'] '.$planet->x.':'.$planet->y.':'.$planet->z.' '.$planet->ruler_name.' of '.$planet->planet_name.' ('.$nick.') '.$planet->race.' '.$planet->amps;
        }

        $response = $response.PHP_EOL.implode("\n", $data);

        return $response;

    }
}
