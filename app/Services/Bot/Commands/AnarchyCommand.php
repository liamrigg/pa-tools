<?php

namespace App\Services\Bot\Commands;

use App\Planet;
use App\Services\Bot\BaseCommand;
use App\Tick;

class AnarchyCommand extends BaseCommand
{
    protected $command = 'anarchy';

    public static $help = 'Shows a list of planets in anarchy.';

    /**
     * Execute the AnarchyCommand
     */
    public function handle(): string
    {
        $planets = Planet::whereNotNull('anarchy')->orderBy('anarchy', 'desc')->get();

        $currentTick = Tick::orderBy('tick', 'DESC')->first();

        $list = [];

        foreach ($planets as $planet) {
            $list[] = $planet->coords.' until tick '.$planet->anarchy.' ('.$planet->anarchy - $currentTick->tick.' ticks)';
        }

        return '<b>Planets in Anarchy at tick '.$currentTick->tick.'</b>'.PHP_EOL.PHP_EOL.implode(PHP_EOL, $list);
    }
}
