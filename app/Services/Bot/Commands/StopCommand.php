<?php

namespace App\Services\Bot\Commands;

use App;
use App\Services\Bot\BaseCommand;
use App\Services\Misc\Stop;

class StopCommand extends BaseCommand
{
    protected $command = 'stop';

    public function handle(): string
    {
        $stop = App::make(Stop::class);

        $string = explode(' ', $this->text);

        if (! $string[0] || ! $string[1]) {
            return 'usage: !stop <amount> <ship>';
        }

        $ship = trim(str_replace($string[0], '', $this->text));

        return $stop
            ->setName($ship)
            ->setAmount($string[0])
            ->execute();
    }
}
