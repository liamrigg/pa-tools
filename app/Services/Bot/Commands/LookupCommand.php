<?php

namespace App\Services\Bot\Commands;

use App\Planet;
use App\Services\Bot\BaseCommand;
use App\User;

class LookupCommand extends BaseCommand
{
    protected $command = 'lookup';

    public static $help = 'Show information about your planet or a planet - usage: !lookup [optional: x:y:z]';

    protected $requireWebUser = true;

    /**
     * Handle the LookupCommand
     */
    public function handle(): string
    {
        // No text, try to find the users planet
        if (! $this->text) {
            $user = User::with('planet')->find($this->userId);
            if (! $user->planet) {
                return "You haven't set your coords on webby, use !setplanet [x:y:z].";
            }

            return $user->planet;
        }

        preg_match("/^(\d+)[.: ](\d+)[.: ](\d+)$/", $this->text, $planet);

        $psearch = ($planet) ? $planet : false;

        if ($psearch) {
            $x = $psearch[1];
            $y = $psearch[2];
            $z = $psearch[3];

            $planet = Planet::with('alliance')->where([
                'x' => $x,
                'y' => $y,
                'z' => $z,
            ])->first();

            if ($planet) {
                return $planet;
            } else {
                return sprintf('No planet found at %d:%d:%d', $x, $y, $z);
            }
        }

        return 'usage: !lookup <x:y:z>';
    }
}
