<?php

namespace App\Services\Bot\Commands;

use App\Services\Bot\BaseCommand;

class WhoDidThisCommand extends BaseCommand
{
    protected $command = 'whodidthis';

    public static $help = 'Kudos for the tools.';

    /**
     * Execute the WhoDidThisCommand
     */
    public function handle(): string
    {
        return 'This is the original VenoX tools with some features added by macen, Nitin, Sven, Johnny Aywah & ChronoX';
    }
}
