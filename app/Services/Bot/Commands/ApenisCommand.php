<?php

namespace App\Services\Bot\Commands;

use App\Alliance;
use App\Services\Bot\BaseCommand;
use App\Setting;

class ApenisCommand extends BaseCommand
{
    protected $command = 'apenis';

    public static $help = 'Shows the score gained by our alliance.';

    /**
     * Execute the ApenisCommand
     */
    public function handle(): string
    {
        $setting = Setting::where('name', 'alliance')->first();

        if (! $setting->value) {
            return 'Alliance not set';
        }

        $alliance = Alliance::find($setting->value);

        return sprintf(
            'In the last 24 hours %s had a total score gain of %s',
            $alliance->name,
            number_format($alliance->growth_score)
        );

    }
}
