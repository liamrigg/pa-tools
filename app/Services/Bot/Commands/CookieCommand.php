<?php

namespace App\Services\Bot\Commands;

use App\Cookie;
use App\Services\Bot\BaseCommand;
use App\User;

class CookieCommand extends BaseCommand
{
    protected $command = 'cookie';

    public static $help = 'Give user a virtual cookie - Usage: !cookie [nick] [reason].';

    protected $requireWebUser = true;

    /**
     * Execute the CookieCommand.
     */
    public function handle(): string
    {
        $message = '';

        $string = explode(' ', $this->text);

        if (! isset($string[0]) || strlen($string[0]) <= 0) {
            return 'usage: !cookie [nick] [reason]';
        }

        // find the user
        $user = $this->findUser($string[0]);

        if (! $user) {
            return 'Unable to find anyone by that name';
        }

        // give cookie
        if (isset($string[1])) {
            $message = substr($this->text, strpos($this->text, ' ') + 1);
            Cookie::create([
                'user_id' => $user->id,
                'message' => $message,
            ]);
        }

        // count cookies
        $countCookies = Cookie::where('user_id', $user->id)->get();

        if (isset($string[1])) {
            return sprintf(
                'Gave cookie to %s with message "%s" (%s total cookies)',
                $user->name,
                $message,
                count($countCookies)
            );
        } else {
            return sprintf(
                '%s has %s total cookies',
                $user->name,
                count($countCookies)
            );
        }
    }
}
