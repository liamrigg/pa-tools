<?php

namespace App\Services\Bot\Commands;

use App;
use App\Services\Bot\BaseCommand;
use App\Services\Transit;

class SmsCommand extends BaseCommand
{
    protected $command = 'sms';

    public static $help = 'Send an SMS to a user.';

    protected $requireWebUser = true;

    /**
     * Execute the SmsCommand
     */
    public function handle(): string
    {
        // validation
        if (! $this->text) {
            return 'usage: !sms <member> <message>';
        }

        $username = addslashes(substr($this->text, 0, strpos($this->text, ' ')));
        $message = addslashes(substr($this->text, strpos($this->text, ' ')));

        if (strlen($username) < 1) {
            return 'Please provide a username';
        }

        if (strlen($message) < 1) {
            return 'No message given';
        }

        $transit = App::make(Transit::class);

        return $transit->sms($username, trim($message));
    }
}
