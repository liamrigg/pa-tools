<?php

namespace App\Services\Bot\Commands;

use App;
use App\Services\Bot\BaseCommand;

class ToolsCommand extends BaseCommand
{
    protected $command = 'tools';

    public function handle(): string
    {
        return App::make('url')->to('/');
    }
}
