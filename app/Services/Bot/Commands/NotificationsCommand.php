<?php

namespace App\Services\Bot\Commands;

use App\Services\Bot\BaseCommand;
use App\User;
use Config;
use App\Services\Bot\NotifyUser;
use App;

class NotificationsCommand extends BaseCommand
{
    protected $command = 'notifications';

    protected $requireWebUser = true;

    public function handle(): string
    {
        $user = User::find($this->userId);

        if (Config::get('notifications.email_notifications.enabled')) {
            $email = Config::get('notifications.email_notifications.email_username');
            $email = str_replace('@gmail.com', '', $email);
            $email .= '+'.$user->id.'@gmail.com';
        } else {
            return 'Notifications not configured.';
        }

        $link = 'https://game.planetarion.com/preferences.pl?#tab6';

        $reply = sprintf(
            "Below you will find your unique notification email address that you can setup in Planetarion on the <a href='%s'>Set Notifications</a> page.".PHP_EOL.PHP_EOL."%s",
            $link,
            $email
        );

        $notifyUser = App::make(NotifyUser::class);
        $notifyUser
            ->setId($this->userId)
            ->setMessage($reply)
            ->execute();

        return "";
    }
}
