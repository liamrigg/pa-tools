<?php

namespace App\Services\Bot\Commands;

use App;
use App\Services\Bot\BaseCommand;
use App\Services\Misc\Cost;

class CostCommand extends BaseCommand
{
    protected $command = 'cost';

    public function handle(): string
    {
        $cost = App::make(Cost::class);

        $string = explode(' ', $this->text);

        if (! $string[0] || ! $string[1]) {
            return 'usage: !cost <amount> <ship>';
        }

        return $cost->setName($string[1])
            ->setAmount($string[0])
            ->execute();
    }
}
