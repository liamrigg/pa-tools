<?php

namespace App\Services\Bot\Commands;

use App;
use App\Services\Bot\BaseCommand;
use App\Services\Transit;

class CallCommand extends BaseCommand
{
    protected $command = 'call';

    public static $help = 'Place a call to <nick> - Usage: !call <nick>.';

    protected $requireWebUser = true;

    /**
     * Execute the CallCommand
     */
    public function handle(): string
    {
        // validation
        if (! $this->text || strpos($this->text, ' ')) {
            return 'usage: !call <member>';
        }

        // place the call
        $transit = App::make(Transit::class);

        return $transit->call(addslashes($this->text));

    }
}
