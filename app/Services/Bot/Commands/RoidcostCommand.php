<?php

namespace App\Services\Bot\Commands;

use App;
use App\Services\Bot\BaseCommand;
use App\Services\Misc\RoidCost;

class RoidcostCommand extends BaseCommand
{
    protected $command = 'roidcost';

    public static $help = 'Shows the return on value loss for roids.';

    /**
     * Execute the RoidcostCommand
     */
    public function handle(): string
    {
        $eff = App::make(RoidCost::class);

        $string = explode(' ', $this->text);

        if (! isset($string[0]) || ! isset($string[1]) || ! isset($string[2])) {
            return 'usage: !roidcost [roids] [value_loss] [mining_bonus]';
        }

        return $eff->setRoids($string[0])
            ->setCost($string[1])
            ->setBonus($string[2])
            ->execute();

        return 'There are no open attacks';
    }
}
