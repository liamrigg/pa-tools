<?php

namespace App\Services\Bot\Commands;

use App\Alliance;
use App\Planet;
use App\Services\Bot\BaseCommand;
use App\User;

class Last24Command extends BaseCommand
{
    protected $command = 'last24';

    protected $requireWebUser = true;

    public static $help = 'Shows the last 24 hour growth for a user, planet or alliance - usage: !last24 [optional: user|x:y:z|alliance]';

    /**
     * Handle the Last24Command
     */
    public function handle(): string
    {
        $string = explode(' ', $this->text);
        $alliance = '';
        $user = '';
        $planet = '';

        // find the user if provided
        if (isset($string[0])) {
            $user = $this->findUser(addslashes($string[0]));

            // if no user found, look for alliance
            if (! $user) {
                $alliance = Alliance::where('name', 'LIKE', '%'.addslashes($string[0]).'%')->first();
            }

            if (! $alliance && ! $user) {
                // find by planet
                preg_match("/^(\d+)[.: ](\d+)[.: ](\d+)$/", $this->text, $planet);

                $psearch = ($planet) ? $planet : false;

                if ($psearch) {
                    $x = $psearch[1];
                    $y = $psearch[2];
                    $z = $psearch[3];

                    $planet = Planet::with('alliance')->where([
                        'x' => $x,
                        'y' => $y,
                        'z' => $z,
                    ])->first();

                    if (! $planet) {
                        return sprintf('No planet found at %d:%d:%d', $x, $y, $z);
                    }
                } else {
                    return 'Could not find user, planet or alliance';
                }
            }
        } else {
            $user = User::with('planet')->find($this->userId);
            if (! $user->planet) {
                return 'You have not set your planet';
            }
        }

        $reply = '';
        if ($alliance) {
            $reply .= sprintf(
                'Alliance: %s'.PHP_EOL,
                $alliance->name
            );
            $reply .= sprintf(
                'Score: %s (%s%s)'.PHP_EOL,
                number_format($alliance->growth_score),
                ($alliance->growth_rank_score > 0 ? '+' : ''),
                $alliance->growth_rank_score
            );
            $reply .= sprintf(
                'Roids: %s (%s%s)'.PHP_EOL,
                number_format($alliance->growth_size),
                ($alliance->growth_rank_size > 0 ? '+' : ''),
                $alliance->growth_rank_size
            );
            $reply .= sprintf(
                'Value: %s (%s%s)'.PHP_EOL,
                number_format($alliance->growth_value),
                ($alliance->growth_rank_value > 0 ? '+' : ''),
                $alliance->growth_rank_value
            );
            $reply .= sprintf(
                'XP: %s (%s%s)'.PHP_EOL,
                number_format($alliance->growth_xpe),
                ($alliance->growth_rank_xp > 0 ? '+' : ''),
                $alliance->growth_rank_xp
            );

            return $reply;
        }
        if ($user) {
            $reply = sprintf(
                'Nick: %s (%d:%d:%d)'.PHP_EOL,
                $user->name,
                $user->planet->x,
                $user->planet->y,
                $user->planet->z
            );
            $reply .= $this->replyPlanet($user->planet);

            return $reply;
        }
        if ($planet) {
            $reply = sprintf(
                'Planet: %d:%d:%d'.PHP_EOL,
                $planet->x,
                $planet->y,
                $planet->z
            );
            $reply .= $this->replyPlanet($planet);

            return $reply;
        }

        return '';
    }

    private function replyPlanet($planet)
    {
        $reply = '';
        $reply .= sprintf(
            'Score: %s (%s%s)'.PHP_EOL,
            number_format($planet->growth_score),
            ($planet->growth_rank_score > 0 ? '+' : ''),
            $planet->growth_rank_score
        );
        $reply .= sprintf(
            'Roids: %s (%s%s)'.PHP_EOL,
            number_format($planet->growth_size),
            ($planet->growth_rank_size > 0 ? '+' : ''),
            $planet->growth_rank_size
        );
        $reply .= sprintf(
            'Value: %s (%s%s)'.PHP_EOL,
            number_format($planet->growth_value),
            ($planet->growth_rank_value > 0 ? '+' : ''),
            $planet->growth_rank_value
        );
        $reply .= sprintf(
            'XP: %s (%s%s)'.PHP_EOL,
            number_format($planet->growth_xp),
            ($planet->growth_rank_xp > 0 ? '+' : ''),
            $planet->growth_rank_xp
        );

        return $reply;
    }
}
