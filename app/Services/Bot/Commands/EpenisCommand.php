<?php

namespace App\Services\Bot\Commands;

use App\Planet;
use App\Services\Bot\BaseCommand;
use App\User;

class EpenisCommand extends BaseCommand
{
    protected $command = 'epenis';

    public static $help = 'Shows the score gain for your planet or another users planet in the last 24h - Usage: !epenis <optional: nick>';

    protected $requireWebUser = true;

    /**
     * Handle the EpenisCommand
     */
    public function handle(): string
    {
        if (! $this->text) {
            $user = User::with('planet')->find($this->userId);
        } else {
            $user = $this->findUser($this->text);

            if (! $user) {
                return "Could not find anyone by the name: $this->text";
            }
        }

        if (! $user->planet) {
            return "$user->name has no planet set.";
        }

        // retrieve planet score change
        $planet = Planet::find($user->planet_id);

        return sprintf(
            'In the last 24 hours %s:%s:%s had a total score gain of %s',
            $planet->x,
            $planet->y,
            $planet->z,
            number_format($planet->growth_score)
        );
    }
}
