<?php

namespace App\Services\Bot;

use App;
use Config;

class BotCommand
{
    private string $message;

    private int $userId;

    private int $botUserId;

    private int $botChatId;

    private $commands;

    public function __construct()
    {
        $this->mapCommands();
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function setUserId(int $id): self
    {
        $this->userId = $id;

        return $this;
    }

    public function setBotUserId(int $id): self
    {
        $this->botUserId = $id;

        return $this;
    }

    public function setBotChatId(int $id): self
    {
        $this->botChatId = $id;

        return $this;
    }

    public function execute()
    {
        $command = false;

        $commandText = strtok(substr($this->message, 1), ' ');

        if (isset($this->commands[$commandText])) {
            $command = App::make($this->commands[$commandText]);
        }

        if ($command) {
            return $command
                ->setUserId($this->userId)
                ->setBotUserId($this->botUserId)
                ->setBotChatId($this->botChatId)
                ->setText($this->message)
                ->execute();
        } else {
            return "That command doesn't exist.";
        }
    }

    private function mapCommands()
    {
        $this->commands = Config::get('bot.commands.map');

        $aliases = Config::get('bot.commands.alias');

        foreach ($aliases as $key => $alias) {
            $this->commands[$key] = $this->commands[$alias];
        }
    }
}
