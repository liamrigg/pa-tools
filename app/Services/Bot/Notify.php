<?php

namespace App\Services\Bot;

use App\Setting;
use Config;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;

/**
 * A helper class for finding a ship with a search string.
 */
class Notify
{
    private $message;

    private $pinMessage = false;

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function setPinMessage(bool $pin): self
    {
        $this->pinMessage = $pin;

        return $this;
    }

    public function execute()
    {
        $telegramNotificationsChannel = Setting::where('name', 'tg_notifications_channel')->first();

        if ($telegramNotificationsChannel) {
            $this->notifyTelegram($telegramNotificationsChannel);
        }
    }

    private function notifyTelegram($chatId)
    {
        if ($chatId->value && $this->message) {
            $data = [
                'chat_id' => $chatId->value,
                'text' => $this->message,
            ];

            if ($this->message != strip_tags($this->message)) {
                $data['parse_mode'] = 'HTML';
            }

            $telegram = new Telegram(
                Config::get('telegram.bot.api_token'),
                Config::get('telegram.bot.username')
            );

            $response = Request::sendMessage($data);

            $messageId = $response->getResult()->getMessageId();

            if ($this->pinMessage) {
                Request::pinChatMessage([
                    'message_id' => $messageId,
                    'chat_id' => $chatId->value
                ]);
            }
        }
    }
}
