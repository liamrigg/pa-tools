<?php

namespace App\Services\Bot;

use App\User;
use Config;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;

class NotifyUser
{
    private $id;

    private $message;

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function execute()
    {
        if (!$this->id || !$this->message) {
            return;
        }

        $user = User::findOrFail($this->id);

        if ($user->telegram_user_id) {
            $this->notifyTelegramUser($user);
        }
    }

    private function notifyTelegramUser($user)
    {
        $data = [
            'chat_id' => $user->telegram_user_id,
            'text' => $this->message,
        ];

        if ($this->message != strip_tags($this->message)) {
            $data['parse_mode'] = 'HTML';
        }

        $telegram = new Telegram(
            Config::get('telegram.bot.api_token'),
            Config::get('telegram.bot.username')
        );

        Request::sendMessage($data);
    }
}
