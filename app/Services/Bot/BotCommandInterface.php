<?php

namespace App\Services\Bot;

interface BotCommandInterface
{
    public function handle();
}
