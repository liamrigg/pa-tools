<?php

namespace App\Services\Bot;

use App\User;
use Config;

abstract class BaseCommand
{
    public static $help = 'Help text not set.';

    /**
     * The user id of the web user if they were identified
     *
     * @var int
     */
    public $userId;

    public $botUserId;

    public $botChatId;

    public $admin;

    public $text;

    protected $command;

    protected $requireBotAdmin = false;

    protected $requireWebAdmin = false;

    protected $requireWebUser = false;

    private $error;

    public function __construct()
    {
        $this->admin = Config::get('telegram.admins');
    }

    /**
     * Set the id of the User from Telegram
     *
     * @param  int  $userId  The id of the Telegram user
     */
    public function setUserId($userId): object
    {
        $this->userId = $userId;

        return $this;
    }

    public function setBotUserId(int $id): self
    {
        $this->botUserId = $id;

        return $this;
    }

    public function setBotChatId(int $id): self
    {
        $this->botChatId = $id;

        return $this;
    }

    /**
     * Set the text passed to the command by trimming the command from the original
     * message
     *
     * @param  string  $text  The text of the command
     */
    public function setText($text): object
    {
        $commandText = strtok(substr($text, 1), ' ');

        $text = substr($text, 1);
        $this->text = ltrim(str_replace($commandText, '', $text));

        return $this;
    }

    /**
     * Checks the user is setup as a Member of the application.
     */
    public function isUser(): bool
    {
        if ($this->userId) {
            return true;
        }

        return false;
    }

    /**
     * Checks the user is setup as an Admin of the bot.
     */
    public function isBotAdmin(): bool
    {
        if ($this->botUserId == $this->admin) {
            return true;
        }

        return false;
    }

    /**
     * Checks the user is setup as an Admin of the application.
     */
    public function isAdmin(): bool
    {
        $user = User::with('role')->find($this->userId);

        if ($user->role->name == 'Admin') {
            return true;
        }

        return false;
    }

    /**
     * Execute the handle method for the command
     */
    public function execute(): string
    {
        if ($this->isAllowed()) {
            return $this->handle();
        }

        if ($this->error) {
            return $this->error;
        }

        return 'Something went wrong.';
    }

    /**
     * Check whether the command is allowed to be used
     */
    public function isAllowed(): bool
    {
        if ($this->requireBotAdmin && ! $this->isBotAdmin()) {
            $this->error = 'You must be a bot admin to use that command.';

            return false;
        }

        if ($this->requireWebAdmin && ! $this->isAdmin()) {
            $this->error = 'You must be a web admin to use that command.';

            return false;
        }

        if ($this->requireWebUser && ! $this->isUser()) {
            $this->error = 'You must have a web user to use that command. Use !setnick [nick] to link your user.';

            return false;
        }

        return true;
    }

    /**
     * Find a user by their name
     */
    public function findUser($name): User|bool
    {
        $name = ltrim($name, '@');

        $user = User::with('planet')->whereHas('botUser', function ($q) use ($name) {
            $q->where('username', $name);
        })->first();

        if (! $user) {
            $user = User::where('name', $name)->first();

            if (! $user) {
                $user = User::where('name', 'like', '%'.$name.'%')->first();

                if (! $user) {
                    return false;
                }
            }
        }

        return $user;
    }

    /**
     * Default handle method, should be overriden by extending command
     */
    public function handle(): string
    {
        return 'Command has no handle method.';
    }
}
