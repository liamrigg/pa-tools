<?php

namespace App\Services\Feed;

use App\Feed;
use App\Services\Pager\PagerTrait;

/**
 * A helper class for finding a ship with a search string.
 */
class FeedList
{
    use PagerTrait;

    private $query;

    public function __construct()
    {
        $this->query = Feed::query()->orderBy('tick', 'desc');
    }

    /**
     * Find ship by name.
     *
     * return Model App\Ship | String
     */
    public function execute()
    {
        return $this->paginate();
    }
}
