<?php

namespace App\Services\Alliance;

use App\FleetMovement;
use App\Tick;
use Carbon\Carbon;

class LandTicksChart
{
    private $id;

    private $type;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function execute()
    {
        $ticks = Tick::orderBy('tick', 'ASC')->limit(24)->get();

        $chart[] = ['Time', 'Total'];
        $times = [];

        $totalTicks = Tick::count();

        foreach ($ticks as $tick) {
            $timeTicks = [];
            $time = Carbon::parse($tick->time)->format('H:i');
            $tick = $tick->tick;
            $timeTicks[] = $tick;
            while ($tick + 24 < $totalTicks) {
                $tick = $tick + 24;
                $timeTicks[] = $tick;
            }
            $times[$time] = FleetMovement::whereHas('planetFrom', function ($query) {
                $query->where('alliance_id', $this->id);
            })->where('mission_type', $this->type)
                ->whereIn('land_tick', $timeTicks)
                ->count();
        }

        ksort($times);

        foreach ($times as $time => $total) {
            $chart[] = [$time, $total];
        }

        return $chart;
    }
}
