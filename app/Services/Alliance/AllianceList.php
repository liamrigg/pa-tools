<?php

namespace App\Services\Alliance;

use App\Alliance;
use App\Services\Pager\PagerTrait;

class AllianceList
{
    use PagerTrait;

    private $query;

    private $alliance;

    public function __construct()
    {
        $this->query = Alliance::query();
    }

    public function setAlliance($alliance)
    {
        $this->alliance = $alliance;

        return $this;
    }

    public function execute()
    {
        $this->query = $this->query->withCount('planets');

        $alliances = $this->paginate();

        if ($this->alliance) {
            $alliances->each(function ($item, $key) {
                $item->is_alliance = $this->alliance == $item->id ? true : false;

                return $item;
            });
        }

        return $alliances;
    }
}
