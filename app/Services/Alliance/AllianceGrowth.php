<?php

namespace App\Services\Alliance;

use App\AllianceHistory;
use App\Tick;
use Carbon\Carbon;

class AllianceGrowth
{
    private $id;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function execute()
    {
        $ticks = Tick::orderBy('tick', 'ASC')->limit(24)->get();

        $totalTicks = Tick::count();

        $columns = ['xp', 'size', 'value', 'members', 'score'];
        $times = [];

        foreach ($columns as $column) {
            $response = [];
            foreach ($ticks as $tick) {
                $timeTicks = [];
                $time = Carbon::parse($tick->time)->format('H:i');
                $tick = $tick->tick;
                $timeTicks[] = $tick;
                while ($tick + 24 < $totalTicks) {
                    $tick = $tick + 24;
                    $timeTicks[] = $tick;
                }
                $times[$time] = AllianceHistory::where('alliance_id', $this->id)
                    ->whereIn('tick', $timeTicks)
                    ->sum('change_'.$column);
            }
            ksort($times);

            foreach ($times as $time => $total) {
                $response[] = [
                    'time' => $time,
                    'total' => $total,
                ];
            }
            $all[$column] = $response;
        }

        return $all;
    }
}
