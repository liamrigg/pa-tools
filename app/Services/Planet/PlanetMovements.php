<?php

namespace App\Services\Planet;

use App\Galaxy;
use App\PlanetMovement;

class PlanetMovements
{
    private $id;

    private $type;

    private $limit;

    /**
     * Set the id of the galaxy or planet
     *
     * @param integer $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set the type
     *
     * @param string $type galaxy|planet
     * @return self
     */
    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set the amount of movements per page
     *
     * @param integer $limit
     * @return self
     */
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    public function execute()
    {
        $movements = PlanetMovement::with([
            'planet' => function ($q) {
                $q->select(['id', 'x', 'y', 'z', 'deleted_at', 'galaxy_id', 'age', 'ruler_name', 'planet_name', 'race', 'size', 'score', 'value', 'alliance_id', 'nick', 'xp'])->withTrashed();
            },
            'planet.alliance' => function ($q) {
                $q->select(['id', 'name']);
            },
        ])->orderBy('tick', 'DESC')->orderBy('id', 'ASC');

        if ($this->type == 'planet') {
            $movements = $movements->where('planet_id', $this->id);
        }

        if ($this->type == 'galaxy') {
            $galaxy = Galaxy::find($this->id);

            $movements = $movements->whereRaw(
                '(from_x = ? AND from_y = ?) OR (to_x = ? AND to_y = ?)',
                [
                    $galaxy->x,
                    $galaxy->y,
                    $galaxy->x,
                    $galaxy->y
                ]
            );
        }

        return $movements
            ->paginate($this->limit)
            ->appends(['type' => $this->type, 'id' => $this->id]);
    }
}
