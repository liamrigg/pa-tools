<?php

namespace App\Services\Galaxy;

use App\Galaxy;
use App\Services\Pager\PagerTrait;
use App\Services\Races\RacesTrait;
use App\Services\Settings\SettingsTrait;

class GalaxyList
{
    use PagerTrait;
    use RacesTrait;
    use SettingsTrait;

    private $allianceId;

    private $excludeC200;

    private $withAlliances;

    private $withRaces;

    private $query;

    public function __construct()
    {
        $this->query = Galaxy::query()->with(['planets', 'planets.alliance' => function ($q) {
            $q->select(['id', 'name', 'nickname', 'relation']);
        }]);
    }

    public function setAllianceId($id)
    {
        $this->allianceId = $id;

        return $this;
    }

    public function setExcludeC200($c200)
    {
        $this->excludeC200 = $c200;

        return $this;
    }

    public function setSort($sort): self
    {
        $this->sort = $sort;

        $direction = substr($sort, 0, 1) == '+' ? 'ASC' : 'DESC';
        $orderBy = substr($sort, 1);

        if ($orderBy == 'coords') {
            $this->query->orderBy('x', $direction);
            $this->query->orderBy('y', $direction);
        } else {
            $this->query->orderBy($orderBy, $direction);
        }

        return $this;
    }

    public function setWithAlliances($withAlliances): self
    {
        $this->withAlliances = $withAlliances;

        return $this;
    }

    public function setWithRaces($withRaces): self
    {
        $this->withRaces = $withRaces;

        return $this;
    }

    public function execute()
    {
        if ($this->allianceId) {
            $this->query->with(['planets' => function ($q) {
                $q->where('alliance_id', $this->allianceId);
            }])->whereHas('planets', function ($q) {
                $q->where('alliance_id', $this->allianceId);
            })->withCount(['planets as member_count' => function ($q) {
                $q->where('alliance_id', $this->allianceId);
            }]);
        }

        if ($this->excludeC200) {
            $this->query->where('x', '!=', '200');
        }

        $galaxies = $this->paginate()->appends([
            'with_alliances' => $this->withAlliances,
            'with_races' => $this->withRaces,
        ]);

        if ($this->withRaces) {
            $galaxies = $this->addRaces($galaxies);
        }

        if ($this->withAlliances) {
            $galaxies = $this->addAlliances($galaxies);
        }

        foreach ($galaxies as $galaxy) {
            $planet50 = $galaxy->planet_count / 2;
            $galaxy->is_fort = false;
            foreach ($galaxy->alliances as $alliance) {
                if ($alliance['count'] > $planet50) {
                    $galaxy->is_fort = true;
                }
            }
            if ($this->allianceId) {
                $galaxy->member_size = $galaxy->planets->sum('size');
            }
        }

        return $galaxies;
    }

    private function addRaces($galaxies)
    {
        foreach ($galaxies as $galaxy) {
            $races = $this->getRaces();

            foreach ($races as $race) {
                $races[$race] = 0;
            }

            foreach ($galaxy->planets as $planet) {
                $races[$planet->race]++;
            }

            $galaxy->races = $races;
        }

        return $galaxies;
    }

    private function addAlliances($galaxies)
    {
        foreach ($galaxies as $galaxy) {
            $ally = [];
            foreach ($galaxy->planets as $planet) {
                if ($planet->alliance) {
                    $name = $planet->alliance->nickname;
                    if (isset($ally[$planet->alliance->name])) {
                        $ally[$planet->alliance->name]['count']++;
                    } else {
                        $ally[$planet->alliance->name]['count'] = 1;
                        $ally[$planet->alliance->name]['nickname'] = $name;
                        $ally[$planet->alliance->name]['name'] = $planet->alliance->name;
                        $ally[$planet->alliance->name]['relation'] = $planet->alliance->relation;
                        $ally[$planet->alliance->name]['is_alliance'] = ($planet->alliance->id == $this->getSetting('alliance')) ? true : false;
                    }
                }
            }
            ksort($ally);

            $galaxy->alliances = $ally;
        }

        return $galaxies;
    }
}
