<?php

namespace App\Services\Pager;

trait PagerTrait
{
    protected $page;

    protected $perPage;

    protected $sort;

    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    public function setPerPage($perPage)
    {
        $this->perPage = $perPage;

        return $this;
    }

    public function setSort($sort)
    {
        $this->sort = $sort;

        $direction = substr($sort, 0, 1) == '+' ? 'ASC' : 'DESC';
        $orderBy = substr($sort, 1);

        $this->query->orderBy($orderBy, $direction);

        return $this;
    }

    public function paginate()
    {
        return $this->query->paginate($this->perPage)->appends(['sort' => $this->sort, 'perPage' => $this->perPage]);
    }
}
