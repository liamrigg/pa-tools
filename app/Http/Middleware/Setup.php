<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class Setup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (! User::count()) {
            if ($request->route()->uri() != 'install') {
                return redirect('/install');
            }
        }

        return $next($request);
    }
}
