<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class Authenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Auth::user()) {
            return redirect('/login');
        }

        return $next($request);
    }
}
