<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Services\Scans\ScanRequestList;
use App\Ship;
use App\User;
use Auth;
use Illuminate\Http\Request;
use View;
use Config;

class UserscriptController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('userscript', [
            'apiToken' => Auth::user()->token,
        ]);
    }

    public function script(Request $request, ScanRequestList $scans)
    {
        $token = $request->input('api_token');

        $ships = Ship::all();

        $shipList = [];

        foreach ($ships as $ship) {
            $shipList[$ship->name] = [
                'id' => $ship->id - 1,
                'class' => $ship->class,
                'cloaked' => $ship->is_cloaked,
            ];
        }

        if ($user = User::where('token', $token)->first()) {
            $scans = $scans->setUserId($user->id);
        }

        $requests = $scans->execute();

        $contents = View::make('pascript', [
            'apiToken' => $token,
            'ships' => collect($shipList),
            'scans' => $requests,
        ]);

        return response($contents)->header('Content-Type', 'application/javascript');
    }
}
