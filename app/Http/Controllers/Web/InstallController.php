<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class InstallController extends Controller
{
    public function index()
    {
        if (User::count()) {
            return redirect('/login');
        }

        return view('install');
    }

    public function store(Request $request)
    {

    }
}
