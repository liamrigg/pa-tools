<?php

namespace App\Http\Controllers;

use Config;
use Log;
use Longman\TelegramBot\Telegram;

class TelegramController extends Controller
{
    public function set()
    {
        try {
            // Create Telegram API object
            $telegram = new Telegram(Config::get('telegram.bot.api_token'), Config::get('telegram.bot.username'));

            $commands_paths = [
                __DIR__.'/app/Telegram/Commands',
            ];
            $telegram->addCommandsPaths($commands_paths);
            //dd($telegram->getCommandsList());

            // Set webhook
            $result = $telegram->setWebhook(secure_url(Config::get('telegram.bot.api_token').'/hook'));
            if ($result->isOk()) {
                echo $result->getDescription();
            }
        } catch (\Longman\TelegramBot\Exception\TelegramException $e) {
            // log telegram errors
            echo $e->getMessage();
        }
    }

    public function delete()
    {
        $telegram = new Telegram(Config::get('telegram.bot.api_token'), Config::get('telegram.bot.username'));

        return $telegram->deleteWebHook();
    }

    public function hook()
    {
        try {
            // Create Telegram API object
            $telegram = new Telegram(Config::get('telegram.bot.api_token'), Config::get('telegram.bot.username'));

            $commands_paths = [
                app_path().'/Telegram/Commands',
            ];

            $telegram->addCommandsPaths($commands_paths, true);

            // Handle telegram webhook request
            $telegram->handle();
        } catch (\Longman\TelegramBot\Exception\TelegramException $e) {
            // Silence is golden!
            // log telegram errors
            Log::info($e->getMessage());
        }
    }

    public function updates()
    {
        $telegram = new Telegram(Config::get('telegram.bot.api_token'), Config::get('telegram.bot.username'));

        $commands_paths = [
            app_path().'/Telegram/Commands',
        ];

        $telegram->addCommandsPaths($commands_paths, true);

        return $telegram->handleGetUpdates();
    }
}
