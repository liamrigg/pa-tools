<?php

namespace App\Http\Controllers\Api;

use App;
use App\AttackBooking;
use App\BattleGroup;
use App\User;
use Auth;
use Illuminate\Http\Request;

class BattleGroupController extends ApiController
{
    public function index(Request $request)
    {
        $sortDirection = '';
        $sort = $request->input('sort', '-avg_score');

        $bgs = BattleGroup::with('owner', 'members')->get();

        if ($sort) {
            $sortDirection = substr($sort, 0, 1);
            $sort = substr($sort, 1);
        }

        if ($sortDirection && $sortDirection == '+') {
            $sorted = $bgs->sortBy(function ($bg) use ($sort) {
                return $bg[$sort];
            });
        } else {
            $sorted = $bgs->sortByDesc(function ($bg) use ($sort) {
                return $bg[$sort];
            });
        }

        return $sorted->values();
    }

    public function store(Request $request)
    {
        $bg = BattleGroup::create([
            'name' => $request->input('name'),
            'creator_id' => Auth::user()->id,
        ]);

        $bg->members()->sync([Auth::user()->id => ['is_pending' => 0]], false);
    }

    public function show($id)
    {
        $bg = BattleGroup::with(['bookings', 'members', 'membersPending', 'members.planet', 'membersPending.planet'])->find($id);

        foreach ($bg->members as $member) {
            $ab1 = AttackBooking::where('bg_id', $id)->whereHas('target', function ($q1) {
                $q1->whereHas('attack', function ($q2) {
                    $q2->where('is_closed', 0);
                });
            })->where('land_tick', '>', $this->tick);

            $ab2 = AttackBooking::where('bg_id', $id)->whereHas('target', function ($q1) {
                $q1->whereHas('attack', function ($q2) {
                    $q2->where('is_closed', 0);
                });
            })->where('land_tick', '>', $this->tick);

            $bookingsShared = $ab1->whereHas('users', function ($q) use ($member) {
                $q->where('user_id', $member->id);
            })->get();
            $bookingsOwned = $ab2->where('user_id', $member->id)->get();

            $member->bookings = $bookingsOwned->count() + $bookingsShared->count();
        }
        $members = $bg->members->keyBy('id')->toArray();
        $membersPending = $bg->membersPending->keyBy('id')->toArray();

        $users = User::whereNotNull('planet_id')->orderBy('name')->get()->keyBy('id')->toArray();

        $new = array_diff_key($users, $members);
        $new = array_diff_key($new, $membersPending);

        $bg->non_members = collect($new)->values();

        return $bg;
    }

    public function destroy($id)
    {
        BattleGroup::find($id)->delete();

        return $this->index(App::make(Request::class));
    }

    public function join($id)
    {
        $bg = BattleGroup::find($id);

        $bg->members()->sync([Auth::user()->id => ['is_pending' => 1]], false);
    }
}
