<?php

namespace App\Http\Controllers\Api;

use App\Services\CreateScanRequest;
use App\Services\DeleteScanRequest;
use App\Services\Scans\ScanRequestList;
use Auth;
use Illuminate\Http\Request;

class ScanRequestController extends ApiController
{
    public function index(ScanRequestList $list)
    {
        return $list
            ->setUserId(Auth::user()->id)
            ->execute();
    }

    public function store(Request $request, CreateScanRequest $create)
    {
        return $create
            ->setX($request->input('x'))
            ->setY($request->input('y'))
            ->setZ($request->input('z'))
            ->setScanType($request->input('scan_type'))
            ->setUserId(Auth::user()->id)
            ->setTick($this->tick)
            ->execute();
    }

    public function destroy($id, DeleteScanRequest $deleteRequest)
    {
        return $deleteRequest
            ->setId($id)
            ->setUserId(Auth::user()->id)
            ->execute();
    }

    public function my(ScanRequestList $list)
    {
        return $list
            ->setRequesterId(Auth::user()->id)
            ->execute();
    }
}
