<?php

namespace App\Http\Controllers\Api;

use App\Galaxy;
use App\GalaxyHistory;
use App\Services\Galaxy\GalaxyList;
use App\Services\History\HistoryList;
use Illuminate\Http\Request;

class GalaxyController extends ApiController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, GalaxyList $galaxies)
    {
        return $galaxies
            ->setSort($request->input('sort', '-score'))
            ->setPerPage($request->input('perPage', 30))
            ->setAllianceId($request->input('alliance_id', null))
            ->setExcludeC200($request->input('exclude_c200', null))
            ->setWithAlliances($request->input('with_alliances', false))
            ->setWithRaces($request->input('with_races', false))
            ->execute();
    }

    public function show($id, Request $request)
    {
        $gals = Galaxy::select(['id', 'x', 'y'])
            ->orderBy('x', 'ASC')
            ->orderBy('y', 'ASC')
            ->get();

        $galaxy = Galaxy::find($id);

        foreach ($gals as $key => $gal) {
            if ($gal->id == $id) {
                $galaxy->next_gal = isset($gals[$key + 1]) ? $gals[$key + 1] : null;
                $galaxy->prev_gal = isset($gals[$key - 1]) ? $gals[$key - 1] : null;
            }
        }

        return $galaxy;
    }

    public function history($id, Request $request, HistoryList $history)
    {
        return $history
            ->setId($id)
            ->setType('galaxy')
            ->setLimit($request->input('limit', 10))
            ->execute();
    }

    public function rankChart($id)
    {
        $history = GalaxyHistory::with('tick')->where('galaxy_id', $id)->orderBy('tick', 'ASC')->get();

        $chart = [[
            'Tick',
            'Size',
            'Value',
            'Score',
            'Xp',
        ]];

        foreach ($history as $item) {
            $chart[] = [
                $item->tick,
                $item->rank_size,
                $item->rank_value,
                $item->rank_score,
                $item->rank_xp,
            ];
        }

        return $chart;
    }
}
