<?php

namespace App\Http\Controllers\Api;

use App\Politics;
use Illuminate\Http\Request;

class PoliticsController extends ApiController
{
    /**
     * Show the application dashboard.
     */
    public function index()
    {
        if ($this->settings['show_political_deals'] || $this->settings['role'] == 'Admin') {
            return Politics::with('alliance')->get();
        }
    }

    public function store(Request $request)
    {
        $allianceId = $request->input('alliance_id');
        $status = $request->input('status');
        $maxPlanets = $request->input('max_planets');
        $maxWaves = $request->input('max_waves');
        $maxFleets = $request->input('max_fleets');
        $expire = $request->input('expire');

        $currentRelations = Politics::where('alliance_id', $allianceId)->count();

        if ($currentRelations == 0) {
            $politics = new Politics();
        } else {
            $politics = Politics::where('alliance_id', $allianceId)->first();
        }

        $politics->alliance_id = $allianceId;
        $politics->status = $status;
        $politics->expire = $expire ? (int) $expire : null;

        if ($status == 'deal') {
            $politics->max_planets = (int) $maxPlanets;
            $politics->max_waves = (int) $maxWaves;
            $politics->max_fleets = (int) $maxFleets;
        } else {
            $politics->max_planets = null;
            $politics->max_waves = null;
            $politics->max_fleets = null;
        }

        $politics->save();
    }

    public function destroy($politicsId)
    {
        return Politics::where('id', $politicsId)->delete();
    }
}
