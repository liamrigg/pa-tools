<?php

namespace App\Http\Controllers\Api;

use App\Attack;
use App\AttackBooking;
use App\AttackTarget;
use App\Http\Requests\AttackRequest;
use App\Planet;
use App\Politics;
use App\Services\Attack\AttackCreate;
use App\Services\Attack\AttackDelete;
use App\Services\Attack\AttackList;
use App\Services\Attack\AttackLoad;
use App\Services\Attack\AttackTrait;
use App\Services\Attack\AttackUpdate;
use App\Services\MyBookings;
use App\Services\ParseCoordsList;
use Auth;
use Illuminate\Http\Request;
use App\Services\Bot\Notify;
use App;

class AttackController extends ApiController
{
    use AttackTrait;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, AttackList $attacks)
    {
        return $attacks
            ->setIsClosed((int) $request->input('is_closed', 0))
            ->setIsOpened((int) $request->input('is_opened', 0))
            ->setIsPrereleased((int) $request->input('is_prereleased', 0))
            ->execute();
    }

    public function store(AttackCreate $attack, AttackRequest $request, ParseCoordsList $coordParser)
    {
        return $attack
            ->setWaves($request->input('waves'))
            ->setLandTick($request->input('land_tick'))
            ->setTime($request->input('time'))
            ->setTargets($request->input('targets'))
            ->setPrereleaseTime($request->input('prerelease_time', ''))
            ->setNotes($request->input('notes', ''))
            ->execute();
    }

    public function show($id, Request $request, AttackLoad $attack)
    {
        return $attack
            ->setId($id)
            ->setSort($request->input('sort', '+coords'))
            ->execute();
    }

    public function update($id, AttackUpdate $attack, Request $request)
    {
        return $attack
            ->setId($id)
            ->setLandTick($request->input('land_tick'))
            ->setNotes($request->input('notes'))
            ->setTargets($request->input('newTargets'))
            ->execute();
    }

    public function destroy($id, AttackDelete $attack)
    {
        return $attack
            ->setId($id)
            ->execute();
    }

    // private function calcXp($cap, $targetValue, $targetScore)
    // {
    //     $user = User::with('planet', 'planet.currentPlanet')->where('id', Auth::user()->id)->first();

    //     $value = $user->planet->getOriginal('value');
    //     $score = $user->planet->getOriginal('score');

    //     $mc = (Auth::user()->military_centres / 200) + 1;

    //     $bravery = max(0.2,(min(2.2,$targetScore/$score) -0.2)) * max(0.2,(min(1.8,$targetValue/$value) - 0.1))/((6+max(4,$score/$value))/10);

    //     //dd($cap, $bravery, $mc);

    //     $xp = $cap * 10 * $bravery * $mc;

    //     return number_shorten($xp, 1);
    // }

    // private function calcCap($size, $wave, $targetValue)
    // {
    //     $user = User::with('planet', 'planet.currentPlanet')->where('id', Auth::user()->id)->first();

    //     $value = $user->planet->getOriginal('value');
    //     $score = $user->planet->getOriginal('score');

    //     for($int = $wave; $int > 0; $int--) {
    //         $cap = (0.25 * pow(($targetValue / $value),0.5))*$size;
    //         $size = $size - $cap;
    //     }

    //     return (int) $cap;
    // }

    public function book($attackId, $bookingId, MyBookings $bookings)
    {
        if ($attack = Attack::where(['attack_id' => $attackId, 'is_opened' => 1, 'is_closed' => 0])->first()) {
            $politicsLimit = false;

            $attackBooking = AttackBooking::where('id', $bookingId)->first();

            $attackTarget = AttackTarget::where('attack_id', $attack['id'])
                ->where('id', $attackBooking->attack_target_id)
                ->first();

            $planetId = $attackTarget->planet_id;
            $planet = Planet::where('id', $planetId)->first();

            $politicalRelation = Politics::where('alliance_id', $planet->alliance_id)->first();

            if (is_object($politicalRelation)) {
                // calculate number of planets
                $attackBookingContainer = AttackBooking::where('attack_id', $attack['id'])
                    ->whereNotNull('user_id')
                    ->get();

                foreach ($attackBookingContainer as $attackBookingValue) {
                    $planetId = AttackTarget::where('attack_id', $attack['id'])
                        ->where('id', $attackBookingValue->attack_target_id)
                        ->first()->planet_id;
                    $planetObject = Planet::where('id', $planetId)->first();
                    if (! isset($planetUsed[$planetObject->id])) {
                        if (! isset($maxPlanetsCount[$planetObject->alliance_id])) {
                            $maxPlanetsCount[$planetObject->alliance_id] = 0;
                        }
                        $maxPlanetsCount[$planetObject->alliance_id]++;
                        $registeredPlanets[$planetObject->id] = true;
                    }
                    $planetUsed[$planetObject->id] = true;
                }

                if (isset($maxPlanetsCount[$planet->alliance_id])) {
                    $numberOfPlanets = $maxPlanetsCount[$planet->alliance_id];
                } else {
                    $numberOfPlanets = 0;
                }

                // calculate number of waves
                $attackTargetClass = AttackTarget::where('attack_id', $attack['id'])
                    ->where('planet_id', $planet->id)
                    ->first();

                $numberOfWaves = AttackBooking::where('attack_id', $attack['id'])
                    ->where('attack_target_id', $attackTargetClass->id)
                    ->whereNotNull('user_id')
                    ->count();

                if (
                    ($politicalRelation->status == 'nap') ||
                    ($numberOfPlanets >= $politicalRelation->max_planets && ! isset($registeredPlanets[$planet->id])) ||
                    ($numberOfWaves >= $politicalRelation->max_waves)
                ) {
                    $politicsLimit = true;
                } else {
                    // do booking
                    $booking = AttackBooking::where('id', $bookingId)->whereNull('user_id')->update([
                        'user_id' => Auth::user()->id,
                    ]);
                }
            } else {
                // do booking
                $booking = AttackBooking::where('id', $bookingId)->whereNull('user_id')->update([
                    'user_id' => Auth::user()->id,
                ]);
            }

            if ($politicsLimit) {
                return response()->json(['error' => 'Booking exceeds political restriction'], 422);
            }

            if (! isset($booking)) {
                return response()->json(['error' => 'Target already booked'], 422);
            }
        }

        return $bookings
            ->setUserId(Auth::user()->id)
            ->setAttackId($attackId)
            ->execute();
    }

    public function close($id)
    {
        Attack::where('id', $id)->update([
            'is_closed' => 1,
            'is_opened' => 0,
            'is_prereleased' => 0,
        ]);
    }

    public function open($id)
    {
        Attack::where('id', $id)->update([
            'is_opened' => 1,
            'is_closed' => 0,
            'is_prereleased' => 0,
        ]);
    }

    public function addWaves($id, Notify $notify)
    {
        $attack = Attack::with('targets')->where('attack_id', $id)->first();

        $url = App::make('url')->to('/').'/#/attacks/'.$attack->attack_id;

        $attack->waves = $attack->waves + 1;
        $attack->save();

        $landTick = $attack->land_tick;
        $waves = $attack->waves;

        $newWave = $landTick + ($waves - 1);

        foreach ($attack->targets as $target) {
            AttackBooking::create([
                'attack_id' => $attack->id,
                'attack_target_id' => $target->id,
                'land_tick' => $newWave,
            ]);
        }

        if ($attack->is_opened) {
            $notify = App::make(Notify::class);
            $notify
                ->setMessage("New wave (LT" . $newWave  . ") added to attack: " . $url)
                ->execute();
        }
    }

    public function removeWaves($id)
    {
        $attack = Attack::with('targets')->where('attack_id', $id)->first();

        $attack->waves = $attack->waves - 1;
        $attack->save();

        $landTick = $attack->land_tick;
        $waves = $attack->waves;

        $deleteWave = $landTick + ($waves);

        AttackBooking::where([
            'land_tick' => $deleteWave,
            'attack_id' => $attack->id,
        ])->delete();
    }
}
