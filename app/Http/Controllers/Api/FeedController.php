<?php

namespace App\Http\Controllers\Api;

use App\Services\Feed\FeedList;
use Illuminate\Http\Request;

class FeedController extends ApiController
{
    /**
     * Show the feed list.
     *
     * @return FeedList
     */
    public function index(Request $request, FeedList $feed)
    {
        return $feed
            ->setSort($request->input('sort', '-tick'))
            ->setPerPage($request->input('perPage', 30))
            ->execute();
    }
}
