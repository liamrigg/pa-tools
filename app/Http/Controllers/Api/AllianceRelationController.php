<?php

namespace App\Http\Controllers\Api;

use App\Services\AllianceRelation\AllianceRelationList;
use Illuminate\Http\Request;

class AllianceRelationController extends ApiController
{
    /**
     * Show the feed list.
     *
     * @return FeedList
     */
    public function index(Request $request, AllianceRelationList $relations)
    {
        return $relations
            ->setSort($request->input('sort', '-id'))
            ->setPerPage($request->input('perPage', 30))
            ->setType($request->input('type', ''))
            ->setStatus($request->input('status', 'current'))
            ->setAlliance($request->input('alliance', null))
            ->execute();
    }
}
