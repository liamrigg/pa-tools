<?php

namespace App\Http\Controllers\Api;

use App\FleetMovement;
use App\Services\Fleets\FleetPager;
use Illuminate\Http\Request;

class FleetMovementController extends ApiController
{
    /**
     * Show a paginated list of FleetMovement
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, FleetPager $fleets)
    {
        $movement = $request->input('movement');
        $mission = json_decode($request->input('mission', '{}'), true);
        $type = $request->input('type');
        $id = $request->input('id');
        $limit = $request->input('perPage', null);
        $since = $request->input('since', 0);
        $prelaunched = $request->input('prelaunched', null);

        return $fleets
            ->setId($id)
            ->setMovement($movement)
            ->setMission($mission)
            ->setType($type)
            ->setLandTick($since)
            ->setLimit($limit)
            ->setPrelaunched($prelaunched)
            ->setTick($this->tick)
            ->execute()
            ->appends($request->input());
    }
}
