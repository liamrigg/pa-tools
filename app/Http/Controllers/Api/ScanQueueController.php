<?php

namespace App\Http\Controllers\Api;

use App\ScanQueue;

class ScanQueueController extends ApiController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ScanQueue::where('processed', 0)->get();
    }
}
