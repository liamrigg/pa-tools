<?php

namespace App\Http\Controllers\Api;

use App;
use App\Services\BcalcParser;
use App\Services\Bot\BotCommand;
use App\Services\ScanParser;
use Auth;
use Illuminate\Http\Request;

class BotTestController extends ApiController
{
    /**
     * Show the application dashboard.
     */
    public function index(Request $request, BotCommand $botCommand)
    {
        $input = strtolower($request->input('input'));

        if (preg_match("/(?i)(https:\/\/[^\/]+\/bcalc.pl\?id=[0-9a-zA-Z]+)/", $input, $link)) {
            $bcalc = App::make(BcalcParser::class);
            $calc = $bcalc->parse($input);

            if ($calc) {
                return $calc;
            }
        }

        $parser = App::make(ScanParser::class);
        $scans = $parser
            ->setRespond(true)
            ->queue($input);

        if ($scans) {
            return $scans;
        }

        if (substr($input, 0, 1) == '!' || substr($input, 0, 1) == '.') {
            $botCommand = App::make(BotCommand::class);
            $response = $botCommand
                ->setMessage($request->input('input'))
                ->setUserId(Auth::user()->id)
                ->setBotUserId(336769304)
                ->setBotChatId(336769304)
                ->execute();

            return $response;
        }
    }
}
