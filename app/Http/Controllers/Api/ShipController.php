<?php

namespace App\Http\Controllers\Api;

use App\Ship;

class ShipController extends ApiController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Ship::all()->groupBy('race');
    }
}
