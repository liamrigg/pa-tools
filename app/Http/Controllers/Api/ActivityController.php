<?php

namespace App\Http\Controllers\Api;

use App\Services\Activity\ActivityList;
use Illuminate\Http\Request;

class ActivityController extends ApiController
{
    /**
     * Show the activity log.
     *
     * @return ActivityList
     */
    public function index(Request $request, ActivityList $activities)
    {
        return $activities
            ->setDays($request->input('days', 0))
            ->setUserId($request->input('user_id', null))
            ->execute();
    }
}
