<?php

namespace App\Http\Controllers\Api;

use App\Schedule;
use Illuminate\Http\Request;

class ScheduleController extends ApiController
{
    private $days = [
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
        'saturday',
        'sunday',
    ];

    private $hours = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24'];

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Schedule::get();
    }

    public function show($userId)
    {
        $schedule = Schedule::where('user_id', $userId)->get()->keyBy('datetime');

        $list = [];

        foreach ($this->days as $day) {
            foreach ($this->hours as $hour) {
                $datetime = $day.'_'.$hour;
                $list[$datetime] = isset($schedule[$datetime]) ? true : false;
            }
        }

        return $list;
    }

    public function store(Request $request)
    {
        $userId = $request->input('user_id');
        $dateTime = $request->input('datetime');
        $action = $request->input('action');

        if ($action == 'allow') {
            // delete any entries
            Schedule::where(['user_id' => $userId, 'datetime' => $dateTime])->delete();
        } else {
            // create entry
            $insert = [
                'user_id' => $userId,
                'datetime' => $dateTime,
            ];
            Schedule::insert($insert);
        }

        return 'success';
    }
}
