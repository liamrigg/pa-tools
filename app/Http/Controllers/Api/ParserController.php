<?php

namespace App\Http\Controllers\Api;

use App\Services\ScanParser;
use Illuminate\Http\Request;

class ParserController extends ApiController
{
    public function store(Request $request, ScanParser $parser)
    {
        return $parser
            ->setRespond(true)
            ->queue($request->input('url'));
    }
}
