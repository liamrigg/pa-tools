<?php

namespace App\Http\Controllers\Api;

use App\Planet;
use App\PlanetHistory;
use App\Services\Transit;
use App\Services\User\ChangeRole;
use App\Services\User\UserDelete;
use App\Services\User\UserDisable;
use App\Services\User\UserEnable;
use App\Services\User\UserList;
use App\User;
use Config;
use Illuminate\Http\Request;
use Twilio;

class MembersController extends ApiController
{
    /**
     * Shows a list of users
     **/
    public function index(Request $request, UserList $users)
    {
        return $users
            ->setType($request->input('type', 'enabled'))
            ->execute();
    }

    public function show($id)
    {
        if (! $this->hasRole('Admin')) {
            return;
        }

        $user = User::with(['planet' => function ($q) {
            $q->select(['id', 'x', 'y', 'z']);
        }])->find($id)->toArray();

        return collect($user);
    }

    public function update($id, Request $request)
    {
        $user = User::find($id);
        $user->fill($request->all());

        if ($request->input('x') && $request->input('y') && $request->input('z')) {
            $planet = PlanetHistory::where('x', $request->input('x'))
                ->where('y', $request->input('y'))
                ->where('z', $request->input('z'))
                ->orderBy('tick', 'DESC')
                ->first();
            if ($planet) {
                $planet = Planet::find($planet->planet_id);
            }
            $user->planet_id = (isset($planet)) ? $planet->id : 0;
        }

        $user->save();

        return $user;
    }

    public function call($id, Transit $transit)
    {
        $user = User::find($id);

        if (Config::get('twilio.twilio.enabled')) {
            $voice = url('/outgoing');

            if ($user->phone) {
                Twilio::call($user->phone, $voice, ['Timeout' => 10]);
                sleep(15);
            }
        }

        if (Config::get('INFOBIP_ENABLED')) {
            return $transit->call(addslashes($user->name));
        }
    }

    public function enable($id, UserEnable $userEnable)
    {
        return $userEnable
            ->setId($id)
            ->execute();
    }

    public function disable($id, UserDisable $userDisable)
    {
        return $userDisable
            ->setId($id)
            ->execute();
    }

    public function role($id, $roleId, ChangeRole $role)
    {
        return $role
            ->setId($id)
            ->setRoleId($roleId)
            ->execute();
    }

    public function delete($id, UserDelete $userDelete)
    {
        return $userDelete
            ->setId($id)
            ->execute();
    }
}
