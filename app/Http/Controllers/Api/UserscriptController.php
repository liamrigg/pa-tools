<?php

namespace App\Http\Controllers\Api;

use App\Services\MyBookings;
use App\Services\Scans\ScanRequestList;
use App\Tick;
use App\User;
use Illuminate\Http\Request;
use View;

class UserscriptController
{
    /**
     * Update the users account information.
     */
    public function bookings(Request $request, MyBookings $bookings)
    {
        $token = $request->input('api_token');
        $fleet1 = $request->input('1');
        $fleet2 = $request->input('2');
        $fleet3 = $request->input('3');

        if (! $token) {
            return response()->json(['error' => 'Missing token.'], 403);
        }

        if (! $user = User::where('token', $token)->first()) {
            return response()->json(['error' => 'Not authorized.'], 403);
        }

        $user->has_user_script = true;
        $user->save();

        $bookings = $bookings
            ->setUserId($user->id)
            ->setAttackId($request->input('attack_id', null))
            ->execute();

        $tick = Tick::orderBy('tick', 'desc')->first()->tick;

        return View::make('userscript.bookings', [
            'bookings' => $bookings,
            'tick' => $tick,
            'fleets' => [
                1 => $fleet1,
                2 => $fleet2,
                3 => $fleet3,
            ],
        ]);
    }

    public function menu(Request $request)
    {
        $token = $request->input('api_token');

        if (! $user = User::where('token', $token)->first()) {
            return response()->json(['error' => 'Not authorized.'], 403);
        }

        $user->has_user_script = true;
        $user->save();

        return View::make('userscript.menu', [
            'name' => env('APP_NAME', 'VenoX Tools'),
        ]);
    }

    public function requests(Request $request, ScanRequestList $requests)
    {
        $token = $request->input('api_token');

        if (! $user = User::where('token', $token)->first()) {
            return response()->json(['error' => 'Not authorized.'], 403);
        }

        $list = $requests->execute();

        return View::make('userscript.requests', [
            'request' => $list,
        ]);
    }
}
