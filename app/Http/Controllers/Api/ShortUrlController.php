<?php

namespace App\Http\Controllers\Api;

use App\ShortUrl;

class ShortUrlController
{
    public function show($id)
    {
        $url = ShortUrl::where('key', $id)->first();

        if ($url) {
            return redirect($url->url);
        }

        return response()->json(['error' => 'Can not find that short url'], 422);
    }
}
