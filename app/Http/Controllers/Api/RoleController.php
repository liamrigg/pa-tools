<?php

namespace App\Http\Controllers\Api;

use App\Role;

class RoleController extends ApiController
{
    public function index()
    {
        return Role::all();
    }
}
