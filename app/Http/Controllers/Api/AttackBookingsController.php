<?php

namespace App\Http\Controllers\Api;

use App\AttackBooking;
use App\FleetMovement;
use App\Services\Booking\BookingUser;
use App\Services\Booking\BookingUserList;
use App\Services\Booking\ChangeBookingStatus;
use App\Services\MyBookings;
use App\User;
use Auth;
use Illuminate\Http\Request;

class AttackBookingsController extends ApiController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, MyBookings $bookings)
    {
        return $bookings
            ->setUserId(Auth::user()->id)
            ->setAttackId($request->input('attack_id', null))
            ->setBattleGroupId($request->input('battlegroup_id', null))
            ->execute();
    }

    public function update($id, Request $request, ChangeBookingStatus $changeStatus)
    {
        $booking = AttackBooking::findOrFail($id);

        if ($booking->user_id != Auth::user()->id && ! $booking->users->where('id', Auth::user()->id)->count()) {
            return response()->json(['error' => 'Only owners or users of this booking can edit it.'], 422);
        }

        $booking = AttackBooking::findOrFail($id);

        if ($request->has('notes')) {
            $booking->notes = $request->input('notes', null);
        }

        if ($request->has('battle_calc')) {
            $booking->battle_calc = $request->input('battle_calc', null);
        }

        if ($request->has('bg')) {
            $booking->bg_id = $request->input('bg', null);
        }

        $booking->save();

        if ($request->has('status')) {
            $changeStatus->setId($id)
                ->setUser(Auth::user())
                ->setNewStatus($request->input('status', 'none'))
                ->execute();
        }

        
    }

    public function show($id, Request $request, MyBookings $bookings)
    {
        return $bookings
            ->setId($id)
            ->setUserId(Auth::user()->id)
            ->setAttackId($request->input('attack_id', null))
            ->execute();
    }

    public function users($id, Request $request, BookingUserList $users)
    {
        return $users
            ->setId($id)
            ->users();
    }

    public function nonUsers($id, Request $request, BookingUserList $users)
    {
        return $users
            ->setId($id)
            ->nonUsers();
    }

    public function addUser($id, Request $request, BookingUser $booking)
    {
        return $booking->setId($id)
            ->setUserId($request->input('user_id'))
            ->add();
    }

    public function removeUser($id, Request $request, BookingUser $booking)
    {
        return $booking->setId($id)
            ->setUserId($request->input('user_id'))
            ->delete();
    }

    public function fleets($id)
    {
        $booking = AttackBooking::with([
            'target',
            'target.planet' => function ($q) {
                $q->select(['id', 'x', 'y', 'z']);
            },
        ])->find($id);

        $fleets = FleetMovement::where('planet_to_id', $booking->target->planet_id)->with([
            'planetFrom' => function ($q) {
                $q->select([
                    'id',
                    'x',
                    'y',
                    'z',
                    'race',
                    'alliance_id',
                    'latest_p',
                    'latest_d',
                    'latest_u',
                    'latest_n',
                    'latest_j',
                    'latest_au',
                ]);
            },
            'planetFrom.latestP',
            'planetFrom.latestP.scan',
            'planetFrom.latestD',
            'planetFrom.latestD.scan',
            'planetFrom.latestJ',
            'planetFrom.latestJ.scan',
            'planetFrom.latestU',
            'planetFrom.latestU.u',
            'planetFrom.latestU.u.ship',
            'planetFrom.latestA',
            'planetFrom.latestA.au',
        ])->where('land_tick', $booking->land_tick)
            ->orderBy('mission_type')
            ->orderBy('fleet_name')
            ->get();

        return $fleets;
    }

    public function owner($id, Request $request)
    {
        $userId = $request->input('user_id');
        $booking = AttackBooking::find($id);

        $user = User::with('battlegroups')->find($userId);

        if ($user) {
            $bgs = $user->battlegroups->pluck('id')->toArray();
            if ($booking->user_id == Auth::user()->id || in_array($booking->bg_id, $bgs)) {
                $existingOwner = $booking->user_id;
                $booking->user_id = $user->id;
                $booking->save();
    
                $booking->users()->detach($userId);
                $booking->users()->syncWithoutDetaching([$existingOwner]);
            }
        }
    }

    public function drop($bookingId, MyBookings $bookings, Request $request)
    {
        $booking = AttackBooking::with('users')->where('id', $bookingId)->first();

        if ($booking) {
            if ($booking->user_id == Auth::user()->id && count($booking->users)) {
                return response()->json(['error' => 'Change owner before dropping target.'], 422);
            }

            // If the booking owner is the same as the user dropping the target and theres no users shared
            if ($booking->user_id == Auth::user()->id && ! count($booking->users)) {
                $booking->user_id = null;
                $booking->bg_id = null;
                $booking->save();
            } else {
                // Check if user dropping is one of the shared users
                if ($booking->users->where('id', Auth::user()->id)->count()) {
                    $booking->users()->detach(Auth::user()->id);
                }
            }
        }

        return $bookings
            ->setUserId(Auth::user()->id)
            ->setAttackId($request->input('attack_id', ''))
            ->execute();
    }
}
