<?php

namespace App\Http\Controllers\Api;

use App\Services\Account\AccountUpdate;
use App\User;
use Auth;
use Illuminate\Http\Request;

class AccountController extends ApiController
{
    /**
     * Show the users account information.
     *
     * @return User
     */
    public function index()
    {
        return User::with('planet')
            ->find(Auth::user()->id);
    }

    /**
     * Update the users account information.
     */
    public function update($id, Request $request, AccountUpdate $account)
    {
        return $account
            ->setUserId(Auth::user()->id)
            ->setX($request->input('x', null))
            ->setY($request->input('y', null))
            ->setZ($request->input('z', null))
            ->setPassword($request->input('password'))
            ->setName($request->input('name'))
            ->setTimezone($request->input('timezone'))
            ->setPhone($request->input('phone'))
            ->setNotes($request->input('notes'))
            ->setDistorters($request->input('distorters'))
            ->setMilitaryCentres($request->input('military_centres'))
            ->setStealth($request->input('stealth'))
            ->setAllowCalls($request->input('allow_calls'))
            ->setAllowNight($request->input('allow_night'))
            ->setAllowNotifications($request->input('allow_notifications'))
            ->setNotificationEmail($request->input('notification_email'))
            ->setNotificationEmailForward($request->input('notification_email_forward'))
            ->execute();
    }
}
