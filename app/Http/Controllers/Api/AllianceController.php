<?php

namespace App\Http\Controllers\Api;

use App\Alliance;
use App\Planet;
use App\Services\Alliance\AllianceGrowth;
use App\Services\Alliance\AllianceList;
use App\Services\Alliance\LandTicksChart;
use App\Services\History\HistoryList;
use App\Services\RaceChart;
use App\Services\ShipChart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class AllianceController extends ApiController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, AllianceList $alliance)
    {
        return $alliance->setSort($request->input('sort', '-score'))
            ->setPage($request->input('page', 1))
            ->setPerPage($request->input('perPage', 20))
            ->setAlliance($this->settings['alliance'])
            ->execute();
    }

    public function show($id, Request $request)
    {
        if ($id) {
            $alliance = Alliance::with(['planets' => function ($query) {
                $query->orderBy('x', 'ASC');
                $query->orderBy('y', 'ASC');
                $query->orderBy('z', 'ASC');
            }, 'planets.latestA', 'planets.latestA.au', 'planets.latestA.au.ship', 'planets.latestP', 'planets.latestP.scan', 'planets.latestD', 'planets.latestD.scan'])->where('id', $id)->first();

        } else {
            $alliance = new \stdClass();
            $alliance->id = 0;
            $alliance->planets = Planet::with('outgoing', 'incoming', 'latestA', 'latestA.au', 'latestA.au.ship', 'latestP', 'latestP.scan', 'latestD', 'latestD.scan')
                ->whereNull('alliance_id')
                ->orderBy('x', 'ASC')
                ->orderBy('y', 'ASC')
                ->orderBy('z', 'ASC')->get();
        }

        return collect($alliance);
    }

    public function update($id, Request $request)
    {
        $alliance = Alliance::find($id);

        $relation = $request->input('relation');
        $nickname = $request->input('nickname');

        $alliance->update([
            'nickname' => $nickname ?? $alliance->nickname,
            'relation' => $relation ?? $alliance->relation,
        ]);

        return $alliance;
    }

    public function development($id, Request $request)
    {
        $development = Planet::join('scans_development', 'scans_development.id', '=', 'planets.latest_d')
            ->where('alliance_id', $id)
            ->get();

        $cons = [
            'fc' => [
                'total' => $development->sum('finance_centre'),
                'avg' => number_format($development->avg('finance_centre'), 0),
            ],
            'mr' => [
                'total' => $development->sum('metal_refinery'),
                'avg' => number_format($development->avg('metal_refinery'), 0),
            ],
            'cr' => [
                'total' => $development->sum('crystal_refinery'),
                'avg' => number_format($development->avg('crystal_refinery'), 0),
            ],
            'er' => [
                'total' => $development->sum('eonium_refinery'),
                'avg' => number_format($development->avg('eonium_refinery'), 0),
            ],
            'has_magma' => $development->sum(function ($planet) {
                return $planet['core'] == 4;
            }),
            'amps' => [
                'total' => $development->sum('wave_amplifier'),
                'avg' => number_format($development->avg('wave_amplifier'), 0),
            ],
            'dists' => [
                'total' => $development->sum('wave_distorter'),
                'avg' => number_format($development->avg('wave_distorter'), 0),
            ],
            'has_inc' => $development->sum(function ($planet) {
                return in_array($planet['waves'], ['I', 'J', 'A']);
            }),
            'sc' => [
                'total' => $development->sum('security_centre'),
                'avg' => number_format($development->avg('security_centre'), 0),
            ],
            'sd' => [
                'total' => $development->sum('structure_defence'),
                'avg' => number_format($development->avg('structure_defence'), 0),
            ],
        ];

        return $cons;
    }

    public function races($id, RaceChart $chart)
    {
        return $chart
            ->setType('alliance')
            ->setId($id)
            ->execute();
    }

    public function ships($id, ShipChart $chart)
    {
        return $chart
            ->setType('alliance')
            ->setId($id)
            ->execute();
    }

    public function growth($id, AllianceGrowth $growth)
    {
        return $growth
            ->setId($id)
            ->execute();
    }

    public function landTicks($id, Request $request, LandTicksChart $landTicks)
    {
        return $landTicks
            ->setId($id)
            ->setType($request->input('type', 'Attack'))
            ->execute();
    }

    public function history($id, Request $request, HistoryList $history)
    {
        return $history
            ->setId($id)
            ->setType('alliance')
            ->setLimit($request->input('limit', 10))
            ->execute();
    }

    public function export($id)
    {
        $alliance = Alliance::with(['planets' => function ($q) {
            $q->select([
                'x',
                'y',
                'z',
                'ruler_name',
                'planet_name',
                'race',
                'size',
                'value',
                'score',
                'alliance_id',
            ]);
            $q->orderBy('x', 'ASC');
            $q->orderBy('y', 'ASC');
            $q->orderBy('z', 'ASC');
        }])->find($id);

        $filename = $alliance->name.'_'.time().'.csv';

        $array = $alliance->planets->toArray();
        $delimiter = ';';

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="'.$filename.'"',
        ];

        $f = fopen('php://output', 'w');

        fputcsv($f, [
            'x', 'y', 'z', 'ruler name', 'planet name', 'race', 'size', 'value', 'score',
        ], $delimiter);

        foreach ($array as $line) {
            unset($line['alliance']);
            unset($line['alliance_id']);
            unset($line['basher']);
            unset($line['is_alliance']);
            unset($line['is_protected']);
            unset($line['coords']);
            fputcsv($f, $line, $delimiter);
        }
        fclose($f);

        return Response::make('', 200, $headers);
    }
}
