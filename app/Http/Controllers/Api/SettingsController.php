<?php

namespace App\Http\Controllers\Api;

use App\Tick;

class SettingsController extends ApiController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->settings;
    }

    public function current()
    {
        return Tick::orderBy('tick', 'DESC')->first();
    }
}
