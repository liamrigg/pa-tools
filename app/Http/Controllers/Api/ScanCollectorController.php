<?php

namespace App\Http\Controllers\Api;

use App\Planet;
use App\Scan;
use App\ScanQueue;
use App\Services\ScanParser;
use App\User;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;

class ScanCollectorController
{
    public function index(Request $request)
    {
        $type = $request->input('type');
        $id = $request->input('id');

        if (! $type || ! $id) {
            return;
        }

        $ids = [];

        if ($type == 'alliance') {
            $ids = Planet::where('alliance_id', $id)->pluck('id');
        }
        if ($type == 'galaxy') {
            $ids = Planet::where('galaxy_id', $id)->pluck('id');
        }
        if ($type == 'planet') {
            $ids = Planet::where('id', $id)->pluck('id');
        }

        $scans = Scan::whereIn('planet_id', $ids)->get();

        $scanTicks = [];

        foreach ($scans as $scan) {
            $scanTicks[$scan->tick][] = $scan;
        }

        krsort($scanTicks);

        return $scanTicks;
    }

    public function store(Request $request, ScanParser $parser)
    {
        $token = $request->input('api_token');

        if (! $user = User::where('token', $token)->first()) {
            return response()->json(['error' => 'Not authorized.'], 403);
        }

        $user->has_user_script = true;
        $user->save();

        $ids = $request->input('ids');
        $instant = $request->input('instant', false);

        $ids = array_unique($ids);

        $existing = ScanQueue::whereIn('scan_id', $ids)->get()->keyBy('scan_id')->toArray();

        $newScans = [];

        $now = Carbon::now();

        foreach ($ids as $id) {
            if (! isset($existing[$id]) && $id) {
                $newScans[] = [
                    'scan_id' => $id,
                    'updated_at' => $now,
                    'created_at' => $now,
                ];
            }
        }

        if ($instant) {
            $scanUrl = Config::get('pa.scan_url');

            foreach ($newScans as $scan) {
                $parser->parse($scanUrl.$scan['scan_id']);
            }
        } else {
            ScanQueue::insert($newScans);
        }

        return response()->json();
    }
}
