<?php

namespace App\Http\Controllers\Api;

use App\Alliance;
use App\AttackBooking;
use App\AttackTarget;
use App\Defence;
use App\FleetMovement;
use App\Planet;
use App\Services\Bot\Notify;
use App\Services\Bot\NotifyUser;
use App\Setting;
use App\User;
use Illuminate\Http\Request;

class FleetCollectorController
{
    private $notify;

    private $notifyUser;

    private $allianceId;

    public function __construct(Notify $notify, NotifyUser $notifyUser)
    {
        $this->notify = $notify;
        $this->notifyUser = $notifyUser;

        $alliance = Setting::where('name', 'alliance')->first();

        if ($alliance) {
            $this->allianceId = $alliance->value;
        }
    }

    public function store(Request $request)
    {
        $token = $request->input('api_token');
        $source = $request->input('source');

        if (! $user = User::where('token', $token)->first()) {
            return response()->json(['error' => 'Not authorized.'], 403);
        }

        $user->has_user_script = true;
        $user->save();

        $fleets = $request->input('fleets');

        $calls = [];
        $allRecalled = [];

        $bookings = [];

        foreach ($fleets as $fleet) {
            $to = Planet::where([
                'x' => $fleet['to_x'],
                'y' => $fleet['to_y'],
                'z' => str_replace('!', '', $fleet['to_z']),
            ])->first();
            $from = Planet::where([
                'x' => $fleet['from_x'],
                'y' => $fleet['from_y'],
                'z' => str_replace('!', '', $fleet['from_z']),
            ])->first();

            $eta = $fleet['eta'];
            $landTick = $fleet['tick'] + $eta;
            $type = $fleet['type'];

            // if ($source == 'userscript_alliance_fleets' && $type == 'Attack' && $eta == 1) {
            //     $bookingId = $this->recallWarning($fleet, $landTick);
            //     if ($bookingId) {
            //         $bookings[$bookingId] = $bookingId;
            //     }
            // }

            if ($this->allianceId == $to->alliance_id && $type == 'Attack') {
                $defence = Defence::firstOrCreate([
                    'planet_to_id' => $to->id,
                    'land_tick' => $landTick,
                ]);

                if ($source == 'userscript_alliance_defence') {
                    $alliancePlanets = Planet::where('alliance_id', $this->allianceId)->pluck('id');
                    // If we havent done it for this defence call already, set all ALLIANCE defenders on this defence call to recalled
                    if (! isset($calls[$defence->id])) {
                        FleetMovement::where([
                            'land_tick' => $landTick,
                            'planet_to_id' => $to->id,
                            'mission_type' => 'Defend',
                        ])->whereIn('planet_from_id', $alliancePlanets)
                            ->update(['is_recalled' => true]);
                    }

                    $calls[$defence->id] = $defence;
                }
            }

            $isRecalled = false;

            if (isset($fleet['is_recalled']) && $fleet['is_recalled']) {
                $isRecalled = true;
            }

            $ships = $fleet['ships'];

            if (strstr($fleet['ships'], 'K')) {
                $ships = (int) $ships * 1000;
            }

            if (strstr($fleet['ships'], 'M')) {
                $ships = (int) $ships * 1000000;
            }

            if (strstr($fleet['ships'], 'B')) {
                $ships = (int) $ships * 1000000000;
            }

            $fm = [
                'fleet_name' => $fleet['fleet'],
                'planet_from_id' => $from->id,
                'planet_to_id' => $to->id,
                'mission_type' => $type,
                'land_tick' => $landTick,
                'tick' => $fleet['tick'],
                'eta' => $eta,
                'ship_count' => $ships,
                'source' => $source,
                'is_recalled' => $isRecalled,
                'is_prelaunched' => 0,
            ];

            if (! $movement = FleetMovement::where([
                'land_tick' => $fleet['tick'] + $fleet['eta'],
                'planet_from_id' => $from->id,
                'planet_to_id' => $to->id,
            ])->first()) {
                FleetMovement::insert($fm);

                // If we had to create a fleet movement for a member planet being attacked we know they dont
                // have notifications configured
                $user = User::where('planet_id', $fm['planet_to_id'])->first();
                if ($user && $fm['mission_type'] == 'Attack') {
                    $user->update([
                        'has_notifications' => 0,
                    ]);
                }
            } else {
                if ($isRecalled) {
                    $fm['is_recalled'] = $isRecalled;

                    // is new recall
                    if (! $movement->is_recalled && $to->alliance_id == $this->allianceId) {
                        $defenderNames = [];
                        // send recall notification to notification channel
                        if ($fm['mission_type'] == 'Attack') {
                            $name = $to->x.':'.$to->y.':'.$to->z;
                            if ($to->user && isset($to->user->id)) {
                                if (isset($to->user->botUser) && $to->user->botUser->username) {
                                    $name = '@'.$to->user->botUser->username;
                                } else {
                                    $name = $to->user->name;
                                }
                            }
                            $message = 'Attacker ['.$from->x.':'.$from->y.':'.$from->z.'] recalled from '.$to->x.':'.$to->y.':'.$to->z.' ('.$name.') eta '.$fm['eta'] . '[user: ' . $user->id . ']';
                            $this->notify
                                ->setMessage($message)
                                ->execute();

                            // if all attackers on eta recalled, send notification channel and message defenders
                            $otherAttackers = FleetMovement::where([
                                'mission_type' => 'Attack',
                                'planet_to_id' => $to->id,
                                'land_tick' => $movement->land_tick,
                                'is_recalled' => 0,
                            ])->where('id', '!=', $movement->id)->count();

                            if (! $otherAttackers) {
                                echo 'All recalled'.PHP_EOL;
                                $message = 'All attackers on '.$to->x.':'.$to->y.':'.$to->z.' ('.$name.') eta '.$fm['eta'].' have recalled [user: ' . $user->id . ']'.PHP_EOL.PHP_EOL;

                                $allRecalled[] = [
                                    'planet_to_id' => $to->id,
                                    'land_tick' => $movement->land_tick,
                                    'mission_type' => 'Defend',
                                    'is_recalled' => 0,
                                    'message' => $message,
                                ];
                            }
                        }
                    }
                }
                $fm = array_merge($movement->toArray(), $fm);
                FleetMovement::where('id', $movement->id)->update($fm);
            }
        }

        if (count($allRecalled)) {
            foreach ($allRecalled as $recalled) {
                $defenderNames = [];
                $message = $recalled['message'];
                unset($recalled['message']);
                $defenders = FleetMovement::with([
                    'planetFrom',
                    'planetFrom.user',
                    'planetFrom.user.botUser',
                ])->where($recalled)->get();

                foreach ($defenders as $defender) {
                    $name = $defender->planetFrom->x.':'.$defender->planetFrom->y.':'.$defender->planetFrom->z;
                    if (isset($defender->planetFrom->user->name)) {
                        $name = $defender->planetFrom->user->name;
                    }
                    if (isset($defender->planetFrom->user->botUser) && ! empty($defender->planetFrom->user->botUser->username)) {
                        $name = '@'.$defender->planetFrom->user->botUser->username;
                    }
                    $defenderNames[] = $name;
                }

                if (count($defenderNames)) {
                    $message .= implode(', ', $defenderNames).' can recall defence.';
                }

                $this->notify
                    ->setMessage($message)
                    ->execute();
            }
        }

        if (count($bookings)) {
            foreach($bookings as $bookingId) {
                AttackBooking::where('id', $bookingId)->update("crash_announced", 1);
            }
        }

        return response()->json();
    }

    private function recallWarning($fleet, $lt) {
        $fromX = $fleet['from_x'];
        $fromY = $fleet['from_y'];
        $fromZ = $fleet['from_z'];
        $targetX = $fleet['to_x'];
        $targetY = $fleet['to_y'];
        $targetZ = $fleet['to_z'];

        $from = Planet::where([
            'x' => $fromX,
            'y' => $fromY,
            'z' => $fromZ,
        ])->first();

        $user = User::with('botUser')->where([
            'planet_id' => $from->id
        ])->first();

        $target = Planet::where([
            'x' => $targetX,
            'y' => $targetY,
            'z' => $targetZ,
        ])->first();

        
        if ($target) {
            $booking = AttackBooking::with(['target', 'target.planet'])->whereHas('target.planet', function ($q) use ($targetX, $targetY, $targetZ) {
                $q->where('x', $targetX)->where('y', $targetY)->where('z', $targetZ);
            })->where('land_tick', $lt)->first();

            if ($booking && !$booking->crash_announced) {
                // Is booking set to recall or status not set
                if($booking->status != 'land') {
                    $name = $fromX.':'.$fromY.':'.$fromZ;
                    if (isset($user->name)) {
                        $name = $user->name;
                    }
                    if (isset($user->botUser) && ! empty($user->botUser->username)) {
                        $name = '@'.$user->botUser->username;
                    }
                    $this->notify
                        ->setMessage($name . " landing " . $targetX . ':' . $targetY . ':' . $targetZ . '  eta 1 & booking status ' . strtoupper($booking->status))
                        ->execute();

                    $this->notifyUser
                        ->setMessage("You are landing on " . $targetX . ':' . $targetY . ':' . $targetZ . '  eta 1 and your booking status is ' . strtoupper($booking->status))
                        ->setId($user->id)
                        ->execute();
                }
            }

            return $booking->id;
        }
    }
}
