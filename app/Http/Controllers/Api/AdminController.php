<?php

namespace App\Http\Controllers\Api;

use App\BotChat;
use App\Services\IntelParser;
use App\Setting;
use App\User;
use Artisan;
use Config;
use Illuminate\Http\Request;

class AdminController extends ApiController
{
    /**
     * Load the site admin settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::whereIn('name', [
            'alliance',
            'tg_channels',
            'tg_scans_channel',
            'tg_notifications_channel',
            'scans_per_second'
        ])->get();

        $enableWebScans = Setting::where('name', 'enable_web_scans')->first();
        $enableWebScans->value = intval($enableWebScans->value);
        $settings = $settings->push($enableWebScans);

        $showPoliticalDeals = Setting::where('name', 'show_political_deals')->first();
        $showPoliticalDeals->value = intval($showPoliticalDeals->value);
        $settings = $settings->push($showPoliticalDeals);

        $settings = $settings->pluck('value', 'name');

        if ($settings['tg_notifications_channel']) {
            $settings['tg_notifications_channel'] = BotChat::find($settings['tg_notifications_channel']);
        }

        if ($settings['tg_scans_channel']) {
            $settings['tg_scans_channel'] = BotChat::find($settings['tg_scans_channel']);
        }

        $settings['tg_admin'] = User::where('telegram_user_id', Config::get('telegram.admins'))->first();

        return $settings;
    }

    /**
     * Update site settings.
     */
    public function store(Request $request)
    {
        $request = $request->all();

        if (array_key_exists('overview', $request)) {
            Setting::where('name', 'overview')->update(['value' => $request['overview']]);
        }
        if (array_key_exists('attack', $request)) {
            Setting::where('name', 'attack')->update(['value' => $request['attack']]);
        }
        if (array_key_exists('alliance', $request)) {
            Setting::where('name', 'alliance')->update(['value' => $request['alliance']]);
        }
        if (array_key_exists('enable_web_scans', $request)) {
            Setting::where('name', 'enable_web_scans')->update(['value' => (int) $request['enable_web_scans']]);
        }
        if (array_key_exists('show_political_deals', $request)) {
            Setting::where('name', 'show_political_deals')->update(['value' => (int) $request['show_political_deals']]);
        }
        if (array_key_exists('scans_per_second', $request)) {
            Setting::where('name', 'scans_per_second')->update(['value' => $request['scans_per_second']]);
        }
    }

    /**
     * Reset webby.
     */
    public function reset()
    {
        Artisan::call('pa:reset');
        Artisan::call('parse:shipstats');

    }

    /**
     * Parse intel to webby.
     */
    public function intel(Request $request, IntelParser $parser)
    {
        try {
            return $parser->parse($request->input('xml'));
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 422);
        }
    }
}
