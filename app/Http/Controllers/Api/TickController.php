<?php

namespace App\Http\Controllers\Api;

use App\Tick;

class TickController extends ApiController
{
    /**
     * Show the application dashboard.
     */
    public function index()
    {
    }

    public function current()
    {
        return Tick::orderBy('tick', 'DESC')->first();
    }
}
