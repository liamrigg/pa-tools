<?php

namespace App\Http\Controllers\Api;

use App\AttackBooking;
use App\AttackTarget;

class AttackTargetController extends ApiController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function bookings($attackId, $targetId)
    {
        return AttackBooking::with(['user' => function ($q) {
            $q->select(['id', 'name']);
        }])->where([
            'attack_target_id' => $targetId,
            'attack_id' => $attackId,
        ])->get();
    }

    public function destroy($attackId, $targetId)
    {
        AttackTarget::where('id', $targetId)->delete();
        AttackBooking::where('attack_target_id', $targetId)->delete();
    }
}
