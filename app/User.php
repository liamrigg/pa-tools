<?php

namespace App;

use Config;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'planet_id',
        'phone',
        'timezone',
        'notes',
        'distorters',
        'military_centres',
        'is_enabled',
        'is_new',
        'last_login',
        'role_id',
        'telegram_user_id',
        'stealth',
        'allow_calls',
        'allow_night',
        'allow_notifications',
        'notification_email',
        'notification_email_forward',
        'has_notifications',
        'token',
        'has_user_script',
        'is_scanner',
    ];

    protected $appends = [
        'notification_email',
    ];

    protected $casts = [
        'allow_calls' => 'boolean',
        'allow_notifications' => 'boolean',
        'allow_night' => 'boolean',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getnotificationEmailAttribute($value)
    {
        if (Config::get('notifications.email_notifications.enabled')) {
            $email = Config::get('notifications.email_notifications.email_username');
            $email = str_replace('@gmail.com', '', $email);

            return $email.'+'.$this->id.'@gmail.com';
        }
    }

    public function scopeAdmin($query)
    {
        return $query->whereHas('role', function ($q) {
            $q->where('name', 'Admin');
        });
    }

    public function scopeMember($query)
    {
        return $query->whereHas('role', function ($q) {
            $q->where('name', 'Member');
        });
    }

    public function scopeBc($query)
    {
        return $query->whereHas('role', function ($q) {
            $q->where('name', 'BC');
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function planet()
    {
        return $this->hasOne(Planet::class, 'id', 'planet_id');
    }

    public function activities()
    {
        return $this->hasMany(Activity::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings()
    {
        return $this->hasMany(AttackBooking::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications()
    {
        return $this->hasMany(UserNotification::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sharedBookings()
    {
        return $this->belongsToMany(AttackBooking::class, 'attack_booking_user', 'user_id', 'booking_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function botUser()
    {
        return $this->hasOne(BotUser::class, 'id', 'telegram_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function battlegroups()
    {
        return $this->belongsToMany(BattleGroup::class, 'battlegroup_users', 'user_id', 'battlegroup_id')->where('is_pending', 0);
    }
}
