<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';

    protected $fillable = [
        'name',
        'value',
    ];

    protected function getValueAttribute($value)
    {
        if ($this->name == 'tg_channels') {
            return unserialize($value);
        }

        return $value;
    }
}
