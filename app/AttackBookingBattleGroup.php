<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttackBookingBattleGroup extends Model
{
    protected $table = 'attack_booking_battlegroup';

    protected $fillable = [
        'battlegroup_id',
        'booking_id',
        'teamup_id',
        'attack_id',
    ];
}
