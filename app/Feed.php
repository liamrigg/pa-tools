<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    protected $table = 'feed';

    protected $fillable = [
        'tick',
        'item_type',
        'item_text',
        'planet_id',
    ];
}
