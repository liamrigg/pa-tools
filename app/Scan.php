<?php

namespace App;

use Config;
use Illuminate\Database\Eloquent\Model;

class Scan extends Model
{
    protected $table = 'scans';

    protected $appends = [
        'type',
        'link',
    ];

    protected $fillable = [
        'planet_id',
        'scan_id',
        'scan_type',
        'pa_id',
        'tick',
        'time',
    ];

    public function getTypeAttribute()
    {
        $type = '';
        switch ($this->scan_type) {
            case 'App\PlanetScan':
                $type = 'P';
                break;
            case 'App\DevelopmentScan':
                $type = 'D';
                break;
            case 'App\UnitScan':
                $type = 'U';
                break;
            case 'App\NewsScan':
                $type = 'N';
                break;
            case 'App\JgpScan':
                $type = 'J';
                break;
            case 'App\AdvancedUnitScan':
                $type = 'A';
                break;
        }

        return $type;
    }

    public function getLinkAttribute()
    {
        return Config::get('pa.scan_url').$this->pa_id;
    }

    public function details()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function au()
    {
        return $this->hasMany(AdvancedUnitScan::class, 'scan_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function u()
    {
        return $this->hasMany(UnitScan::class, 'scan_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function planet()
    {
        return $this->belongsTo(Planet::class, 'planet_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tick()
    {
        return $this->belongsTo(Tick::class, 'tick', 'tick');
    }
}
