<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Config;

class Planet extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'planets';

    protected $appends = [
        'basher',
        'is_alliance',
        'is_protected',
        'coords',
    ];

    protected $fillable = [
        'planet_id',
        'nick',
        'alliance_id',
        'galaxy_id',
        'latest_p',
        'latest_d',
        'latest_u',
        'latest_j',
        'latest_au',
        'amps',
        'dists',
        'waves',
        'min_alert',
        'max_alert',
        'total_cons',
        'x',
        'y',
        'z',
        'ruler_name',
        'planet_name',
        'race',
        'size',
        'score',
        'value',
        'xp',
        'covop_hit',
        'rank_size',
        'rank_value',
        'rank_score',
        'rank_xp',
        'day_rank_size',
        'day_rank_value',
        'day_rank_score',
        'day_rank_xp',
        'day_size',
        'day_value',
        'day_score',
        'day_xp',
        'growth_value',
        'growth_score',
        'growth_size',
        'growth_xp',
        'growth_rank_value',
        'growth_rank_score',
        'growth_rank_size',
        'growth_rank_xp',
        'growth_percentage_value',
        'growth_percentage_score',
        'growth_percentage_size',
        'growth_percentage_xp',
        'last_covopped',
        'age',
        'rank_tick_roids',
        'total_cu',
        'total_rp',
    ];

    public function __toString()
    {
        $planet = sprintf("%s:%s:%s (%s) '%s' of '%s' ", $this->x, $this->y, $this->z, $this->race, htmlspecialchars($this->ruler_name), htmlspecialchars($this->planet_name));
        $planet .= sprintf('Score: %s (%s) ', number_shorten($this->score, 1), $this->rank_score);
        $planet .= sprintf('Value: %s (%s) ', number_shorten($this->value, 1), $this->rank_value);
        $planet .= sprintf('Size: %s (%s) ', number_format($this->size), $this->rank_size);
        $planet .= sprintf('Xp: %s (%s) ', number_format($this->xp), $this->rank_xp);

        return $planet;
    }

    public function getIsProtectedAttribute()
    {
        if ($this->age < 24) {
            return true;
        }

        return false;
    }

    public function getBasherAttribute()
    {
        $bashScore = '';
        $bashValue = '';
        if (! Auth::user() || ! Auth::user()->planet_id) {
            return false;
        }

        $user = User::with('planet')->find(Auth::user()->id);
        if (isset($user->planet)) {
            $bashScore = $user->planet->getOriginal('score') * Config::get('pa.bash.score');
            $bashValue = $user->planet->getOriginal('value') * Config::get('pa.bash.value');
        }
        if ($this->getOriginal('score') < $bashScore && $this->getOriginal('value') < $bashValue) {
            return true;
        }

        return false;
    }

    public function getCoordsAttribute()
    {
        return $this->x.':'.$this->y.':'.$this->z;
    }

    public function getWavesAttribute($value)
    {
        $waves = [
            '0' => 'P',
            '1' => 'L',
            '2' => 'D',
            '3' => 'U',
            '4' => 'N',
            '5' => 'I',
            '6' => 'J',
            '7' => 'A',
            '8' => 'M',
        ];

        return $waves[$value];
    }

    public function getIsAllianceAttribute($value)
    {
        $settings = Setting::all()->keyBy('name')->toArray();
        if ($this->alliance && isset($settings['alliance']) && $this->alliance->id == $settings['alliance']['value']) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function currentPlanet()
    {
        return $this->hasOne(PlanetHistory::class)->latest();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function history()
    {
        return $this->hasMany(PlanetHistory::class, 'planet_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function lastTick()
    {
        return $this->hasOne(PlanetHistory::class, 'planet_id')->orderBy('tick', 'DESC')->limit(1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function alliance()
    {
        return $this->belongsTo(Alliance::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function latestP()
    {
        return $this->hasOne(PlanetScan::class, 'id', 'latest_p');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function latestD()
    {
        return $this->hasOne(DevelopmentScan::class, 'id', 'latest_d');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function latestU()
    {
        return $this->hasOne(Scan::class, 'id', 'latest_u');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function latestA()
    {
        return $this->hasOne(Scan::class, 'id', 'latest_au');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function latestN()
    {
        return $this->hasOne(NewsScan::class, 'id', 'latest_n');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function latestJ()
    {
        return $this->hasOne(JgpScan::class, 'id', 'latest_j');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function galaxy()
    {
        return $this->hasOne(Galaxy::class, 'id', 'galaxy_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function outgoing()
    {
        return $this->hasMany(FleetMovement::class, 'planet_from_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function incoming()
    {
        return $this->hasMany(FleetMovement::class, 'planet_to_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'planet_id', 'id');
    }
}
