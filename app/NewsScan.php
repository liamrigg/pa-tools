<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsScan extends Model
{
    protected $table = 'scans_news';

    protected $fillable = [
        'scan_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function scan()
    {
        return $this->hasOne(Scan::class, 'id', 'scan_id');
    }
}
