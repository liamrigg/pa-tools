<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BattleGroupUser extends Model
{
    protected $table = 'battlegroup_users';
}
