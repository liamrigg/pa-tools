<?php

namespace App\Console\Commands;

use App\Alliance;
use App\AllianceRelation;
use App\Feed;
use App\Planet;
use App\Services\Bot\Notify;
use App\Setting;
use Config;
use Exception;
use Illuminate\Console\Command;

class ParseFeed extends Command
{
    private $coordsPattern = "/\((\d+):(\d+):(\d+)\)/";

    private $notify;

    private $toNotify = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pa:parseFeed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parses the PA User Feed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Notify $notify)
    {
        parent::__construct();

        $this->notify = $notify;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $feed = file_get_contents(Config::get('dumps.feed'));
        } catch (Exception $e) {
            return;
        }

        $dump = explode("\n", $feed);

        $items = array_slice($dump, 8);

        foreach ($items as $item) {
            $item = explode("\t", $item);

            if (! isset($item[0]) || ! isset($item[1]) || ! isset($item[2])) {
                continue;
            }

            $tick = $item[0];
            $itemType = mb_convert_encoding(trim($item[1], '"'), 'UTF-8');
            $itemText = mb_convert_encoding(trim($item[2], '"'), 'UTF-8');

            if (! $exists = Feed::where([
                'tick' => $tick,
                'item_type' => $itemType,
                'item_text' => $itemText,
            ])->first()) {
                $feed = [
                    'tick' => $tick,
                    'item_type' => $itemType,
                    'item_text' => $itemText,
                ];

                $feed = $this->checkForPlanet($feed, $itemText);

                Feed::create($feed);

                switch ($itemType) {
                    case 'Relation Change':
                        $this->handleRelationChange($itemText, $tick);
                        break;
                    case 'Anarchy':
                        $this->handleAnarchy($itemText);
                        break;
                }
            }
        }

        $this->sendNotifications();

        return 1;
    }

    private function sendNotifications()
    {
        $int = 1;

        if (count($this->toNotify)) {
            $text = implode(PHP_EOL, $this->toNotify);
            $this->notify
                ->setMessage($text)
                ->execute();
        }
    }

    private function handleRelationChange($itemText, $tick)
    {
        $warPattern = "/(.+) has declared war on (.+) \!/";

        preg_match($warPattern, $itemText, $warMatch);

        if ($warMatch) {
            $allianceOne = Alliance::where('name', $warMatch[1])->first();
            $allianceTwo = Alliance::where('name', $warMatch[2])->first();
            if (! $allianceOne || ! $allianceTwo) {
                return;
            }
            $ar = AllianceRelation::where([
                'type' => 'war',
                'alliance_one_id' => $allianceOne->id,
                'alliance_two_id' => $allianceTwo->id,
                'expires' => $tick + 48,
            ])->count();

            if (! $ar) {
                AllianceRelation::create([
                    'type' => 'war',
                    'alliance_one_id' => $allianceOne->id,
                    'alliance_two_id' => $allianceTwo->id,
                    'expires' => $tick + 48,
                ]);
            }
        }

        // Report relation change to Telegram
        $this->toNotify[] = $itemText;
    }

    private function checkForPlanet($feed, $itemText)
    {
        preg_match($this->coordsPattern, $itemText, $coordsMatch);

        if ($coordsMatch) {
            $x = $coordsMatch[1];
            $y = $coordsMatch[2];
            $z = $coordsMatch[3];

            $planet = Planet::with('user', 'alliance')->where([
                'x' => $x,
                'y' => $y,
                'z' => $z,
            ])->first();

            if (isset($planet->id)) {
                $feed['planet_id'] = $planet->id;
            }

            $ourAlliance = Setting::where('name', 'alliance')->first();

            if (isset($ourAlliance->value) && isset($planet->alliance->id)) {
                if ($ourAlliance->value == $planet->alliance->id) {
                    if (isset($planet->user->name)) {
                        $itemText = $planet->user->name.': '.$itemText;
                    }
                    $this->toNotify[] = $itemText;
                }
            }
        }

        return $feed;
    }

    private function handleAnarchy($text)
    {
        $entered = "/\((\d+):(\d+):(\d+)\) has entered anarchy until tick (\d+)\./";
        $exited = "/\((\d+):(\d+):(\d+)\) has exited anarchy\./";

        preg_match($entered, $text, $enteredMatch);
        preg_match($exited, $text, $exitedMatch);

        if ($enteredMatch) {
            $x = $enteredMatch[1];
            $y = $enteredMatch[2];
            $z = $enteredMatch[3];
            $tick = $enteredMatch[4];

            Planet::where([
                'x' => $x,
                'y' => $y,
                'z' => $z,
            ])->update(['anarchy' => $tick]);
        }

        if ($exitedMatch) {
            $x = $exitedMatch[1];
            $y = $exitedMatch[2];
            $z = $exitedMatch[3];

            Planet::where([
                'x' => $x,
                'y' => $y,
                'z' => $z,
            ])->update(['anarchy' => null]);
        }
    }
}
