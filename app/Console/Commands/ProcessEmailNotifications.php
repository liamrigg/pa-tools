<?php

namespace App\Console\Commands;

use App\FleetMovement;
use App\Planet;
use App\Setting;
use App\User;
use App\UserNotification;
use Config;
use Illuminate\Console\Command;
use Log;
use Longman\TelegramBot\Request;
use PhpImap\Exceptions\ConnectionException;
use App\Services\Bot\NotifyUser;
use App\Services\Bot\Notify;
use App;

class ProcessEmailNotifications extends Command
{
    private $incomingPattern = "/We have detected an open jumpgate from (.*), located at (\d+):(\d+):(\d+). The fleet will approach our system in tick (\d+) and appears to have (\d+) visible ships\./";

    private $recallPattern = "/The (.*) fleet from (\d+):(\d+):(\d+) has been recalled\./";

    private $landingPattern = '/Attacking fleet(s) landing next tick/';

    private $subjectPattern = "/Planetarion Notifications Mail For Tick (\d+)/";

    private $telegramNotificationChannel;

    private $mailBox;

    private $tick;

    private $incs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pa:processNotifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process email notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // If notifications aren't enabled, don't do anything
        if (! $enabled = Config::get('notifications.email_notifications.enabled')) {
            return;
        }

        $this->telegramNotificationChannel = Setting::where('name', 'tg_notifications_channel')->first();

        if (! $mailIds = $this->getNewEmails()) {
            return;
        }

        $this->processNewEmails($mailIds);
    }

    private function processNewEmails($mailIds)
    {
        foreach ($mailIds as $id) {
            $mail = $this->mailBox->getMail($id);

            Log::info('Processing email notification - id:'.$mail->id.'|email: '.$mail->toString.'|subject: '.$mail->subject);

            if (! $this->isPaNotification($mail)) {
                Log::info('Not a PA notification: '.$mail->id.' - '.$mail->subject);
                $this->mailBox->markMailAsRead($id);

                continue;
            }

            // if can't read user from email, skip email
            if (! $user = $this->getUserFromEmail($mail)) {
                Log::info('Unable to get user from email: '.$mail->id.' - '.$mail->toString);
                $this->mailBox->markMailAsRead($id);

                continue;
            }

            // if notifications already exist for user this tick, skip email
            if (UserNotification::where(['user_id' => $user->id, 'tick' => $this->tick])->count()) {
                Log::info('Notifications already exist for user: '.$user->id);
                $this->mailBox->markMailAsRead($id);

                continue;
            }

            // Get the notifications from the email body
            $body = $mail->textPlain;
            $notifications = explode("\r\n", $body);

            $this->incs = [];

            foreach ($notifications as $notification) {
                $this->processNotification($notification, $user);
            }

            $this->recordUserNotification($body, $user);

            foreach ($this->incs as $eta => $value) {
                $this->sendNotificationToChannel($user, $user->planet, $eta);
            }

            // Message user the notifications
            $this->sendNotificationToUser($user, $body);

            $this->mailBox->markMailAsRead($id);
        }
    }

    private function recordUserNotification($notification, $user)
    {
        UserNotification::create([
            'user_id' => $user->id,
            'tick' => $this->tick,
            'notification' => $notification,
        ]);
    }

    private function isPaNotification($mail)
    {
        // Check that it's a PA notification
        $subject = $mail->subject;

        preg_match($this->subjectPattern, $subject, $subjectMatch);

        if ($subjectMatch) {
            $this->tick = $subjectMatch[1];

            return true;
        }

        return false;
    }

    private function getNewEmails()
    {
        $username = Config::get('notifications.email_notifications.email_username');
        $password = Config::get('notifications.email_notifications.email_password');

        $this->mailBox = new \PhpImap\Mailbox(
            '{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX', // IMAP server and mailbox folder
            $username, // Username for the before configured mailbox
            $password, // Password for the before configured username
            false
        );

        // Looking for unseen emails from the PA email address
        $searchString = 'UNSEEN FROM "'.Config::get('notifications.email_notifications.pa_email').'"';

        try {
            // Get all emails (messages)
            // PHP.net imap_search criteria: http://php.net/manual/en/function.imap-search.php
            $mailIds = $this->mailBox->searchMailbox($searchString);
            //Log::info('Unread notifications: '.count($mailIds));
        } catch (ConnectionException $ex) {
            echo 'IMAP connection failed: '.$ex;
            exit();
        }

        // No new emails could be found
        if (! $mailIds) {
            return false;
        }

        return $mailIds;
    }

    private function getUserFromEmail($email)
    {
        // Get the user id the notification is for, from the recipient address
        $to = $email->toString;

        preg_match('/[+](\d+)[@]/', $to, $userId);

        // This is the user id on webby
        $userId = isset($userId[1]) ? $userId[1] : 0;

        // Get the users coords from user account
        if ($userId) {
            $user = User::with('planet')->find($userId);
            if (! isset($user->planet_id)) {
                return false;
            }
            $user->update(['has_notifications' => 1]);

            return $user;
        } else {
            return false;
        }
    }

    private function sendNotificationToChannel($user, $planetTo, $eta)
    {
        $notify = App::make(Notify::class);
        $notify
            ->setMessage('New incoming(s) reported for '.$user->name.' ('.$planetTo->x.':'.$planetTo->y.':'.$planetTo->z.') eta '.$eta)
            ->execute();
    }

    private function sendNotificationToUser($user, $notifications)
    {
        $notifyUser = App::make(NotifyUser::class);
        $notifyUser
            ->setId($user->id)
            ->setMessage($notifications)
            ->execute();
    }

    private function processNotification($notification, $user)
    {
        preg_match($this->incomingPattern, $notification, $incoming);
        preg_match($this->recallPattern, $notification, $recall);
        preg_match($this->landingPattern, $notification, $landing);

        if ($incoming) {
            $fullNotification = $incoming[0];
            $fleetName = $incoming[1];
            $x = $incoming[2];
            $y = $incoming[3];
            $z = $incoming[4];
            $landTick = $incoming[5];
            $visibleShips = $incoming[6];
            $planetFrom = Planet::where(['x' => $x, 'y' => $y, 'z' => $z])->first();
            $eta = ($landTick - $this->tick);
            $planetTo = $user->planet;

            $launched = FleetMovement::where([
                'land_tick' => $landTick,
                'planet_from_id' => $planetFrom->id,
                'planet_to_id' => $planetTo->id,
                'fleet_name' => $fleetName,
                'is_prelaunched' => 0,
            ])->first();

            $prelaunched = FleetMovement::where([
                'land_tick' => null,
                'planet_from_id' => $planetFrom->id,
                'planet_to_id' => $planetTo->id,
                'fleet_name' => $fleetName,
                'is_prelaunched' => 1,
            ])->first();

            if (! $launched && ! $prelaunched) {
                FleetMovement::create([
                    'launch_tick' => $this->tick,
                    'fleet_name' => $fleetName,
                    'land_tick' => $landTick,
                    'planet_from_id' => $planetFrom->id,
                    'planet_to_id' => $planetTo->id,
                    'mission_type' => 'Attack',
                    'tick' => $this->tick,
                    'eta' => $eta,
                    'ship_count' => $visibleShips,
                    'source' => 'notification',
                    'is_prelaunched' => 0,
                ]);
                echo "Fleet movement saved.\n";
            }

            if ($launched) {
                $launched->source = 'notification';
                $launched->save();
            }

            if ($prelaunched) {
                $prelaunched->launch_tick = $this->tick;
                $prelaunched->is_prelaunched = 0;
                $prelaunched->land_tick = $landTick;
                $prelaunched->source = 'notification';
                $prelaunched->save();
            }

            $this->incs[$eta] = true;

            // Request scans @todo
        }

        if ($recall) {
            $fleetName = $recall[1];
            $x = $recall[2];
            $y = $recall[3];
            $z = $recall[4];
            $planetTo = $user->planet;

            FleetMovement::where([
                'fleet_name' => $fleetName,
                'planet_to_id' => $planetTo->id,
            ])->where('land_tick', '>=', $this->tick)
                ->update(['is_recalled' => true]);
        }
    }
}
