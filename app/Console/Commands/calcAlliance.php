<?php

namespace App\Console\Commands;

use App\Alliance;
use DB;
use Illuminate\Console\Command;

class CalcAlliance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pa:calcAlliance {allianceId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('allianceId');

        $alliance = Alliance::findOrFail($id);

        $json = DB::table('planets')->select(['id', 'size', 'xp'])->whereNull('alliance_id')->get()->toJson();

        $num_items = $alliance->members;
        $target_size_xp = $alliance->size + $alliance->xp;

        $data = json_decode($json, true);

        $valid_combinations = [];

        $combos = $this->combinations($data, $num_items);

        foreach ($combos as $combo) {
            $total_size = array_sum(array_column($combo, 'size'));
            $total_xp = array_sum(array_column($combo, 'xp'));

            if ($total_size + $total_xp == $target_size_xp) {
                $valid_combinations[] = array_column($combo, 'id');
            }
        }

        print_r($valid_combinations);
    }

    private function combinations($array, $size)
    {
        if ($size == 0) {
            return [[]];
        }

        if (count($array) == 0) {
            return [];
        }

        $first = $array[0];
        $rest = array_slice($array, 1);

        $combinationsWithoutFirst = $this->combinations($rest, $size);
        $combinationsWithFirst = $this->combinations($rest, $size - 1);

        foreach ($combinationsWithFirst as &$combo) {
            array_unshift($combo, $first);
        }

        return array_merge($combinationsWithoutFirst, $combinationsWithFirst);
    }
}
