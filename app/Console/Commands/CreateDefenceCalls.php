<?php

namespace App\Console\Commands;

use App\Defence;
use App\FleetMovement;
use App\Planet;
use App\Setting;
use App\Tick;
use Illuminate\Console\Command;

class CreateDefenceCalls extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:defenceCalls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'A single use command for creating defence calls from fleet movements.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tick = Tick::orderBy('tick', 'DESC')->first();
        $allianceId = Setting::where('name', 'alliance')->first()->value;
        $planets = Planet::where('alliance_id', $allianceId)->get();

        foreach ($planets as $planet) {
            $movements = FleetMovement::where('land_tick', '>', $tick->tick)
                ->where('mission_type', 'Attack')
                ->where('planet_to_id', $planet->id)
                ->get();

            foreach ($movements as $movement) {
                Defence::firstOrCreate([
                    'planet_to_id' => $movement->planet_to_id,
                    'land_tick' => $movement->land_tick,
                ]);
            }
        }
    }
}
