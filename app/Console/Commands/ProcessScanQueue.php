<?php

namespace App\Console\Commands;

use App;
use App\ScanQueue;
use App\Setting;
use App\Services\ScanParser;
use Artisan;
use Config;
use Illuminate\Console\Command;

class ProcessScanQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pa:processScans';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process the command queue, this will do x at a time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $scanUrl = Config::get('pa.scan_url');

        $parsed = 0;

        $scansPerSecond = Setting::where('name', 'scans_per_second')->first();

        while (ScanQueue::where('processed', 0)->orderBy('id', 'ASC')->count()) {
            $scans = ScanQueue::where('processed', 0)->orderBy('id', 'ASC')->limit($scansPerSecond->value)->get();

            $parser = App::make(ScanParser::class);

            foreach ($scans as $scan) {
                $parser->parse($scanUrl.$scan->scan_id);
                $scan->processed = 1;
                $scan->save();
                $parsed++;
            }

            sleep(1);
        }

        // We parsed some scans, trigger some recalculations
        if ($parsed) {
            Artisan::call('pa:calcAlert');
            Artisan::call('pa:hiddenResources');
        }

    }
}
