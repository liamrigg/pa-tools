<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Misc\Bonus;
use App\Tick;
use App;
use App\PlanetHistory;
use App\Planet;

class CalcBonus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pa:calcBonus {tick?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $tick = $this->argument('tick');

        if ($tick) {
            $ticks = Tick::where('tick', $tick)->get();
        } else {
            $ticks = Tick::orderBy('tick', 'Asc')->get();
        }

        foreach ($ticks as $tick) {
            $bonus = App::make(Bonus::class)->setTick($tick->tick)->execute();

            $planets = PlanetHistory::select('planet_id', 'change_size', 'change_xp', 'x', 'y', 'z')->where('tick', $tick->tick)->get();

            foreach ($planets as $planet) {
                $totalRoids = $bonus['asteroid_bonus'] * 3;
                if ($totalRoids <= $planet->change_size && $totalRoids > 60 && $planet->change_xp == 0) {
                    Planet::where('id', $planet->planet_id)->update(['used_bonus' => $tick->tick]);
                    $this->info('Planet ' . $planet->x . ':' . $planet->y . ':' . $planet->z . ' has bonused at tick ' . $tick->tick);
                }
            }
        }
    }
}
