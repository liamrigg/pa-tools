<?php

namespace App\Console\Commands;

use App\Attack;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Services\Bot\Notify;
use App;

class AttackScheduler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pa:attackScheduler';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get current hour
        $hour = Carbon::now()->hour;

        $scheduled = [];
        $prereleased1 = [];
        $prereleased2 = [];

        $where1 = [
            'is_opened' => 0,
            'is_closed' => 0,
            'scheduled_time' => $hour,
        ];

        $where2 = [
            'is_opened' => 0,
            'prerelease_time' => $hour,
            'is_prereleased' => 0,
            'is_closed' => 0,
        ];

        $where3 = [
            'is_opened' => 0,
            'prerelease_time' => $hour.'30',
            'is_prereleased' => 0,
            'is_closed' => 0,
        ];

        // check if any attacks scheduled to open now
        $scheduled = Attack::where($where1)->get();

        Attack::where($where1)->update([
            'is_opened' => 1,
            'is_prereleased' => 0,
        ]);

        $prereleased1 = Attack::where($where2)->get();

        Attack::where($where2)->update([
            'is_prereleased' => 1,
        ]);

        if (Carbon::now()->minute >= 30) {
            $prereleased2 = Attack::where($where3)->get();

            Attack::where($where3)->update([
                'is_prereleased' => 1,
            ]);
        }

        foreach ($scheduled as $attack) {
            $this->notify($attack, "released");
        }

        foreach ($prereleased1 as $attack) {
            $this->notify($attack, "pre-released");
        }

        foreach ($prereleased2 as $attack) {
            $this->notify($attack, "pre-released");
        }
    }

    private function notify($attack, $type)
    {
        $url = App::make('url')->to('/').'/#/attacks/'.$attack->attack_id;
        $notify = App::make(Notify::class);
        $notify
            ->setMessage("Attack " . $type . ": " . $url)
            ->setPinMessage(true)
            ->execute();
    }
}
