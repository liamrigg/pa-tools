<?php

namespace App\Console\Commands;

use App\Setting;
use App\User;
use DB;
use Illuminate\Console\Command;

class ResetPa extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pa:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset application for a new round of Planetarion.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->clearTables();
        $this->clearUserFields();
        $this->clearSettings();
        $this->call('parse:shipStats');
    }

    /**
     * Clear last rounds data from specified tables
     *
     * @return void
     */
    private function clearTables()
    {
        DB::table('alliance_history')->truncate();
        DB::table('alliance_relations')->truncate();
        DB::table('alliances')->truncate();
        DB::table('attack_booking_users')->truncate();
        DB::table('attack_bookings')->truncate();
        DB::table('attack_targets')->truncate();
        DB::table('attacks')->truncate();
        DB::table('battlegroup_users')->truncate();
        DB::table('battlegroups')->truncate();
        DB::table('defence')->truncate();
        DB::table('feed')->truncate();
        DB::table('fleet_movements')->truncate();
        DB::table('galaxies')->truncate();
        DB::table('galaxy_history')->truncate();
        DB::table('intel_change')->truncate();
        DB::table('planet_history')->truncate();
        DB::table('planet_movements')->truncate();
        DB::table('planets')->truncate();
        DB::table('politics')->truncate();
        DB::table('scan_queue')->truncate();
        DB::table('scan_requests')->truncate();
        DB::table('scans')->truncate();
        DB::table('scans_advanced_unit')->truncate();
        DB::table('scans_development')->truncate();
        DB::table('scans_jgp')->truncate();
        DB::table('scans_news')->truncate();
        DB::table('scans_planet')->truncate();
        DB::table('scans_unit')->truncate();
        DB::table('ships')->truncate();
        DB::table('short_urls')->truncate();
        DB::table('ticks')->truncate();
        DB::table('user_notifications')->truncate();
    }

    /**
     * Clear any round specific User fields
     *
     * @return void
     */
    private function clearUserFields()
    {
        User::whereNotNull('id')->update([
            'planet_id' => null,
            'distorters' => 0,
            'military_centres' => 0,
            'has_notifications' => 0,
        ]);
    }

    /**
     * Clear any round specific Settings
     *
     * @return void
     */
    private function clearSettings()
    {
        Setting::where('name', 'alliance')->update(['value' => null]);
        Setting::where('name', 'attack')->update(['value' => null]);
    }
}
