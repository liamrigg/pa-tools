<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galaxy extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'galaxies';

    protected $fillable = [
        'x',
        'y',
        'hidden_resources',
        'name',
        'rank_size',
        'rank_value',
        'rank_score',
        'rank_xp',
        'ratio',
        'planet_count',
    ];

    public function gethasMembersAttribute($value)
    {
        $alliance = Setting::where('name', 'alliance')->first();

        if ($this->planets->where('alliance_id', $alliance->value)->count()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function currentGalaxy()
    {
        return $this->hasOne(GalaxyHistory::class)->latest();
    }

    public function getAlliancesAttribute()
    {
        $settings = Setting::all()->keyBy('name')->toArray();

        $ally = [];

        $planets = $this->planets()->get();

        foreach ($planets as $planet) {
            if ($planet->alliance) {
                $name = $planet->alliance->nickname;
                if (isset($ally[$planet->alliance->name])) {
                    $ally[$planet->alliance->name]['count']++;
                } else {
                    $ally[$planet->alliance->name]['count'] = 1;
                    $ally[$planet->alliance->name]['nickname'] = $name;
                    $ally[$planet->alliance->name]['name'] = $planet->alliance->name;
                    $ally[$planet->alliance->name]['relation'] = $planet->alliance->relation;
                    $ally[$planet->alliance->name]['is_alliance'] = ($planet->alliance->id == $settings['alliance']['value']) ? true : false;
                }
            }
        }

        ksort($ally);

        return $ally;
    }

    public function getRacesAttribute()
    {
        $races = [
            'Ter' => 0,
            'Cat' => 0,
            'Xan' => 0,
            'Zik' => 0,
            'Etd' => 0,
            'Sly' => 0,
            'Kin' => 0,
        ];

        $planets = $this->planets()->get();

        foreach ($planets as $planet) {
            $races[$planet->race]++;
        }

        return $races;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function history()
    {
        return $this->hasMany(GalaxyHistory::class, 'galaxy_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function planets()
    {
        return $this->hasMany(Planet::class, 'galaxy_id', 'id')->orderBy('z', 'ASC');
    }
}
