<?php

return [
    'enabled' => env('INFOBIP_ENABLED', 0),
    'api_url' => env('INFOBIP_URL', ''),
    'api_key' => env('INFOBIP_API_KEY', ''),
    'from' => env('INFOBIP_FROM', ''),
    'username' => env('INFOBIP_USERNAME', ''),
    'password' => env('INFOBIP_PASSWORD', ''),
];
