<?php

return [

    /*
     * Currently only supports gmail
     */
    'email_notifications' => [

        'enabled' => env('PA_NOTIFICATION_EMAIL_ENABLED', 0),

        /*
         * PA email address
         */
        'pa_email' => env('PA_NOTIFICATION_PA_EMAIL', 'support@planetarion.com'),

        /*
         * The email address that collects email notification from PA
         */
        'email_username' => env('PA_NOTIFICATION_EMAIL_USERNAME', ''),

        /*
         * The password of the email inbox that collects email notifications from PA
         */
        'email_password' => env('PA_NOTIFICATION_EMAIL_PASSWORD', ''),

        'server' => env('PA_NOTIFICATION_EMAIL_SERVER', ''),

        'port' => env('PA_NOTIFICATION_EMAIL_PORT'),

    ],

];
