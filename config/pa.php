<?php

return [
    'bcalc_url' => env('PA_URL', 'https://game.planetarion.com').'/bcalc.pl?id=',
    'group_scan_url' => env('PA_URL', 'https://game.planetarion.com').'/showscan.pl?scan_grp=',
    'races' => [
        'Terran' => 'Ter',
        'Cathaar' => 'Cat',
        'Xandathrii' => 'Xan',
        'Zikonian' => 'Zik',
        'Eitraides' => 'Etd',
        'Kinthia' => 'Kin',
        'Slythonian' => 'Sly',
    ],
    'scan_url' => env('PA_URL', 'https://game.planetarion.com').'/showscan.pl?scan_id=',
    'targetting' => [
        't1' => 1,
        't2' => 0.7,
        't3' => 0.5,
    ],
    'bash' => [
        'value' => 0.5,
        'score' => 0.6
    ],
    'research' => [
        'travel' => [
            1 => 1600,
            2 => 3200,
            3 => 4800,
            4 => 6400
        ],
        'infrastructure' => [
            1 => 1600,
            2 => 3200,
            3 => 4800,
            4 => 4800,
            5 => 7200,
            6 => 9600
        ],
        'hulls' => [
            1 => 1600,
            2 => 4800,
            3 => 7200
        ],
        'waves' => [
            1 => 1600,
            2 => 2400,
            3 => 2400,
            4 => 3600,
            5 => 4800,
            6 => 7200,
            7 => 7200
        ],
        'core' => [
            1 => 2400,
            2 => 4800,
            3 => 7200,
            4 => 7200
        ],
        'covert' => [
            1 => 1200,
            2 => 1600,
            3 => 2400,
            4 => 3200,
            5 => 4000,
            6 => 4800,
            7 => 5600,
            8 => 7200
        ],
        'mining' => [
            1 => 1200,
            2 => 1600,
            3 => 2400,
            4 => 3600,
            5 => 4800,
            6 => 6400,
            7 => 7200,
            8 => 7200,
            9 => 7200,
            10 => 7200,
            11 => 7200,
            12 => 7200,
            13 => 9600,
            14 => 9600,
            15 => 9600,
            16 => 9600
        ],
        'population' => [
            1 => 800,
            2 => 1200,
            3 => 1800,
            4 => 2400,
            5 => 3000
        ]
        ],
        'construction' => [
            'light_factory' => 500,
            'medium_factory' => 500,
            'heavy_factory' => 500,
            'wave_amplifier' => 800,
            'wave_distorter' => 600,
            'metal_refinery' => 750,
            'crystal_refinery' => 750,
            'eonium_refinery' => 750,
            'research_laboratory' => 500,
            'finance_centre' => 1000,
            'military_centre' => 750,
            'security_centre' => 900,
            'structure_defence' => 500
        ]
];
