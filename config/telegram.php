<?php

return [
    'bot' => [
        'enabled' => env('TELEGRAM_ENABLED', 0),

        'api_token' => env('TELEGRAM_API_TOKEN', ''),

        'username' => env('TELEGRAM_BOT_USERNAME', ''),

        'api_url' => env('TELEGRAM_API_URL'),
    ],

    'admins' => env('TELEGRAM_ADMINS', ''),

    'commands' => [
        'before' => true,
        'paths' => [
            base_path('/app/Telegram/Commands'),
        ],
        'configs' => [

        ],
    ],

];
