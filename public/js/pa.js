(function(w) {
	'use strict';
	var doc = w.document;
	if (!doc) return;

	var $j;
	var $pa;
	var skin = '';
	var page = '';
	var view = '';
	var tick = 0;
	var ticking = false;
	var globalDependencies = ['jQuery', 'get_cookie', 'get_ships_cookie', 'PA'];
	var races = { "Ter": 1, "Cat": 2, "Xan": 3, "Zik": 4, "Kin": 5, "Etd": 6 };
	let $availableFleets;
	var shipClasses = { "Fi": "Fighter", "Co": "Corvette", "Fr": "Frigate", 'De': 'Destroyer', 'Cr': 'Cruiser', 'Bs': 'Battleship' };
	var oldPage = false;

	if(doc.readyState == 'complete') {
		checkDeps(true);
	} else {
		var _ev = w.addEventListener ? {add: 'addEventListener', rem: 'removeEventListener', pfx: ''} : w.attachEvent ? {add: 'attachEvent', rem: 'detachEvent', pfx: 'on'} : null;
		if(_ev) {
			doc[_ev.add](_ev.pfx + 'DOMContentLoaded', waitLoad, false);
			doc[_ev.add](_ev.pfx + 'readystatechange', waitLoad, false);
			w[_ev.add](_ev.pfx + 'load', waitLoad, false);
		} else {
			checkDeps();
		}
	}

	function waitLoad(ev) {
		ev = ev || w.event;
		if(ev.type === 'readystatechange' && doc.readyState && doc.readyState !== 'complete' && doc.readyState !== 'loaded') return;
		if(_ev) {
			doc[_ev.rem](_ev.pfx + 'DOMContentLoaded', waitLoad);
			doc[_ev.rem](_ev.pfx + 'readystatechange', waitLoad);
			w[_ev.rem](_ev.pfx + 'load', waitLoad);
			_ev = null;
			checkDeps(true);
		}
	}

	function checkDeps(loaded) {
		var remainingDeps = globalDependencies.filter(function(dep) {
			return !w[dep];
		});
		if(!remainingDeps.length) {
			init();
		}
		else if (loaded) console.error(remainingDeps.length+' missing userscript dependenc'+(remainingDeps.length==1?'y':'ies')+': '+remainingDeps.join(', '));
	}

	function init() {
		if($j) return;
		$j = w.jQuery;
		$pa = w.PA;

		if(($pa.timediff+5000)<0) {
			oldPage = true;
		}

		loadMenu();

		if(scans.length) {
			loadScanRequests();
		}

		if($j('#ticking').length) {
			ticking = true;
		}

		$j('ul#menu').append('<div style="background:pink; color: black; text-align: center;padding: 5px; margin-top: 10px">Userscript Loaded</div>');

		var pageHtml = $j('html').html();

		parseScanLinks(pageHtml);

		if(typeof w.PA != 'undefined' && 'page' in w.PA) { 
			page = w.PA.page; 
		}
		if(typeof w.PA != 'undefined' && 'last_tick' in w.PA) { 
			tick = w.PA.last_tick; 
		}

		if(page == 'galaxy_status' && !oldPage) {
			if(!ticking) {
				parseGalStatus(pageHtml);
			}
		}

		if(page == 'alliance_defence' && !oldPage) {
			initAutoBattleCalcLink();
      		if(!ticking) {
				parseAllianceDefence();
			}
		}

		if(page == 'alliance_fleets' && !oldPage) {
			const fleetsPage = getAllianceFleetsPage();
			initAutoBattleCalcLink();
      		if(!ticking && fleetsPage === 'fleets') {
				parseAllianceFleets();
			}
			if (fleetsPage === 'launch') {
				enhanceAllyFleets();
			}
		}

		if(page == 'bcalc') {
			initBattleCalcData();
		}

		if(page == 'fleets') {
			getAttackBookings();
		}

		if(page == 'scan') {
			colorJgp();
		}
	}

	function getAllianceFleetsPage() {
		const searchParams = new URLSearchParams(location.search);
		let fleetsPage = searchParams.get('view');
		if (!fleetsPage) {
			fleetsPage = document.querySelector('form[method=post] input[type=hidden][name=view]')?.value;
		}
		return fleetsPage ? fleetsPage : 'fleets';
	}

	function colorJgp() {
		let scan = document.querySelector("#scan");
		let rows = scan.querySelectorAll("table tbody tr");
		Array.prototype.forEach.call(rows, function (element) {
			let cols = element.querySelectorAll(".left");
			Array.prototype.forEach.call(cols, function (leftcol) {
				if (leftcol.innerText == "Attack") {
					element.style.backgroundColor = 'rgba(90,0,0,.2)';
					element.style.outline = '#900000 1px solid';
				}
				if (leftcol.innerText == "Defend") {
					element.style.backgroundColor = 'rgba(0,40,0,.2)';
					element.style.outline = '#004000 1px solid';
				}
			});
		});
	}

	function parseAllianceFleets()
	{
		var $fleets = [];

		$("tr.mission_attack").each(function() {
			var $attacks = $(this).find("td");
			var $from = $attacks[0].innerText.split(":");
			var $fleetName = $attacks[1].innerText;
			var $missionType = $attacks[2].innerText;
			var $to = $attacks[3].innerText.split(":");
			var $ships = $attacks[4].innerText;
			var $eta = $attacks[5].innerText.split(" / ");
			if($eta[0] == $eta[1] || parseInt($eta[0]) < parseInt($eta[1])) {
				$eta = $eta[0];
			} else {
				var $prelaunch = $eta[0].split(" ");
				$eta = Number($prelaunch[0]) + Number($prelaunch[1].replace("(+", "").replace(")", ""));
			}
			$fleets.push({
				to_x: $to[0],
				to_y: $to[1],
				to_z: $to[2],
				from_x: $from[0],
				from_y: $from[1],
				from_z: $from[2],
				fleet: $fleetName,
				ships: Number($ships.replace(",","")),
				eta: $eta,
				tick: tick,
				type: $missionType
			});
		});

		$("tr.mission_defend").each(function() {
			var $attacks = $(this).find("td");
			var $from = $attacks[0].innerText.split(":");
			var $fleetName = $attacks[1].innerText;
			var $missionType = $attacks[2].innerText;
			var $to = $attacks[3].innerText.split(":");
			var $ships = $attacks[4].innerText;
			var $eta = $attacks[5].innerText.split(" / ");
			if($eta[0] == $eta[1] || $eta[0] < $eta[1]) {
				$eta = $eta[0];
			} else {
				var $prelaunch = $eta[0].split(" ");
				$eta = Number($prelaunch[0]) + Number($prelaunch[1].replace("(+", "").replace(")", ""));
			}
			$fleets.push({
				to_x: $to[0],
				to_y: $to[1],
				to_z: $to[2],
				from_x: $from[0],
				from_y: $from[1],
				from_z: $from[2],
				fleet: $fleetName,
				ships: Number($ships.replace(",","")),
				eta: $eta,
				tick: tick,
				type: $missionType
			});
		});

		collectFleets($fleets, "userscript_alliance_fleets");
	}

	function enhanceAllyFleets() {
		// Made by Damien (@damoxc), thank you.
		const fleetsTable = document.querySelector('#tablesort_0')

		$(".maintext table thead tr th:contains('Alliance ETA')").text("ETA");
		
		var AFs = 0;
		fleetsTable.querySelectorAll('tbody tr').forEach(row => {
			AFs = AFs + 1;
			row.dataset.coords = row.querySelector('td.coords').textContent;
			row.dataset.shipsId = Number(row.querySelector('td.right a').getAttribute('onclick').split(',')[1].replace(')', ''));
		})
	
		let showFleetsEnabled = localStorage.getItem('pa:af-show-ships') === 'true'
	
		const holder = document.createElement('div')
		holder.classList.add('center')
		holder.style = 'margin: 1rem';
	
		const btn = document.createElement('input')
		btn.type = 'button'
		btn.value = showFleetsEnabled ? 'Hide Ships' : 'Show Ships';
		btn.addEventListener('click', () => {
			localStorage.setItem('pa:af-show-ships', showFleetsEnabled ? 'false' : 'true')
	
			// Bit counter-intuitive, but if enabled is false we're enabling here
			if (!showFleetsEnabled) {
				enhanceAllyFleetsShowShips(fleetsTable)
				btn.value = 'Hide Ships'
				showFleetsEnabled = true;
			} else {
				enhanceAllyFleetsHideShips(fleetsTable)
				btn.value = 'Show Ships'
				showFleetsEnabled = false;
			}
		});
		holder.appendChild(btn)
		$(holder).append(`<p style="margin-top: 10px;">Alliance Fleets: ${AFs}</p>`)

	
		fleetsTable.parentNode.parentNode.insertBefore(holder, fleetsTable.parentNode)
	
		if (showFleetsEnabled) enhanceAllyFleetsShowShips(fleetsTable)
	}

	function enhanceAllyFleetsHideShips(fleetsTable) {
		// Made by Damien (@damoxc), thank you.
		const shipsHeader = fleetsTable.querySelector('thead th.right')
		shipsHeader.textContent = 'Ship count'
		shipsHeader.style = '';
		shipsHeader.classList.add('tablesort_asc')
	
		fleetsTable.querySelectorAll(':scope > tbody > tr').forEach(row => {
			const fleetShips = window.PA_dships[row.dataset.shipsId];
			const totalShips = fleetShips.reduce((a, b) => a + b.count, 0)
	
			row.querySelector('.right').innerHTML = `<a href="#" onclick="dfleet(event, ${row.dataset.shipsId})">${totalShips}</a>`
		});
	}
	
	function enhanceAllyFleetsShowShips(fleetsTable) {
		// Made by Damien (@damoxc), thank you.
		const shipsStats = Object.assign({}, ...Object.values(window.Ships).map(s => ({[s.name]: s})))
	
		const shipsHeader = fleetsTable.querySelector('thead th.right')
		shipsHeader.textContent = 'Ships'
		shipsHeader.style = 'width: 15rem';
		shipsHeader.classList.remove('tablesort_asc')
	
		fleetsTable.querySelectorAll('tbody tr').forEach(row => {
			const fleetShips = window.PA_dships[row.dataset.shipsId];
			fleetShips.forEach(ship => {
				const shipStats = shipsStats[ship.name]
				ship.value = (shipStats.metal + shipStats.crystal + shipStats.eonium) * ship.count
			});
			fleetShips.sort((a, b) => b.value - a.value)
	
			let bcalcShips = []
			const shipList = document.createElement('table')
			//shipList.style = 'width: 100%';
			fleetShips.forEach((ship, i) => {
				const shipListRow = document.createElement('tr')
	
				if (i > 2) shipListRow.style = 'display: none';
	
				const shipNameCell = document.createElement('td')
				shipNameCell.textContent = ship.name;
				shipNameCell.classList.add('left')
	
				const shipCountCell = document.createElement('td')
				shipCountCell.textContent = ship.count;
	
				shipListRow.append(shipNameCell, shipCountCell)
				shipList.appendChild(shipListRow)
	
				bcalcShips.push(`${ship.name}: ${ship.count}`);
			});
	
			const linksContainer = document.createElement('div');
			linksContainer.style = 'justify-content: space-between; flex-direction: row-reverse;  margin-top: 0.5rem;';
	
			const addToBcalcLink = document.createElement('a');
			addToBcalcLink.href = '#';
			addToBcalcLink.style = 'display: block';
			addToBcalcLink.setAttribute('onclick', `set_ships_cookie(event, 'af', '${row.dataset.shipsId}', '${bcalcShips.join('\\n')}');`);
			addToBcalcLink.textContent = 'Add To Bcalc';
			linksContainer.append(addToBcalcLink);
	
			const bcalcLinksContainer = document.createElement('div');
			bcalcLinksContainer.style = 'display: flex; justify-content: space-between; margin-top: 0.1rem;';
	
			const bcalcLinks = document.createElement('div');
			bcalcLinks.style = 'display: flex; justify-content: space-between; margin-top: 0.1rem;'
			bcalcLinks.classList.add('bcalc_links');
			bcalcLinks.id = `bcalc_link_${row.dataset.shipsId}`;
			bcalcLinksContainer.append(bcalcLinks);
	
			const bcalcTarget = document.createElement('div');
			bcalcTarget.classList.add('bcalc_target');
			bcalcTarget.style = 'display: flex; justify-content: space-between; margin-top: 0.1rem;'
			bcalcTarget.id = `bcalc_target_${row.dataset.shipsId}`;
			bcalcLinksContainer.append(bcalcTarget);
	
			if (fleetShips.length > 3) {
				let allShipsVisible = false;
				const showAllLink = document.createElement('a');
				showAllLink.href = '#'
				showAllLink.style = 'display: block';
				showAllLink.addEventListener('click', () => {
					shipList.querySelectorAll('tr').forEach((row, i) => {
						if (i > 2) row.style = allShipsVisible ? 'display: none' : '';
					});
					showAllLink.textContent = allShipsVisible ? 'Show All': 'Hide';
					allShipsVisible = !allShipsVisible;
				});
				showAllLink.textContent = 'Show All';
				linksContainer.append(showAllLink)
			}
	
			const shipsCell = row.querySelector('.right')
			shipsCell.innerHTML = '';
			shipsCell.append(shipList, linksContainer, bcalcLinksContainer)
		});
	}

	function loadMenu()
	{
		var params = "api_token="+apiToken;
		$j.ajax({
			url: toolsUrl + '/api/v1/userscript/menu?'+params,
			type: 'get',
			dataType: 'html',
			async: false,
			success: function(data) {
				$j('li#menu_help').after(data);
			}
		});
	}

	function loadScanRequests() {
		var $requests = "<div id='scan_requests' class='container'><div id='requests_content' style='display: none'></div><div class='header'>Open Scan Requests: "+scans.length+" [<a href='#' id='scanall'>attempt all</a>]</div><div class='maintext' style='padding-bottom: 5px; max-height: 100px; overflow-y: scroll'><table><thead><th></th><th>Target</th><th>Type</th><th>Dists</th><th>Link</th></thead>";
		$j.each(scans, function(index, request) {
			var $index = index+1;
			$requests = $requests + "<tr data-scan-key='"+$index+"'><td style='text-align:left'>#"+$index+"</td><td>"+request.planet.x+":"+request.planet.y+":"+request.planet.z+"</td><td>"+request.scan_type+"</td><td>"+request.planet.dists+"</td><td><a href='https://game.planetarion.com/waves.pl?id="+request.scan_type_id+"&x="+request.planet.x+"&y="+request.planet.y+"&z="+request.planet.z+"'>attempt scan</a></td></tr>";
		});
		$requests = $requests + "</table></div></div>";
		$j($requests).insertBefore('#notice');
		$('#scanall').on('click', function() {
			var $scansDone = 0;
			$j.each(scans, function(index, request) {
				index = index+1;
				var $url = "https://game.planetarion.com/waves.pl?id="+request.scan_type_id+"&x="+request.planet.x+"&y="+request.planet.y+"&z="+request.planet.z;
				$j.ajax({
					url: $url,
					type: 'get',
					dataType: 'html',
					async: false,
					success: function(data) {
						var $data = $(data).find('#stored_scans tr:nth-of-type(2)').html();
						$('#requests_content').append($data);
						$("[data-scan-key='"+index+"']").html("");
						$scansDone++;
					}
				});
			});
			var $reqcontent = $('#requests_content').first().html();
			parseScanLinks($reqcontent, true);
			$('#scan_requests').html("<div class='container'><div class='header'>Scans completed: "+$scansDone+"</div></div>");
			return false;
		});
	}

	function getAttackBookings() {
		var params = "api_token="+apiToken;
		var $fleets = getFleets();
		$fleets = new URLSearchParams($fleets).toString();

		$j.ajax({
			url: toolsUrl + '/api/v1/userscript/bookings?'+params+'&'+$fleets,
			type: 'get',
			dataType: 'html',
			async: false,
			success: function(data) {
				$j(data).insertBefore('#base_behavior');
				$('.launcher').on('change', function() {
					var $booking = $(this).parents('tr.booking');
					var $fleet = $(this).val();
					var $x = $booking.data('x');
					var $y = $booking.data('y');
					var $z = $booking.data('z');
					var $lt = $booking.data('lt');
					var $etas = new Array();
					var $eta = 0;
					var $int = 0;
					$("select[name=mission"+$fleet+"]").val(1);
					$("input[name=x"+$fleet+"]").val($x);
					$("input[name=y"+$fleet+"]").val($y);
					$("input[name=z"+$fleet+"]").val($z);
					$('td:contains(Universe:)').each(function() {
						$etas.push($(this).text().match(/Universe: (\d+)/g)[0].match(/\d+/g)[0]);
					});
					$.each($availableFleets, function(fleet, available) {
						if(available == 1 && fleet == $fleet) {
							$eta = $etas[$int];
						}
						if(available) {
							$int++;
						}
					});
					var launch = $lt-$eta+1;
					$("select[name=launch_tick"+$fleet+"]").val(launch);
				});
			}
		});
	}

	function getFleets() {
		$availableFleets = {
			1: $('select[name=mission1]').length,
			2: $('select[name=mission2]').length,
			3: $('select[name=mission3]').length,
		};
		return $availableFleets;
	}

	function initAutoBattleCalcLink() {
		$j('.ally_def_incs_wrapper').each(function() {
			var that = this;
			var linkDiv = $j(this).find('.ally_def_target_links');
			var bcLink = document.createElement("input");
			bcLink.setAttribute("type", "button");
			bcLink.setAttribute("value", "AUTO CALC");
			bcLink.setAttribute("style", "margin-top: 1px; margin-bottom: 4px;");
			bcLink.onclick = function() {
				generateBattleCalcData(that);
			};

			$j(linkDiv).after(bcLink);
		});
	}

	function getBaseFleet(userId) {
		var baseShips = [];
		$j.ajax({
			url: "https://game.planetarion.com/alliance_fleets.pl?view=free&member=" + userId,
			type: 'get',
			dataType: 'html',
			async: false,
			success: function(data) {
				$j(data).find('.maintext table:nth-of-type(2) td.left').each(function() {
					var $shipPANumber = $j(this).find('a').attr('class').match(/(\d+)/g)[0],
						$shipCount = $j(this).next().html();
					baseShips.push({
						type: $shipPANumber,
						count: $shipCount.replace(/,/g, '')
					});
				});
			}
		});
		return baseShips;
	}

	function getDefenceFleet(id) {
		var defShips = [];
		var fleet = w.PA_dships[id];
		$j.each(fleet, function(ind, ship) {
			defShips.push({
					type: ships[ship.name].id,
					count: ship.count
			});
		});
		return defShips;
	}

	function getAttackFleet(url, incUrl, classes, race) {
		var attShips = [];
		var incShips = [];
		$j.ajax({
			url: "https://game.planetarion.com/" + url,
			type: 'get',
			dataType: 'html',
			async: false,
			success: function(data) {
				var incClasses = [];
				if (classes.length) {
					$j.each(classes, function(ind, val) {
						if(val) { incClasses.push(shipClasses[val]) };
					});
				}
				$j(data).find('table tr').each(function(dt) {
					var $attShipName = $j(this).find('td:nth-of-type(1)').html();
					if($attShipName !== undefined) {
						if (!incClasses.length || (incClasses.length && $j.inArray(ships[$attShipName].class, incClasses) !== -1)) {
							var $attShipCount = $j(this).find('td:nth-of-type(2)').html().replace(',', '');
							var $attShipNumber = ships[$attShipName].id;
							attShips.push({
									type: $attShipNumber,
									count: $attShipCount.replace(',', '')
							});
						}
					}
				});
			}
		});
		if(incUrl) {
			var total = 0;
			var $shipTotal = 0;
			$j.ajax({
				url: "https://game.planetarion.com/" + incUrl,
				type: 'get',
				dataType: 'html',
				async: false,
				success: function(data) {
					$j(data).find('table tr').each(function(dt) {
						var $attShipName = $j(this).find('td:nth-of-type(1)').html();
						if($attShipName !== undefined && ships[$attShipName]) {
							var $attShipCount = $j(this).find('td:nth-of-type(2)').html().replace(',', '');
							$shipTotal = Number($shipTotal)+Number($attShipCount);
							var $attShipNumber = ships[$attShipName].id;
							incShips.push({
									type: $attShipNumber,
									count: $attShipCount.replace(',', '')
							});
						} else {
							if($(this).find('th').html()) {
								total = $(this).find('th').html().replace('Total Ships: ', '').replace(',', '');
							}
						}
					});
				}
			});
			if($shipTotal == total) {
				return incShips;
			}
		}
		return attShips;
	}

	function initBattleCalcData() {
        var defFleetCounter = 1;

        var autobcalc = localStorage.getItem('autobcalc');
        if(autobcalc !== null && autobcalc !== undefined) {
            autobcalc = JSON.parse(autobcalc);
            $j('#comment').val('Calc auto generated. Remember to trim attack fleets!');
            $j.each(autobcalc.def_fleets, function(ind, fleet){
                addFleet('def', fleet, defFleetCounter);
                defFleetCounter++;
            });
            var attFleetCounter = 1;
            $j.each(autobcalc.att_fleets, function(ind, fleet){
                addFleet('att', fleet, attFleetCounter);
                attFleetCounter++;
            });
        }
        else {
            console.log('nothing to calc');
        }
        localStorage.removeItem("autobcalc");
	}

	function generateBattleCalcData(obj) {
		var autobcalc = {
				def_fleets: [],
				att_fleets: [],
		};
		var eta = $j(obj).closest('.ally_def_eta_wrapper').data('eta');

		//Get base fleet
		var userDetail = $j(obj).find('div.ally_def_target_coords').find('a').attr('href');
		var userId = $j(obj).data('inc').split('_')[0];
		var userCoords = $j(obj).find('div.ally_def_target_coords').text().split(' ')[0];
		var roids = $j(obj).find('.ally_def_target_mdata_roids').text();
		var planetX = userCoords.match(/(\d+)/g)[0],
		planetY = userCoords.match(/(\d+)/g)[1],
		planetZ = userCoords.match(/(\d+)/g)[2];
		autobcalc.def_fleets.push({
			x:planetX,
			y:planetY,
			z:planetZ,
			roids: roids,
			ships:getBaseFleet(userId)
		});

		//Get defence fleets
		$j(obj).find('.ally_def_defender.mission_defend').each(function(defind, defobj) {
			var fleetId = $j(defobj).find('div.ally_def_defender_fleetname a').attr('id').split('_')[1];
			var defCoords = $j(defobj).find('div.ally_def_defender_coords a').text();
			var defX = defCoords.match(/(\d+)/g)[0],
				defY = defCoords.match(/(\d+)/g)[1],
				defZ = defCoords.match(/(\d+)/g)[2];
			var race = races[$j(defobj).find('.ally_def_defender_race').text()];
			autobcalc.def_fleets.push({
				x:defX,
				y:defY,
				z:defZ,
				roids: 0,
				race: race,
				ships:getDefenceFleet(fleetId)
			});
		});

		//Get attacker fleets
		$j(obj).find('.ally_def_attacker.mission_attack').each(function(attind, attobj) {
			var attCoords = $j(attobj).find('div.ally_def_attacker_coords a').text();
			var attX = attCoords.match(/(\d+)/g)[0],
				attY = attCoords.match(/(\d+)/g)[1],
				attZ = attCoords.match(/(\d+)/g)[2];
			var race = races[$j(attobj).find('.ally_def_attacker_race').text()];
			var classes = $j(attobj).find('div.ally_def_attacker_classes').text().split(', ');
			var incScanId = $j(attobj).find('div.ally_def_attacker_links a:contains("I")').attr('href');
			var scanId = $j(attobj).find('div.ally_def_attacker_links a:contains("A")').attr('href');
			if(scanId === undefined) {
				scanId = $j(attobj).find('div.ally_def_attacker_links a:contains("U")').attr('href');
			}
			if(scanId !== undefined) {
				autobcalc.att_fleets.push({
					x:attX,
					y:attY,
					z:attZ,
					roids: 0,
					race: race,
					ships:getAttackFleet(scanId, incScanId, classes, race)
				});
			}
		});
		localStorage.setItem("autobcalc", JSON.stringify(autobcalc));
		window.open('bcalc.pl?rn=' + Math.random(),'_blank');
	}

	function addFleet(type, fleet, fleetCounter) {
		if(fleetCounter !== 1) {
				w.add_fleet(type);
		}
		else {
				if(type === 'def') {
						$j('#def_asteroids').val(fleet.roids)
				}
		}
		$j('#' + type + '_coords_x_' + fleetCounter).val(fleet.x)
		$j('#' + type + '_coords_y_' + fleetCounter).val(fleet.y)
		$j('#' + type + '_coords_z_' + fleetCounter).val(fleet.z)
		$j('#' + type + '_planet_value_' + fleetCounter).val(fleet.value);
		$j('#' + type + '_planet_score_' + fleetCounter).val(fleet.score);
		$j('#' + type + '_' + fleetCounter + '_race').val(fleet.race);
		$j.each(fleet.ships, function(shipind, ship) {
				$j('#' + type + '_' + fleetCounter + '_' + ship.type).val(ship.count);
				$j('#' + type + '_row_' + ship.type).css("display", "");
		});
	}

	function parseScanLinks(page, parseNow = false)
	{
		var regex = new RegExp(/showscan.pl[?]scan_id=([a-zA-Z0-9]*)/g);
		var mtc = [];
		var match = [];
		while( (match = regex.exec( page )) != null ) {
				mtc.push(match[1]);
		}

		var xhr = new XMLHttpRequest();
		xhr.open("POST", toolsUrl + '/api/v1/collector/scans', true);
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send(
      		JSON.stringify({
				ids: mtc,
        		api_token: apiToken,
				instant: parseNow
		  	})
    	);
	}

	function parseGalStatus(page)
	{
		var fleets = [];

		$j('#galaxy_status_incoming .mission_attack').each(function() {
			var fleet = $(this).html().replace(/(\r\n|\n|\r|\t)/gm, "");
			var regex = new RegExp(/<td[^>]*><b>(\d+)\:(\d+)\:(\d+).*<\/b>[*]?<\/td><td[^>]*><a[^>]*>(\d+)\:(\d+)\:(\d+)<\/a><\/td><td[^>]*>A<\/td><td[^>]*>([\w ]+)<\/td><td[^>]*>[a-zA-Z]*<\/td><td[^>]*>(\d+)<\/td><td[^>]*>(\d+)<\/td>/g);
			var match = regex.exec( fleet );
			if(match) {
				fleets.push({
					to_x: match[1],
					to_y: match[2],
					to_z: match[3],
					from_x: match[4],
					from_y: match[5],
					from_z: match[6],
					fleet: match[7],
					ships: match[8],
					eta: match[9],
					tick: tick,
					type: 'Attack'
				});
			}
		});

		$j('#galaxy_status_incoming .mission_defend').each(function() {
			var fleet = $(this).html().replace(/(\r\n|\n|\r|\t)/gm, "");
			var regex = new RegExp(/<td[^>]*><b>(\d+)\:(\d+)\:(\d+).*<\/b>[*]?<\/td><td[^>]*><a[^>]*>(\d+)\:(\d+)\:(\d+)<\/a><\/td><td[^>]*>D<\/td><td[^>]*>([\w ]+)<\/td><td[^>]*>[a-zA-Z]*<\/td><td[^>]*>(\d+)<\/td><td[^>]*>(\d+)<\/td>/g);
			var match = regex.exec( fleet );
			if(match) {
				fleets.push({
					to_x: match[1],
					to_y: match[2],
					to_z: match[3],
					from_x: match[4],
					from_y: match[5],
					from_z: match[6],
					fleet: match[7],
					ships: match[8],
					eta: match[9],
					tick: tick,
					type: 'Defend'
				});
			}
		});

		$j('#galaxy_status_outgoing .mission_attack').each(function() {
			var fleet = $(this).html().replace(/(\r\n|\n|\r|\t)/gm, "");
			var regex = new RegExp(/<td[^>]*><a[^>]*>(\d+)\:(\d+)\:(\d+)<\/a><\/td><td[^>]*>(\d+)\:(\d+)\:(\d+).*<\/td><td[^>]*>A<\/td><td[^>]*>([\w ]+)<\/td><td[^>]*>(\d+)<\/td><td[^>]*>(\d+)<\/td>/g);
			var match = regex.exec( fleet );
			if(match) {
				fleets.push({
					to_x: match[1],
					to_y: match[2],
					to_z: match[3],
					from_x: match[4],
					from_y: match[5],
					from_z: match[6],
					fleet: match[7],
					ships: match[8],
					eta: match[9],
					tick: tick,
					type: 'Attack'
				});
			}
		});

		$j('#galaxy_status_outgoing .mission_defend').each(function() {
			var fleet = $(this).html().replace(/(\r\n|\n|\r|\t)/gm, "");
			var regex = new RegExp(/<td[^>]*><a[^>]*>(\d+)\:(\d+)\:(\d+)<\/a><\/td><td[^>]*>(\d+)\:(\d+)\:(\d+).*<\/td><td[^>]*>D<\/td><td[^>]*>([\w ]+)<\/td><td[^>]*>(\d+)<\/td><td[^>]*>(\d+)<\/td>/g);
			var match = regex.exec( fleet );
			if(match) {
				fleets.push({
					to_x: match[1],
					to_y: match[2],
					to_z: match[3],
					from_x: match[4],
					from_y: match[5],
					from_z: match[6],
					fleet: match[7],
					ships: match[8],
					eta: match[9],
					tick: tick,
					type: 'Defend'
				});
			}
		});

		collectFleets(fleets, "userscript_galaxy_status");
	}

  	function parseAllianceDefence()
	{
		var fleets = [];

		$j('.ally_def_eta_wrapper').each(function() {
			var eta = $(this).data('eta');
			$(this).find('.ally_def_incs_wrapper').each(function() {
				var coordsTo = $(this).find('.ally_def_target_coords')[0].innerText.split(":");
				$(this).find('.ally_def_attacker').each(function() {
					var isRecalled = false;
					if($(this).hasClass('mission_return')) {
						isRecalled = true;
					}
					var attackerCoords = $(this).find('.ally_def_attacker_coords')[0].innerText.split(":");
					var fleetName = $(this).find('.ally_def_attacker_fleetname')[0].innerText;
					var shipCount = $(this).find('.ally_def_attacker_shipcount')[0].innerText;
					var type = 'Attack';
					var fleet = {
						to_x: coordsTo[0],
						to_y: coordsTo[1],
						to_z: coordsTo[2],
						from_x: attackerCoords[0],
						from_y: attackerCoords[1],
						from_z: attackerCoords[2],
						fleet: fleetName,
						ships: shipCount,
						eta: eta,
						tick: tick,
						type: type,
						is_recalled: isRecalled
					};
					fleets.push(fleet);
				});
				$(this).find('.ally_def_defender').each(function() {
					var defenderCoords = $(this).find('.ally_def_defender_coords')[0].innerText.split(":");
					var fleetName = $(this).find('.ally_def_defender_fleetname')[0].innerText;
					var shipCount = $(this).find('.ally_def_defender_shipcount')[0].innerText;
					var type = 'Defend';
					var fleet = {
						to_x: coordsTo[0],
						to_y: coordsTo[1],
						to_z: coordsTo[2],
						from_x: defenderCoords[0],
						from_y: defenderCoords[1],
						from_z: defenderCoords[2],
						fleet: fleetName,
						ships: shipCount,
						eta: eta,
						tick: tick,
						type: type
					};
					fleets.push(fleet);
				});
			});
		});

		collectFleets(fleets, "userscript_alliance_defence");
	}
})(window);

function collectFleets(fleets, source) {
	if(fleets.length) {
		var xhr = new XMLHttpRequest();
		xhr.open("POST", toolsUrl + '/api/v1/collector/fleets', true);
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send(JSON.stringify({
		fleets: fleets,
		api_token: apiToken,
		source: source
		}));
	}	
}

function getUrlParameter(name) {
	name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
	var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
	var results = regex.exec(location.search);
	return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}