## Requirements

Laravel 10 requirements can be found [here](https://laravel.com/docs/10.x/deployment).

You will also require:

* php imap extension
* node 14 (currently)

**Tip** I use [Laravel Forge](https://forge.laravel.com/) to deploy the application to save me the effort of managing servers

## Setup .env file

* Copy .env.example to .env
* Change APP_NAME to whatever you want your webby to be called
* Change APP_ENV to production
* Change APP_DEBUG to false
* Change APP_URL to the url of your webby
* Update your DB settings

## Run commands

* composer install (you must have composer installed, google it)
* php artisan key:generate - this will set the APP_KEY in your .env file
* php artisan migrate - this will create your database assuming your DB settings were configured above
* php artisan db:seed - this will seed your database with required data
* npm install (you must have npm installed, google it)
* npm run ~~production~~ development - builds the front-end application (use "development" in development)

## Setup first user

Visit your website url and it will forward you to the install screen to create your first user

## Setup cron scheduler

The follow command needs to run every minute to enable the application to tick (among other things):

    php /path-to-your-project/artisan schedule:run

## PA Email Notifications

To collect email notifications from ingame, you must setup a new gmail account to receive emails and set the following in your .env file:

* PA_NOTIFICATION_EMAIL_ENABLED=1
* PA_NOTIFICATION_EMAIL_SERVER=
* PA_NOTIFICATION_EMAIL_PORT=
* PA_NOTIFICATION_EMAIL_USERNAME=
* PA_NOTIFICATION_EMAIL_PASSWORD=

The most important function of this feature is to collect incoming fleet notifications. These are added to the "Defence" page within your tools where calcs with the most up to date scans are created automatically. 

Once enabled, these notifications will also be private messaged on telegram to the user they concern providing they have linked their telegram user to the tools via !setnick <webby_name> to the bot.

## Infobip SMS & Calling

You must have your own Infobip credentials and add them to your .env file:

* INFOBIP_ENABLED=1
* INFOBIP_URL=
* INFOBIP_USERNAME=
* INFOBIP_PASSWORD=
* INFOBIP_API_KEY=
* INFOBIP_FROM=

## Twilio Calling

You can use Twilio for calling instead (currently not SMS), you must have your own twilio credentials and add them to your .env file:

* TWILIO_ENABLED=1
* TWILIO_SID=
* TWILIO_TOKEN=
* TWILIO_FROM=

## Telegram Integration

To enable telegram integration, you must first create a bot with BotFather and add the following credentials to your .env file:

* TELEGRAM_ENABLED=1
* TELEGRAM_API_TOKEN=
* TELEGRAM_BOT_USERNAME=

Once added you must setup the webhook by visiting <webby_url>/<bot_api_key>/set. This must be an SSL enabled url or telegram will reject it.

* TELEGRAM_ADMINS=

You can get your TG ID from the bot once the webhook is set by typing /whoami to it. Certain commands can only be run by the Bot Admin set in the .env file.

You will then have to add the bot to your telegram channel.

* Use !addnotificationschannel in the channel that you want to receive notifications.
* Use !addscanschannel in the channel you want to receive scan requests.

For a full list of commands loaded on the bot use the !help command.

## Using the application

Before a round starts you should go to the Admin section and hit the "Reset tools" button at the bottom, this will remove the previous rounds data and parse the new ship stats. You can also do this via the command line:

* php artisan pa:reset
* php artisan parse:shipstats

## Extending the application 

You can run a development environment of the application locally using [Laravel Sail](https://laravel.com/docs/10.x/sail).

## Credits

* ChronoX for adding a lot of the Telegram commands.
* macen for stealing my code and forcing me to open source them.

## License
PA Tools is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).